package com.dyj.common.domain.vo;

import com.dyj.common.domain.TextAntiDirtPredicts;

import java.util.List;

public class TextAntiDirtVo {

    /**
     * 检测结果-状态码
     * 检测结果-消息
     */
    private String msg;

    /**
     * 检测结果-状态码
     */
    private Integer code;

    /**
     * 检测结果-任务 id
     */
    private String task_id;

    /**
     * 检测结果-数据 id
     */
    private String data_id;

    /**
     * 检测结果-置信度列表
     */
    private List<TextAntiDirtPredicts> predicts;

    public String getMsg() {
        return msg;
    }

    public TextAntiDirtVo setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public TextAntiDirtVo setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getTask_id() {
        return task_id;
    }

    public TextAntiDirtVo setTask_id(String task_id) {
        this.task_id = task_id;
        return this;
    }

    public String getData_id() {
        return data_id;
    }

    public TextAntiDirtVo setData_id(String data_id) {
        this.data_id = data_id;
        return this;
    }

    public List<TextAntiDirtPredicts> getPredicts() {
        return predicts;
    }

    public TextAntiDirtVo setPredicts(List<TextAntiDirtPredicts> predicts) {
        this.predicts = predicts;
        return this;
    }
}
