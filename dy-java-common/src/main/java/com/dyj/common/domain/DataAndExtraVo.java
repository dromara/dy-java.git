package com.dyj.common.domain;

public class DataAndExtraVo<T>{
    private T data;

    private DyExtra extra;

    public T getData() {
        return data;
    }

    public DataAndExtraVo<T> setData(T data) {
        this.data = data;
        return this;
    }

    public DyExtra getExtra() {
        return extra;
    }

    public DataAndExtraVo<T> setExtra(DyExtra extra) {
        this.extra = extra;
        return this;
    }
}
