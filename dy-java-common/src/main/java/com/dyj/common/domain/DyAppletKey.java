package com.dyj.common.domain;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * 小程序密钥
 */
public class DyAppletKey {
    /**
     * 应用私钥
     */
    private PrivateKey applicationPrivateKey;
    /**
     * 应用公钥
     */
    private PublicKey applicationPublicKey;

    /**
     * 平台私钥
     */
    private PrivateKey platformPrivateKey;

    /**
     * 平台公钥
     */
    private PublicKey platformPublicKey;

    public PrivateKey getApplicationPrivateKey() {
        return applicationPrivateKey;
    }

    public DyAppletKey setApplicationPrivateKey(PrivateKey applicationPrivateKey) {
        this.applicationPrivateKey = applicationPrivateKey;
        return this;
    }

    public PublicKey getApplicationPublicKey() {
        return applicationPublicKey;
    }

    public DyAppletKey setApplicationPublicKey(PublicKey applicationPublicKey) {
        this.applicationPublicKey = applicationPublicKey;
        return this;
    }

    public PrivateKey getPlatformPrivateKey() {
        return platformPrivateKey;
    }

    public DyAppletKey setPlatformPrivateKey(PrivateKey platformPrivateKey) {
        this.platformPrivateKey = platformPrivateKey;
        return this;
    }

    public PublicKey getPlatformPublicKey() {
        return platformPublicKey;
    }

    public DyAppletKey setPlatformPublicKey(PublicKey platformPublicKey) {
        this.platformPublicKey = platformPublicKey;
        return this;
    }
}
