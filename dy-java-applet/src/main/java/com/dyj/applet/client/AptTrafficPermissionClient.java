package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.query.OpenTrafficPermissionQuery;
import com.dyj.applet.domain.vo.AdIncomeVo;
import com.dyj.applet.domain.vo.AdPlacementVo;
import com.dyj.applet.domain.vo.AdSettlementVo;
import com.dyj.applet.domain.vo.TrafficPermissionVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.interceptor.ClientTokenInterceptor;

/**
 * @author danmo
 * @date 2024-06-17 15:13
 **/
@BaseRequest(baseURL = "${domain}")
public interface AptTrafficPermissionClient {

    /**
     * 查询流量主开通状态
     *
     * @param query 应用信息
     * @return DySimpleResult<TrafficPermissionVo>
     */
    @Get(value = "/api/apps/v1/traffic_permission/query/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<TrafficPermissionVo> queryTrafficPermission(@Var("query") BaseQuery query);


    /**
     * 开通流量主
     *
     * @param query 应用信息
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v1/traffic_permission/open/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> openTrafficPermission(@JSONBody("query") OpenTrafficPermissionQuery query);


    /**
     * 查询广告位列表
     *
     * @param query 应用信息
     * @return DySimpleResult<AdPlacementVo>
     */
    @Get(value = "/api/apps/v1/ad_placement/query/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AdPlacementVo> queryAdPlacementList(@Var("query") BaseQuery query);


    /**
     * 新增广告位
     *
     * @param query           应用信息
     * @param adPlacementName 广告位名称
     * @param adPlacementType 广告位类型 1：Banner广告 2：激励视频广告 3：信息流广告 4：插屏广告 5：视频前贴片广告 6：视频后贴片广告
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v1/ad_placement/add/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> addAdPlacement(@Var("query") BaseQuery query, @JSONBody("ad_placement_name") String adPlacementName, @JSONBody("ad_placement_type") Integer adPlacementType);

    /**
     * 更新广告位
     *
     * @param query         应用信息
     * @param adPlacementId 广告位ID
     * @param status        广告位状态  0：关闭  1：开启
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v1/ad_placement/update/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> updateAdPlacement(@Var("query") BaseQuery query, @JSONBody("ad_placement_id") String adPlacementId, @JSONBody("status") Integer status);

    /**
     * 查询广告收入
     *
     * @param query     应用信息
     * @param startDate 开始日期，格式“2006-01-02”
     * @param endDate   结束日期，格式“2006-01-02”，查询时间范围不能超过90天
     * @param hostName  宿主APP  douyin-抖音 douyin_lite-抖音 litetoutiao-今日头条 tt_lite-今日头条 litehuoshan-抖音火山版
     * @return DySimpleResult<AdIncomeVo>
     */
    @Get(value = "/api/apps/v3/capacity/query_ad_income/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AdIncomeVo> queryAdIncome(@Var("query") BaseQuery query, @Query("start_date") String startDate, @Query("end_date") String endDate, @Query("host_name") String hostName);


    /**
     * 查询广告结算单列表
     * @param query 应用信息
     * @param month 结算月份，格式“2006-01”
     * @param status 结算状态0：未完成1：已完成
     * @return DySimpleResult<AdSettlementVo>
     */
    @Get(value = "/api/apps/v3/capacity/query_ad_settlement_list/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AdSettlementVo> queryAdSettlementList(@Var("query") BaseQuery query, @Query("month") String month, @Query("status") Integer status);
}
