package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.interceptor.ClientTokenInterceptor;

/**
 * 交易工具
 */
@BaseRequest(baseURL = "${domain}")
public interface TradeToolClient {

    /**
     * 交易工具->信用免押->信用授权->信用准入查询
     * @param body 信用准入查询请求值
     * @return
     */
    @Post(value = "${queryAdmissibleAuth}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryAdmissibleAuthVo> queryAdmissibleAuth(@JSONBody QueryAdmissibleAuthQuery body);

    /**
     * 交易工具->信用免押->信用授权->信用下单
     * @param body 信用下单请求值
     * @return
     */
    @Post(value = "${createAuthOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreateAuthOrderDataVo> createAuthOrder(@JSONBody CreateAuthOrderQuery body);

    /**
     * 交易工具->信用免押->信用授权->信用订单撤销
     * @param body 信用订单撤销请求值
     * @return
     */
    @Post(value = "${cancelAuthOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<Void> cancelAuthOrder(@JSONBody CancelAuthOrderQuery body);

    /**
     * 交易工具->信用免押->信用授权->信用订单完结
     * @param body 信用订单完结请求值
     * @return
     */
    @Post(value = "${finishAuthOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<Void> finishAuthOrder(@JSONBody FinishAuthOrderQuery body);

    /**
     * 交易工具->信用免押->信用授权->信用订单查询
     * @param body 信用订单查询请求值
     * @return
     */
    @Post(value = "${queryAuthOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryAuthOrderVo> queryAuthOrder(@JSONBody QueryAuthOrderQuery body);

    /**
     * 交易工具->信用免押->扣款->发起扣款
     * @param body 发起扣款请求值
     * @return
     */
    @Post(value = "${createPayOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreatePayOrderVo> createPayOrder(@JSONBody CreatePayOrderQuery body);

    /**
     * 交易工具->信用免押->扣款->关闭扣款
     * @param body 关闭扣款请求值
     * @return
     */
    @Post(value = "${closePayOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<Void> closePayOrder(@JSONBody ClosePayOrderQuery body);


    /**
     * 交易工具->信用免押->扣款->扣款单查询
     * @param body 扣款单查询请求值
     * @return
     */
    @Post(value = "${queryPayOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryPayOrderVo> queryPayOrder(@JSONBody QueryPayOrderQuery body);

    /**
     * 交易工具->信用免押->退款->发起退款
     * @param body 发起退款请求值
     * @return
     */
    @Post(value = "${createPayOrderRefund}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreatePayOrderVo> createPayOrderRefund(@JSONBody CreatePayOrderRefundQuery body);

    /**
     * 交易工具->信用免押->退款->退款查询
     * @param body 退款查询请求值
     * @return
     */
    @Post(value = "${queryPayRefund}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryPayRefundVo> queryPayRefund(@JSONBody QueryPayRefundQuery body);

    /**
     * 交易工具->周期代扣->签约授权->发起解约
     * @param body 发起解约请求值
     * @return
     */
    @Post(value = "${terminateSign}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<Void> terminateSign(@JSONBody TerminateSignQuery body);

    /**
     * 交易工具->周期代扣->签约授权->签约订单查询
     * @param body 签约订单查询请求值
     * @return
     */
    @Post(value = "${querySignOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QuerySignOrderVo> querySignOrder(@JSONBody QuerySignOrderQuery body);

    /**
     * 交易工具->周期代扣->代扣->发起代扣
     * @param body 发起代扣请求值
     * @return
     */
    @Post(value = "${createSignPayOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreateSignPayOrderVo> createSignPayOrder(@JSONBody CreateSignPayOrderQuery body);

    /**
     * 交易工具->周期代扣->代扣->代扣结果查询
     * @param body 代扣结果查询请求值
     * @return
     */
    @Post(value = "${querySignPayOrder}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QuerySignPayOrderVo> querySignPayOrder(@JSONBody QuerySignPayOrderQuery body);


    /**
     * 交易工具->周期代扣->退款->发起退款
     * @param body 发起退款请求值
     * @return
     */
    @Post(value = "${createSignRefund}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CreateSignRefundVo> createSignRefund(@JSONBody CreateSignRefundQuery body);

    /**
     * 交易工具->周期代扣->退款->退款查询
     * @param body 退款查询请求值
     * @return
     */
    @Post(value = "${querySignRefund}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QuerySignRefundVo> querySignRefund(@JSONBody QuerySignRefundQuery body);
}
