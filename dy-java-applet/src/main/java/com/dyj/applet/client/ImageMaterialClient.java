package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.query.AddFunctionConfigQuery;
import com.dyj.applet.domain.query.UploadImageMaterialQuery;
import com.dyj.applet.domain.vo.QueryFunctionConfigStatusVo;
import com.dyj.applet.domain.vo.UploadImageMaterialVo;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.interceptor.TransactionMerchantTokenInterceptor;

import java.util.List;

/**
 * 素材库
 */
@BaseRequest(baseURL = "${domain}")
public interface ImageMaterialClient {

    /**
     * 运营->素材库->素材库图片上传
     * @param body 素材库图片上传请求值
     * @return
     */
    @Post(value = "${uploadImageMaterial}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<UploadImageMaterialVo> uploadImageMaterial(@JSONBody UploadImageMaterialQuery body);


    /**
     * 运营->素材库->素材库设置功能配置
     * @param body 素材库设置功能配置请求值
     * @return
     */
    @Post(value = "${addFunctionConfig}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<Void> addFunctionConfig(@JSONBody AddFunctionConfigQuery body);

    /**
     * 运营->素材库->素材库查询功能配置审核状态
     * @param functionId 功能id
     * @return
     */
    @Get(value = "${queryFunctionConfigStatus}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<QueryFunctionConfigStatusVo> queryFunctionConfigStatus(@Var("query")BaseTransactionMerchantQuery query, @Query("function_id") List<String> functionId);
}
