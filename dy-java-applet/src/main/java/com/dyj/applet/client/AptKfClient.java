package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.Query;
import com.dtflys.forest.annotation.Var;
import com.dyj.applet.domain.vo.AptKfUrlVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.UserInfoQuery;
import com.dyj.common.interceptor.AppV2TokenHeaderInterceptor;

/**
 * @author danmo
 * @date 2024-06-06 09:47
 **/
@BaseRequest(baseURL = "${ttDomain}")
public interface AptKfClient {

    /**
     * 获取客服链接
     * @param query 用户信息
     * @param appId 授权小程序 appid
     * @param openid 用户在当前小程序的 ID，使用 code2session 接口返回的 openid
     * @param type 来源，抖音传 1128，抖音极速版传 2329
     * @param scene 场景值，固定值 1
     * @param orderId  订单号
     * @param imType im_type声明客服所属行业，小程序服务类目必须属于该行业 "group_buy"：酒旅、美食、其他本地服务行业
     * @return DySimpleResult<AptKfUrlVo>
     */
    @Get(url = "/api/apps/chat/customer_service_url",interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<AptKfUrlVo> getKfUrl(@Var("query")UserInfoQuery query, @Query("appid")String appId, @Query("openid")String openid, @Query("type")String type, @Query("scene") String scene, @Query("order_id") String orderId, @Query("im_type") String imType);
}
