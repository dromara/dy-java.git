package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.vo.ApplyCapacityListVo;
import com.dyj.applet.domain.vo.AuditCategoryListVo;
import com.dyj.applet.domain.vo.CapacityApplyStatusVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.interceptor.ClientTokenInterceptor;

import java.util.List;
import java.util.Map;

/**
 * 能力申请
 *
 * @author danmo
 * @date 2024-07-17 09:46
 **/
@BaseRequest(baseURL = "${domain}")
public interface AptCapacityApplyClient {

    /**
     * 能力申请  接口频率限制：10 QPS
     *
     * @param query       应用信息
     * @param capacityKey 能力标识
     * @param applyReason 申请理由
     * @param applyInfo   申请能力填写的字段
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v1/capacity/apply_capacity/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> applyCapacity(@Var("query") BaseQuery query, @JSONBody("capacity_key") String capacityKey, @JSONBody("apply_reason") String applyReason, @JSONBody("apply_info") Map<String, List<String>> applyInfo);

    /**
     * 能力申请状态查询 接口频率限制：10 QPS
     *
     * @param query       应用信息
     * @param capacityKey 能力key，能力唯一标识
     * @return DySimpleResult<CapacityApplyStatusVo>
     */
    @Get(value = "/api/apps/v1/capacity/query_apply_status/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CapacityApplyStatusVo> queryApplyStatus(@Var("query") BaseQuery query, @Query("capacity_key") String capacityKey);

    /**
     * 能力列表查询 接口频率限制：10 QPS
     *
     * @param query 应用信息
     * @return DySimpleResult<ApplyCapacityListVo>
     */
    @Get(value = "/api/apps/v1/capacity/query_capacity_list/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<ApplyCapacityListVo> queryCapacityList(@Var("query") BaseQuery query);


    /**
     * 获取已设置的服务类目
     * @param query 应用信息
     * @return  DySimpleResult<AuditCategoryListVo>
     */
    @Get(value = "/api/apps/v1/category/get_audit_categories/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AuditCategoryListVo> getAuditCategories(@Var("query") BaseQuery query);

}
