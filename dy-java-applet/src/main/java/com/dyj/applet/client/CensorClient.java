package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dyj.applet.domain.query.ImageCensorQuery;
import com.dyj.applet.domain.query.TextAntiDirtQuery;
import com.dyj.applet.domain.vo.ImageCensorVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.vo.TextAntiDirtVo;
import com.dyj.common.interceptor.ClientTokenInterceptor;
import com.dyj.common.interceptor.XTokenHeaderInterceptor;

import java.util.List;

/**
 * 基础能力->内容安全
 */
@BaseRequest(baseURL = "${domain}")
public interface CensorClient {

    /**
     * 基础能力->内容安全->图片检测V3
     * @param body 图片检测V3请求值
     * @return
     */
    @Post(value = "${imageCensor}", interceptor = ClientTokenInterceptor.class)
    ImageCensorVo imageCensor(@JSONBody ImageCensorQuery body);

    /**
     * 基础能力->内容安全->内容安全检测/api/tpapp/v2/auth/get_auth_token/
     * @param body
     * @return
     */
    @Post(value = "${ttDomain}${textAntiDirt}", interceptor = XTokenHeaderInterceptor.class)
    DySimpleResult<List<TextAntiDirtVo>> textAntiDirt(@JSONBody TextAntiDirtQuery body);
}
