package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.interceptor.ClientTokenInterceptor;

import java.util.List;

/**
 * 数据分析接口
 *
 * @author: danmo
 * @date: 2024/08/19
 */
@BaseRequest(baseURL = "${domain}")
public interface AptDataAnalysisClient {

    //****************用户分析******************************

    /**
     * 行为分析
     *
     * @param query       应用信息
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param os          操作系统，ios或者android
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserBehaviorDataVo>
     */
    @Get(value = "api/platform/v2/data_analysis/query_behavior_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisUserBehaviorDataVo> queryBehaviorData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime, @Query("host_name") String hostName, @Query("os") String os, @Query("version_type") String versionType);

    /**
     * 实时用户分析
     *
     * @param query       应用信息
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserBehaviorDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_real_time_user_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisUserBehaviorDataVo> queryRealTimeUserData(@Var("query") BaseQuery query, @Query("host_name") String hostName, @Query("version_type") String versionType);

    /**
     * 留存分析
     *
     * @param query       应用信息
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param os          操作系统，ios或者android
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserRetentionDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_retention_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisUserRetentionDataVo> queryRetentionData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime, @Query("host_name") String hostName, @Query("os") String os, @Query("version_type") String versionType);

    /**
     * 来源分析
     *
     * @param query       应用信息
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return
     */
    @Get(value = "/api/platform/v2/data_analysis/query_scene_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisUserSceneDataVo> querySceneData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime, @Query("host_name") String hostName, @Query("version_type") String versionType);

    /**
     * 用户画像分析
     * @param query       应用信息
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param userType    用户类型，active_user-活跃用户；new_user-新增用户
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserPortraitDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_user_portrait_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisUserPortraitDataVo> queryUserPortraitData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime, @Query("user_type") String userType, @Query("host_name") String hostName, @Query("version_type") String versionType);

    /**
     * 终端分析
     * @param query       应用信息
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param userType    用户类型，active_user-活跃用户；new_user-新增用户
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserClientDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_client_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisUserClientDataVo> queryClientData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime, @Query("user_type") String userType, @Query("host_name") String hostName, @Query("version_type") String versionType);

    /**
     * 页面分析
     * @param query       应用信息
     * @param startTime   开始时间，必须是昨天以前的某个时间戳
     * @param endTime     结束时间，必须是昨天以前的某个时间戳
     * @param hostName    宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param os          操作系统，ios或者android
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisUserPageDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_page_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisUserPageDataVo> queryPageData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime, @Query("host_name") String hostName, @Query("os") String os, @Query("version_type") String versionType);

    //****************交易分析******************************

    /**
     * 总览分析
     * @param query 应用信息
     * @param startTime 开始时间，必须是昨天以前的某个时间戳
     * @param endTime 结束时间，必须是昨天以前的某个时间戳
     * @param hostName 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；
     * @param versionType 版本类型。online-线上版本；gray-灰度版本
     * @return DySimpleResult<AnalysisDealOverviewDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_deal_overview_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisDealOverviewDataVo> queryDealOverviewData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime, @Query("host_name") String hostName, @Query("version_type") String versionType);

    /**
     * 流量转化
     * @param query 应用信息
     * @param startTime 开始时间，必须是昨天以前的某个时间戳
     * @param endTime 结束时间，必须是昨天以前的某个时间戳
     * @param hostName 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版
     * @param scenes_list 流量来源渠道
     * @return DySimpleResult<AnalysisDealDataWithConversionVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_deal_data_with_conversion/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisDealDataWithConversionVo> queryDealDataWithConversion(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime, @Query("host_name") String hostName, @Query("scenes_list") List<String> scenes_list);

    /**
     * 短视频交易分析
     * @param query 应用信息
     * @param startTime 开始时间，必须是昨天以前的某个时间戳
     * @param endTime 结束时间，必须是昨天以前的某个时间戳
     * @param hostName 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；
     * @return DySimpleResult<AnalysisVideoDealDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_video_deal_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisVideoDealDataVo> queryVideoDealData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime, @Query("host_name") String hostName);

    /**
     * 获取直播房间数据
     * @param query 应用信息
     * @param anchorName 主播昵称
     * @return DySimpleResult<AnalysisLiveRoomDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_live_room/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<LiveRoomDataVo> queryLiveRoomData(@Var("query") BaseQuery query, @Query("anchor_name") String anchorName);

    /**
     * 直播数据分析
     * @param query 应用信息
     * @param liveRoomId 直播间ID，可通过【获取直播房间数据】接口获取该字段通过 /api/platform/v2/data_analysis/query_live_room/
     *                   接口返回的 data.current_live_room.0.live_room_id 字段获取
     * @return
     */
    @Get(value = "/api/platform/v2/data_analysis/query_live_room_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisLiveRoomDataVo> queryLiveRoomData(@Var("query") BaseQuery query, @Query("live_room_id") Long liveRoomId);

    /**
     * 直播交易分析
     * @param query 应用信息
     * @param liveRoomId  直播间ID，可通过【获取直播房间数据】接口获取该字段通过 /api/platform/v2/data_analysis/query_live_room/
     *                    接口返回的 data.history_live_room.0.live_room_id 字段获取
     * @return
     */
    @Get(value = "/api/platform/v2/data_analysis/query_live_deal_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisLiveDealDataVo> queryLiveDealData(@Var("query") BaseQuery query,  @Query("live_room_id") Long liveRoomId);

    /**
     * 商品分析
     * @param query 应用信息
     * @param startTime 开始时间，必须是昨天以前的某个时间戳
     * @param endTime 结束时间，必须是昨天以前的某个时间戳
     * @param pageNum 页码，默认为1
     * @param pageSize 每页数量，默认为20，最大为50
     * @param hostName 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；
     * @return DySimpleResult<AnalysisProductDealDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_product_deal_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisProductDealDataVo> queryProductDealData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime,@Query("page_num") Long pageNum, @Query("page_size") Long pageSize, @Query("host_name") String hostName);


    //****************短视频分析******************************
    /**
     * 短视频投稿数据
     * @param query 短视频投稿数据参数
     * @return DySimpleResult<ShortLiveIdWithAwemeIdVo>
     */
    @Post(value = "/api/platform/v2/data_analysis/query_short_live_id_with_awemeid/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<ShortLiveIdWithAwemeIdVo> queryShortLiveIdWithAwemeId(@JSONBody ShortLiveIdWithAwemeIdQuery query);


    /**
     * 短视频总览数据
     * @param query 视频数据查询参数
     * @return DySimpleResult<AnalysisVideoDataVo>
     */
    @Post(value = "/api/platform/v2/data_analysis/query_video_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisVideoDataVo> queryVideoData(@JSONBody AnalysisVideoDataQuery query);

    /**
     * 短视频详细数据
     * @param query 视频数据查询参数
     * @return DySimpleResult<ShortLiveDataWithIdVo>
     */
    @Post(value = "/api/platform/v2/data_analysis/query_short_live_data_with_id/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<ShortLiveDataWithIdVo> queryShortLiveDataWithId(@JSONBody ShortLiveDataWithIdQuery query);

    /**
     * 流量来源
     * @param query 流量来源查询参数
     * @return DySimpleResult<AnalysisVideoSourceDataVo>
     */
    @Post(value = "/api/platform/v2/data_analysis/query_video_with_source/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisVideoSourceDataVo> queryVideoWithSource(@JSONBody AnalysisVideoSourceQuery query);

    //****************直播分析******************************

    /**
     * 主播分析
     * @param query 主播分析参数
     * @return DySimpleResult<LiveWithShortIdVo>
     */
    @Post(value = "/api/platform/v2/data_analysis/query_live_with_short_id/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<LiveWithShortIdVo> queryLiveWithShortId(@JSONBody LiveWithShortIdQuery query);



    //****************小房子直播分析******************************

    /**
     * 小房子直播间总览数据
     * @param query 小房子直播间总览数据参数
     * @param startTime 开始时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
     * @param endTime 结束时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据，end_time就传2023-03-20 00:00:00对应的时间戳
     * @return DySimpleResult<SmallHomeOverviewDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_small_home_overview_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<SmallHomeOverviewDataVo> querySmallHomeOverviewData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime);

    /**
     * 小房子直播间详细数据
     * @param query 小房子直播间详细数据参数
     * @param startTime 开始时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
     * @param endTime 结束时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据，end_time就传2023-03-20 00:00:00对应的时间戳
     * @return DySimpleResult<SmallHomeRoomDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_small_home_room_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<SmallHomeRoomDataVo> querySmallHomeRoomData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime);

    /**
     * 小房子直播间订单数据
     * @param query 小房子直播间订单数据参数
     * @param startTime 开始时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
     * @param endTime 结束时间，必须是昨天以前的某个时间戳，最好是传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据，end_time就传2023-03-20 00:00:00对应的时间戳
     * @param pageNum 分页编号，从 0 开始
     * @param pageSize 分页大小，小于等于 1000，大于1000的默认取最大值1000
     * @return DySimpleResult<SmallHomeOrderDataVo>
     */
    @Get(value = "/api/platform/v2/data_analysis/query_small_home_order_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<SmallHomeOrderDataVo> querySmallHomeOrderData(@Var("query") BaseQuery query, @Query("start_time") Long startTime, @Query("end_time") Long endTime, @Query("page_num") Integer pageNum, @Query("page_size") Integer pageSize);

    //****************留资分析******************************

    /**
     * 组件使用数据
     * @param query 组件使用数据参数
     * @param startTime 结束时间，必须是昨天以前的某个时间戳
     * @param endTime 开始时间，必须是昨天以前的某个时间戳
     * @param componentIdList 组件配置id（配置的唯一标识）。可以通过查询已创建的线索组件接口获得
     * @return DySimpleResult<AnalysisComponentWithOverviewVo>
     */
    @Post(value = "/api/platform/v2/data_analysis/query_component_with_overview/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisComponentWithOverviewVo> queryComponentWithOverview(@Var("query") BaseQuery query,@JSONBody("start_time") Long startTime, @Query("end_time") Long endTime,@JSONBody("componentId_list") List<String> componentIdList);

    /**
     * 流量来源
     * @param query 流量来源参数
     * @param startTime 开始时间，必须是昨天以前的某个时间戳
     * @param endTime 结束时间，必须是昨天以前的某个时间戳
     * @param componentIdList 组件配置id（配置的唯一标识）
     * @return DySimpleResult<AnalysisComponentWithSourceVo>
     */
    @Post(value = "/api/platform/v2/data_analysis/query_component_with_source/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisComponentWithSourceVo> queryComponentWithSource(@Var("query") BaseQuery query,@JSONBody("start_time") Long startTime, @Query("end_time") Long endTime,@JSONBody("componentId_list") List<String> componentIdList);

    /**
     * 组件详细数据
     * @param query 组件详细数据参数
     * @param startTime 开始时间，必须是昨天以前的某个时间戳
     * @param endTime 结束时间，必须是昨天以前的某个时间戳
     * @param pageNo 页号
     * @param pageSize 页面大小，最大50
     * @param isQueryLive 是否查询直播维度
     * @param isQueryVideo 是否查询短视频维度
     * @param componentIdList 组件配置id（配置的唯一标识）
     * @return DySimpleResult<AnalysisComponentWithDetailVo>
     */
    @Post(value = "/api/platform/v2/data_analysis/query_component_with_detail/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisComponentWithDetailVo> queryComponentWithDetail(@Var("query") BaseQuery query,@JSONBody("start_time") Long startTime, @Query("end_time") Long endTime, @JSONBody("page_no") Integer pageNo, @JSONBody("page_size") Integer pageSize, @JSONBody("is_query_live") Boolean isQueryLive, @JSONBody("is_query_video") Boolean isQueryVideo,@JSONBody("componentId_list") List<String> componentIdList);

    /**
     * 组件使用对比
     * @param query 组件使用对比参数
     * @param startTime 开始时间，必须是昨天以前的某个时间戳
     * @param endTime 结束时间，必须是昨天以前的某个时间戳
     * @param pageNo 页号
     * @param pageSize 页面大小，最大50
     * @param componentIdList 组件配置id（配置的唯一标识）
     * @return DySimpleResult<AnalysisComponentWithDataVo>
     */
    @Post(value = "/api/platform/v2/data_analysis/query_component_with_data/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AnalysisComponentWithDataVo> queryComponentWithData(@Var("query") BaseQuery query,@JSONBody("start_time") Long startTime, @Query("end_time") Long endTime, @JSONBody("page_no") Integer pageNo, @JSONBody("page_size") Integer pageSize, @JSONBody("componentId_list") List<String> componentIdList);
}
