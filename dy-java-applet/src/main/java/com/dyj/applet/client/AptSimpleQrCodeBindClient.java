package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.vo.SimpleQrBindListVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.interceptor.ClientTokenInterceptor;
import com.dyj.common.interceptor.TpV2AuthTokenHeaderInterceptor;

/**
 * @author danmo
 * @date 2024-07-04 10:16
 **/
@BaseRequest(baseURL = "${domain}")
public interface AptSimpleQrCodeBindClient {

    /**
     * 查询普通二维码绑定列表
     * @param query 应用信息
     * @param pageNum 分页编号，从 1 开始
     * @param pageSize 分页大小，小于等于 50
     * @return DySimpleResult<SimpleQrBindListVo>
     */
    @Get(value = "/api/apps/v2/capacity/query_simple_qr_bind_list/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<SimpleQrBindListVo> querySimpleQrBindList(@Var("query") BaseQuery query, @Query("page_num") Long pageNum, @Query("page_size") Long pageSize);

    /**
     * 查询普通二维码绑定列表(服务商代调用场景)
     * @param query 应用信息
     * @param pageNum 分页编号，从 1 开始
     * @param pageSize 分页大小，小于等于 50
     * @return DySimpleResult<SimpleQrBindListVo>
     */
    @Get(value = "/api/apps/v2/capacity/query_simple_qr_bind_list/", interceptor = TpV2AuthTokenHeaderInterceptor.class)
    DySimpleResult<SimpleQrBindListVo> queryTpSimpleQrBindList(@Var("query") BaseQuery query, @Query("page_num") Long pageNum, @Query("page_size") Long pageSize);

    /**
     * 新增绑定二维码
     * @param query 应用信息
     * @param exclusiveQrUrlPrefix 是否独占该链接作为前缀0：不独占1：独占
     * @param loadPath 跳转的小程序路径
     * @param qrUrl 绑定的链接地址
     * @param stage 测试范围，latest-测试 online-线上
     * @return DySimpleResult
     */
    @Post(value = "/api/apps/v2/capacity/add_simple_qr_bind/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> addSimpleQrBind(@Var("query") BaseQuery query,
                                           @JSONBody("exclusive_qr_url_prefix") Integer exclusiveQrUrlPrefix,
                                           @JSONBody("load_path") String loadPath,
                                           @JSONBody("qr_url") String qrUrl,
                                           @JSONBody("stage") String stage);
    /**
     * 新增绑定二维码(服务商代调用场景)
     * @param query 应用信息
     * @param exclusiveQrUrlPrefix 是否独占该链接作为前缀0：不独占1：独占
     * @param loadPath 跳转的小程序路径
     * @param qrUrl 绑定的链接地址
     * @param stage 测试范围，latest-测试 online-线上
     * @return DySimpleResult
     */
    @Post(value = "/api/apps/v2/capacity/add_simple_qr_bind/", interceptor = TpV2AuthTokenHeaderInterceptor.class)
    DySimpleResult<String> addTpSimpleQrBind(@Var("query") BaseQuery query,
                                           @JSONBody("exclusive_qr_url_prefix") Integer exclusiveQrUrlPrefix,
                                           @JSONBody("load_path") String loadPath,
                                           @JSONBody("qr_url") String qrUrl,
                                           @JSONBody("stage") String stage);

    /**
     * 更新绑定二维码链接
     * @param query 应用信息
     * @param beforeQrUrl 更新前的链接地址
     * @param exclusiveQrUrlPrefix 是否独占该链接作为前缀0：不独占1：独占
     * @param loadPath 跳转的小程序路径
     * @param qrUrl 绑定的链接地址
     * @param stage 测试范围，latest-测试 online-线上
     * @return  DySimpleResult<String>
     */
    @Post(value = "/api/apps/v2/capacity/update_simple_qr_bind/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> updateSimpleQrBind(@Var("query") BaseQuery query,
                                              @JSONBody("before_qr_url") String beforeQrUrl,
                                              @JSONBody("exclusive_qr_url_prefix") Integer exclusiveQrUrlPrefix,
                                              @JSONBody("load_path") String loadPath,
                                              @JSONBody("qr_url") String qrUrl,
                                              @JSONBody("stage") String stage);

    /**
     * 更新绑定二维码链接(服务商代调用场景)
     * @param query 应用信息
     * @param beforeQrUrl 更新前的链接地址
     * @param exclusiveQrUrlPrefix 是否独占该链接作为前缀0：不独占1：独占
     * @param loadPath 跳转的小程序路径
     * @param qrUrl 绑定的链接地址
     * @param stage 测试范围，latest-测试 online-线上
     * @return  DySimpleResult<String>
     */
    @Post(value = "/api/apps/v2/capacity/update_simple_qr_bind/", interceptor = TpV2AuthTokenHeaderInterceptor.class)
    DySimpleResult<String> updateTpSimpleQrBind(@Var("query") BaseQuery query,
                                              @JSONBody("before_qr_url") String beforeQrUrl,
                                              @JSONBody("exclusive_qr_url_prefix") Integer exclusiveQrUrlPrefix,
                                              @JSONBody("load_path") String loadPath,
                                              @JSONBody("qr_url") String qrUrl,
                                              @JSONBody("stage") String stage);

    /**
     * 更新绑定二维码状态
     * @param query 应用信息
     * @param qrUrl 需要更新状态的链接地址
     * @param status 需要更新的状态1：发布2：下线
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v2/capacity/update_simple_qr_bind_status/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> updateSimpleQrBindStatus(@Var("query") BaseQuery query,
                                              @JSONBody("qr_url") String qrUrl,
                                              @JSONBody("status") Integer status);


    /**
     * 更新绑定二维码状态(服务商代调用场景)
     * @param query 应用信息
     * @param qrUrl 需要更新状态的链接地址
     * @param status 需要更新的状态1：发布2：下线
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v2/capacity/update_simple_qr_bind_status/", interceptor = TpV2AuthTokenHeaderInterceptor.class)
    DySimpleResult<String> updateTpSimpleQrBindStatus(@Var("query") BaseQuery query,
                                              @JSONBody("qr_url") String qrUrl,
                                              @JSONBody("status") Integer status);

    /**
     * 删除绑定二维码链接
     * @param query 应用信息
     * @param qrUrl 需要删除的链接地址
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v2/capacity/delete_simple_qr_bind/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> deleteSimpleQrBind(@Var("query") BaseQuery query,
                                              @JSONBody("qr_url") String qrUrl);

    /**
     * 删除绑定二维码链接(服务商代调用场景)
     * @param query 应用信息
     * @param qrUrl 需要删除的链接地址
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v2/capacity/delete_simple_qr_bind/", interceptor = TpV2AuthTokenHeaderInterceptor.class)
    DySimpleResult<String> deleteTpSimpleQrBind(@Var("query") BaseQuery query,
                                              @JSONBody("qr_url") String qrUrl);
}
