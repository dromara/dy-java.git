package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dtflys.forest.backend.ContentType;
import com.dyj.applet.domain.UpdateCommonPlanStatus;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.domain.DyProductResult;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.domain.vo.BaseVo;
import com.dyj.common.interceptor.AppV2TokenHeaderInterceptor;
import com.dyj.common.interceptor.ClientQueryTokenInterceptor;
import com.dyj.common.interceptor.ClientTokenInterceptor;

import java.util.List;

/**
 * 生活服务
 *
 * @author danmo
 * @date 2024-04-28 11:16
 **/
@BaseRequest(baseURL = "${domain}", contentType = ContentType.APPLICATION_JSON)
public interface LifeServicesClient {

    /**
     * 商铺同步
     *
     * @param query 入参
     * @return DyResult<SupplierSyncVo>
     */
    @Post(url = "${supplierSync}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierSyncVo> supplierSync(@JSONBody SupplierSyncQuery query);

    /**
     * 查询店铺
     *
     * @param query         应用信息
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierVo>
     */
    @Get(url = "${querySupplier}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierVo> querySupplier(@Var("query") BaseQuery query, @Query("supplier_ext_id") String supplierExtId);

    /**
     * 获取抖音POI ID
     *
     * @param query  应用信息
     * @param amapId 高德POI ID
     * @return DyResult<PoiIdVo>
     */
    @Get(url = "${queryPoiId}", interceptor = ClientTokenInterceptor.class)
    DyResult<PoiIdVo> queryPoiId(@Var("query") BaseQuery query, @Query("amap_id") String amapId);

    /**
     * 店铺匹配任务结果查询
     *
     * @param query           应用信息
     * @param supplierTaskIds 店铺任务ID
     * @return DyResult<SupplierTaskResultVo>
     */
    @Get(url = "${querySupplierTaskResult}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskResultVo> querySupplierTaskResult(@Var("query") BaseQuery query, @Query("supplier_task_ids") String supplierTaskIds);

    /**
     * 店铺匹配任务状态查询
     *
     * @param query         应用信息
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierTaskStatusVo>
     */
    @Get(url = "${querySupplierMatchStatus}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskStatusVo> querySupplierMatchStatus(@Var("query") BaseQuery query, @Query("supplier_ext_id") String supplierExtId);

    /**
     * 提交门店匹配任务
     *
     * @param query 入参
     * @return DyResult<SupplierSubmitTaskVo>
     */
    @Post(url = "${submitSupplierMatchTask}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierSubmitTaskVo> submitSupplierMatchTask(@JSONBody SupplierSubmitTaskQuery query);

    /**
     * 查询全部店铺信息接口(天级别请求5次)
     *
     * @param query 应用信息
     * @return DyResult<SupplierTaskVo>
     */
    @Get(url = "${queryAllSupplier}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskVo> queryAllSupplier(@Var("query") BaseQuery query);

    /**
     * 查询店铺全部信息任务返回内容
     *
     * @param query  应用信息
     * @param taskId 任务ID
     * @return DyResult<SupplierTaskVo>
     */
    @Get(url = "${querySupplierCallback}", interceptor = ClientTokenInterceptor.class)
    DyResult<SupplierTaskVo> querySupplierCallback(@Var("query") BaseQuery query, @Query("task_id") String taskId);

    /**
     * （老版本）SKU同步
     *
     * @param query 入参
     * @return DyResult<BaseVo>
     */
    @Post(url = "${skuSync}", interceptor = ClientTokenInterceptor.class)
    DyResult<BaseVo> skuSync(@JSONBody SkuSyncQuery query);

    /**
     * （老版本）sku拉取(该接口由接入方实现)
     *
     * @param query     用户信息
     * @param spuExtId  接入方SPU ID 列表
     * @param startDate 拉取价格时间区间[start_date, end_date)
     * @param endDate   拉取价格时间区间[start_date, end_date)
     * @return DyResult<SkuHotelPullVo>
     */
    @Get(url = "${skuHotelPull}", interceptor = ClientTokenInterceptor.class)
    DyResult<SkuHotelPullVo> skuHotelPull(@Var("query") BaseQuery query, @Query("spu_ext_id") List<String> spuExtId, @Query("start_date") String startDate, @Query("end_date") String endDate);

    /**
     * （老版本）多门店SPU同步
     *
     * @param query 入参
     * @return DyResult<SpuSyncVo>
     */
    @Post(url = "${spuSync}", interceptor = ClientTokenInterceptor.class)
    DyResult<SpuSyncVo> spuSync(@JSONBody SpuSyncQuery query);

    /**
     * （老版本）多门店SPU状态同步
     *
     * @param query        应用信息
     * @param spuExtIdList 接入方商品ID列表
     * @param status       SPU状态， 1 - 在线; 2 - 下线
     * @return DyResult<SpuStatusVo>
     */
    @Post(url = "${spuStatusSync}", interceptor = ClientTokenInterceptor.class)
    DyResult<SpuStatusVo> spuStatusSync(@Var("query") BaseQuery query, @JSONBody("spu_ext_id_list") List<String> spuExtIdList, @JSONBody("status") Integer status);

    /**
     * （老版本）多门店SPU库存同步
     *
     * @param query 入参
     * @return DyResult<SpuStockVo>
     */
    @Post(url = "${spuStockSync}", interceptor = ClientTokenInterceptor.class)
    DyResult<SpuStockVo> spuStockSync(@JSONBody SpuStockQuery query);

    /**
     * （老版本）多门店SPU信息查询
     *
     * @param query
     * @param spuExtId                   第三方SPU ID
     * @param needSpuDraft               是否需要商品草稿数据(带有商品的审核状态，只展示最近30天的数据，并且最多最近提交的20次内)
     * @param spuDraftCount              需要商品草稿数据的数目(0-20)，超过这个范围默认返回20个
     * @param supplierIdsForFilterReason 供应商id列表，需要商品在某供应商下的过滤状态
     * @return DyResult<SpuVo>
     */
    @Get(url = "${spuQuery}", interceptor = ClientTokenInterceptor.class)
    DyResult<SpuVo> spuQuery(@Var("query") BaseQuery query, @Query("spu_ext_id") String spuExtId, @Query("need_spu_draft") Boolean needSpuDraft, @Query("spu_draft_count") Integer spuDraftCount, @Query("supplier_ids_for_filter_reason") List<String> supplierIdsForFilterReason);


    /**
     * 创建/修改团购商品
     *
     * @param query 入参
     * @return DyProductResult<SaveGoodsProductVo>
     */
    @Post(url = "${saveGoodsProduct}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<SaveGoodsProductVo> saveGoodsProduct(@JSONBody SaveGoodsProductQuery query);

    /**
     * 免审修改商品
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    @Post(url = "${freeAuditGoodsProduct}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<String> freeAuditGoodsProduct(@JSONBody FreeAuditGoodsProductQuery query);

    /**
     * 上下架商品
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    @Post(url = "${operateGoodsProduct}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<String> operateGoodsProduct(@JSONBody OperateGoodsProductQuery query);

    /**
     * 同步库存
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    @Post(url = "${syncGoodsStock}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<String> syncGoodsStock(@JSONBody SyncGoodsStockQuery query);

    /**
     * 查询商品模板
     *
     * @param query       应用信息
     * @param categoryId  行业类目；详细见； 商品类目表
     * @param productType 商品类型 1 : 团购套餐 3 : 预售券 4 : 日历房 5 : 门票 7 : 旅行跟拍 8 : 一日游 11 : 代金券
     * @return DyProductResult<GoodsTemplateVo>
     */
    @Get(url = "${getGoodsTemplate}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<GoodsTemplateVo> getGoodsTemplate(@Var("query") BaseQuery query, @Query("category_id") String categoryId, @Query("product_type") Integer productType);

    /**
     * 查询商品草稿数据
     *
     * @param query      应用信息
     * @param productIds 商品ID列表（逗号分隔）
     * @param outIds     外部商品ID列表（逗号分隔）
     * @return DyProductResult<GoodsProductDraftVo>
     */
    @Get(url = "${getGoodsProductDraft}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<GoodsProductDraftVo> getGoodsProductDraft(@Var("query") BaseQuery query, @Query("product_ids") String productIds, @Query("out_ids") String outIds);

    /**
     * 查询商品线上数据
     *
     * @param query      应用信息
     * @param productIds 商品ID列表（逗号分隔）
     * @param outIds     外部商品ID列表（逗号分隔）
     * @return DyProductResult<GoodsProductOnlineVo>
     */
    @Get(url = "${getGoodsProductOnline}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<GoodsProductOnlineVo> getGoodsProductOnline(@Var("query") BaseQuery query, @Query("product_ids") String productIds, @Query("out_ids") String outIds);

    /**
     * 查询商品线上数据列表
     *
     * @param query  应用信息
     * @param cursor 第一页不传，之后用前一次返回的next_cursor传入进行翻页
     * @param count  分页数量，不传默认为5
     * @param status 过滤在线状态 1-在线 2-下线 3-封禁
     * @return DyProductResult
     */
    @Get(url = "${queryGoodsProductOnlineList}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<GoodsProductOnlineVo> queryGoodsProductOnlineList(@Var("query") BaseQuery query, @Query("cursor") String cursor, @Query("count") Integer count, @Query("status") Integer status);

    /**
     * 查询商品草稿数据列表
     *
     * @param query  应用信息
     * @param cursor 第一页不传，之后用前一次返回的next_cursor传入进行翻页
     * @param count  分页数量，不传默认为5
     * @param status 过滤草稿状态，10-审核中 12-审核失败 1-审核通过
     * @return DyProductResult<GoodsProductDraftVo>
     */
    @Get(url = "${queryGoodsProductDraftList}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<GoodsProductDraftVo> queryGoodsProductDraftList(@Var("query") BaseQuery query, @Query("cursor") String cursor, @Query("count") Integer count, @Query("status") Integer status);

    /**
     * 创建/更新多SKU商品的SKU列表
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    @Post(url = "${batchSaveGoodsSku}", interceptor = ClientQueryTokenInterceptor.class)
    DyProductResult<String> batchSaveGoodsSku(@JSONBody BatchSaveGoodsSkuQuery query);

    /**
     * 查询商品品类
     *
     * @param query      应用信息
     * @param categoryId 行业类目ID，返回当前id下的直系子类目信息；传0或者不传，均返回所有一级行业类目
     * @param accountId  服务商的入驻商户ID/代运营的商户ID，不传时默认为服务商身份
     * @return GoodsCategoryVo
     */
    @Get(url = "${getGoodsCategory}", interceptor = ClientQueryTokenInterceptor.class)
    GoodsCategoryVo getGoodsCategory(@Var("query") BaseQuery query, @Query("category_id") String categoryId, @Query("account_id") String accountId);

    /**
     * 订单同步
     *
     * @param query 入参
     * @return DyResult<SyncOrderVo>
     */
    @Post(url = "${syncOrder}", interceptor = ClientTokenInterceptor.class)
    DyResult<SyncOrderVo> syncOrder(@JSONBody SyncOrderQuery query);

    /**
     * 获取POI基础数据
     *
     * @param query     应用信息
     * @param poiId     抖音poi_id
     * @param beginDate 最近30天，开始日期(yyyy-MM-dd)
     * @param endDate   最近30天，结束日期(yyyy-MM-dd)
     * @return DyResult<PoiBaseDataVo>
     */
    @Get(url = "${queryPoiBaseData}", interceptor = AppV2TokenHeaderInterceptor.class)
    DyResult<PoiBaseDataVo> queryPoiBaseData(@Var("query") BaseQuery query, @Query("poi_id") String poiId, @Query("begin_date") String beginDate, @Query("end_date") String endDate);

    /**
     * POI用户数据
     *
     * @param query    应用信息
     * @param poiId    抖音poi_id
     * @param dateType 近7/15/30天
     * @return DyResult<PoiUserDataVo>
     */
    @Get(url = "${queryPoiUserData}", interceptor = AppV2TokenHeaderInterceptor.class)
    DyResult<PoiUserDataVo> queryPoiUserData(@Var("query") BaseQuery query, @Query("poi_id") String poiId, @Query("date_type") Integer dateType);

    /**
     * POI服务基础数据
     *
     * @param query       应用信息
     * @param poiId       抖音poi_id
     * @param serviceType 服务类型，40:民宿
     * @param beginDate   最近30天，开始日期(yyyy-MM-dd)
     * @param endDate     最近30天，结束日期(yyyy-MM-dd)
     * @return DyResult<PoiServiceBaseDataVo>
     */
    @Get(url = "${queryPoiServiceBaseData}", interceptor = AppV2TokenHeaderInterceptor.class)
    DyResult<PoiServiceBaseDataVo> queryPoiServiceBaseData(@Var("query") BaseQuery query, @Query("poi_id") String poiId, @Query("service_type") Integer serviceType, @Query("begin_date") String beginDate, @Query("end_date") String endDate);

    /**
     * POI服务成交用户数据
     *
     * @param query       应用信息
     * @param poiId       抖音poi_id
     * @param serviceType 近7/15/30天
     * @param dateType    服务类型，40:民宿
     * @return DyResult<PoiServiceTransactionUserDataVo>
     */
    @Get(url = "${queryPoiServiceTransactionUserData}", interceptor = AppV2TokenHeaderInterceptor.class)
    DyResult<PoiServiceTransactionUserDataVo> queryPoiServiceTransactionUserData(@Var("query") BaseQuery query, @Query("poi_id") String poiId, @Query("service_type") Integer serviceType, @Query("date_type") Integer dateType);

    /**
     * POI热度榜
     *
     * @param query         应用信息
     * @param billboardType 10：近30日餐饮类POI的热度TOP500；20：近30日景点类POI的热度TOP500；30：近30日住宿类POI的热度TOP500
     * @return DyResult<PoiBillboardVo>
     */
    @Get(url = "${queryPoiBillboard}", interceptor = AppV2TokenHeaderInterceptor.class)
    DyResult<PoiBillboardVo> queryPoiBillboard(@Var("query") BaseQuery query, @Query("billboard_type") String billboardType);

    /**
     * POI认领列表
     *
     * @param query  应用信息
     * @param openId 通过/oauth/access_token/获取，用户唯一标志
     * @param cursor 分页游标, 第一页请求cursor是0, response中会返回下一页请求用到的cursor, 同时response还会返回has_more来表明是否有更多的数据
     * @param count  每页数量
     * @return DyResult
     */
    @Get(url = "${queryPoiClaimList}", interceptor = ClientTokenInterceptor.class)
    DyResult<PoiClaimVo> queryPoiClaimList(@Var("query") BaseQuery query, @Query("open_id") String openId, @Query("cursor") String cursor, @Query("count") Integer count);

    /**
     * 通过高德POI ID获取抖音POI ID
     *
     * @param query  应用信息
     * @param amapId 高德POI ID
     * @return DyResult<AmapPoiIdVo>
     */
    @Get(url = "${queryAmapPoiId}", interceptor = AppV2TokenHeaderInterceptor.class)
    DyResult<AmapPoiIdVo> queryAmapPoiId(@Var("query") BaseQuery query, @Query("amap_id") String amapId);

    /**
     * 优惠券同步
     *
     * @param query 入参
     * @return DyResult<SyncV2CouponVo>
     */
    @Post(url = "${syncV2Coupon}", interceptor = ClientQueryTokenInterceptor.class)
    DyResult<SyncV2CouponVo> syncV2Coupon(@JSONBody SyncV2CouponQuery query);

    /**
     * 优惠券更新
     *
     * @param query 入参
     * @return DyResult<SyncV2CouponVo>
     */
    @Post(url = "${syncV2CouponAvailable}", interceptor = ClientQueryTokenInterceptor.class)
    DyResult<SyncV2CouponVo> syncV2CouponAvailable(@JSONBody SyncV2CouponAvailableQuery query);

    /**
     * 通用佣金计划查询带货数据
     * @param query 应用信息
     * @param planIdList 通用佣金计划ID列表
     * @return DySimpleResult<CommonPlanSellDetailVo>
     */
    @Post(url = "${queryCommonPlanSellDetail}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CommonPlanSellDetailVo> queryCommonPlanSellDetail(@Var("query") BaseQuery query, @JSONBody("plan_id_list") List<Long> planIdList);

    /**
     * 通用佣金计划查询带货达人列表
     * @param query 应用信息
     * @param pageNum 分页参数：页码，从1开始计数
     * @param pageSize 分页参数：数量，1<=page_size<=100
     * @param planId 通用佣金计划ID
     * @return
     */
    @Post(url = "${queryCommonPlanTalentList}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CommonPlanTalentVo> queryCommonPlanTalentList(@Var("query") BaseQuery query,@JSONBody("page_num") Integer pageNum,@JSONBody("page_size") Integer pageSize, @JSONBody("plan_id") Long planId);

    /**
     * 通用佣金计划查询达人带货数据
     * @param query 应用信息
     * @param douyinIdList 待查询的达人抖音号列表
     * @param planId 通用佣金计划ID
     * @return  DySimpleResult<CommonPlanTalentSellDetailVo>
     */
    @Post(url = "${queryCommonPlanTalentSellDetail}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CommonPlanTalentSellDetailVo> queryCommonPlanTalentSellDetail(@Var("query") BaseQuery query,@JSONBody("douyin_id_list") List<String> douyinIdList,@JSONBody("plan_id") Long planId);

    /**
     * 通用佣金计划查询达人带货详情
     * @param query 应用信息
     * @param contentType  带货场景，1-仅短视频 2-仅直播间 3-短视频和直播间
     * @param douyinId 达人抖音号
     * @param pageNum 分页参数：页码，从1开始计数
     * @param pageSize 分页参数：数量，1<=page_size<=100
     * @param planId 通用佣金计划ID
     * @return DySimpleResult<CommonPlanTalentMedialVo>
     */
    @Post(url = "${queryCommonPlanTalentMediaList}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CommonPlanTalentMedialVo> queryCommonPlanTalentMediaList(@Var("query") BaseQuery query,@JSONBody("content_type")Integer contentType,@JSONBody("douyin_id") String douyinId , @JSONBody("page_num")Integer pageNum, @JSONBody("page_size")Integer pageSize, @JSONBody("plan_id")Long planId);

    /**
     * 查询通用佣金计划
     * @param query 应用信息
     * @param pageNo 分页参数：页码，从1开始计数
     * @param pageSize 分页参数：数量，1<=page_size<=100
     * @param spuId 上传小程序商品时返回的抖音内部商品ID
     * @param spuIdType 商品id类型 Product=1  MSpu=2
     * @return DySimpleResult<CommonPlanListVo>
     */
    @Post(url = "${queryCommonPlanList}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<CommonPlanListVo> queryCommonPlanList(@Var("query") BaseQuery query,@JSONBody("page_no")Integer pageNo,@JSONBody("page_size")Integer pageSize,@JSONBody("spu_id")Long spuId, @JSONBody("spu_id_type")Integer spuIdType);

    /**
     * 发布/修改通用佣金计划
     * @param query 应用信息
     * @param commissionRate 商品的抽佣率，万分数。 小程序商品可选值范围：100-2900具有代运营商服合作关系的商品可选值范围：100-8000，须小于或等于商家确认的佣金比例
     * @param contentType 商品支持的达人带货场景，可选以下的值：1：仅短视频2：仅直播间3：短视频和直播间
     * @param spuId 上传小程序商品时返回的抖音内部商品ID
     * @param planId 计划ID，创建计划时，plan_id不传或传0。修改现有计划时，传入待修改计划的ID
     * @return  DySimpleResult<SaveCommonPlanVo>
     */
    @Post(url = "${saveCommonPlan}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<SaveCommonPlanVo> saveCommonPlan(@Var("query") BaseQuery query,@JSONBody("commission_rate")Long commissionRate,@JSONBody("content_type")Integer contentType,@JSONBody("spu_id")Long spuId, @JSONBody("plan_id")Long planId);

    /**
     * 修改通用佣金计划状态
     * @param query 应用信息
     * @param statusList 待更新的计划与状态列表
     * @return DySimpleResult<UpdateCommonPlanStatusVo>
     */
    @Post(url = "${updateCommonPlanStatus}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<UpdateCommonPlanStatusVo> updateCommonPlanStatus(@Var("query") BaseQuery query, @JSONBody("plan_update_list") List<UpdateCommonPlanStatus> statusList);

    /**
     * 发布/修改直播间定向佣金计划
     * @param query 入参
     * @return  DySimpleResult<SaveCommonPlanVo>
     */
    @Post(url = "${saveLivePlan}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<SaveCommonPlanVo> saveLivePlan(@JSONBody SaveLivePlanQuery query);

    /**
     * 发布/修改短视频定向佣金计划
     * @param query 入参
     * @return  DySimpleResult<SaveCommonPlanVo>
     */
    @Post(url = "${saveShortVideoPlan}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<SaveCommonPlanVo> saveShortVideoPlan(@JSONBody SaveShortVideoPlanQuery query);

    /**
     * 取消定向佣金计划指定的达人
     * @param query 应用信息
     * @param planId 定向佣金计划ID
     * @param douyinId 达人抖音号
     * @return DySimpleResult
     */
    @Post(url = "${deletePlanTalent}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> deletePlanTalent(@Var("query") BaseQuery query,@JSONBody("plan_id") Long planId,@JSONBody("douyin_id") String douyinId);

    /**
     * 查询达人的定向佣金计划带货数据
     * @param query 应用信息
     * @param douyinIdList 待查询的达人抖音号列表
     * @param planId 定向佣金计划ID
     * @return DySimpleResult<OrientedPlanTalentSellDetailVo>
     */
    @Post(url = "${queryOrientedPlanTalentSellDetail}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<OrientedPlanTalentSellDetailVo> queryOrientedPlanTalentSellDetail(@Var("query") BaseQuery query, @JSONBody("douyin_id_list") List<String> douyinIdList, @JSONBody("plan_id") Long planId);

    /**
     * 通过商品 ID 查询定向佣金计划
     * @param query 应用信息
     * @param spuIdList 小程序商品 ID 列表，长度限制不超过 50 个
     * @return DySimpleResult<OrientedPlanTalentVo>
     */
    @Post(url = "${queryOrientedPlanList}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<OrientedPlanTalentVo> queryOrientedPlanList(@Var("query") BaseQuery query, @JSONBody("spu_id_list") List<Long> spuIdList);

    /**
     * 查询定向佣金计划带货汇总数据
     * @param query 应用信息
     * @param planIdList 待查询的定向佣金计划ID列表
     * @return
     */
    @Post(url = "${queryOrientedPlanSellDetail}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<OrientedPlanSellDetailVo> queryOrientedPlanSellDetail(@Var("query") BaseQuery query, @JSONBody("plan_id_list") List<Long> planIdList);
}
