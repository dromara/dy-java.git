package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.SimpleQrBindInfo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-07-04 10:21
 **/
public class SimpleQrBindListVo {
    /**
     * 剩余可以绑定的链接数量
     */
    private Integer bind_rest_count;
    /**
     * 总共可以绑定的链接数量 (上限)
     */
    private Integer bind_upper_limit;
    /**
     * 当月剩余可以的发布次数
     */
    private Integer release_rest_count;
    /**
     * 当月总共可以发布的次数 (上限)
     */
    private Integer release_upper_limit;
    /**
     * 目前绑定的链接数量
     */
    private Integer total_count;
    /**
     * 绑定列表
     */
    private List<SimpleQrBindInfo> qr_list;

    public Integer getBind_rest_count() {
        return bind_rest_count;
    }

    public void setBind_rest_count(Integer bind_rest_count) {
        this.bind_rest_count = bind_rest_count;
    }

    public Integer getBind_upper_limit() {
        return bind_upper_limit;
    }

    public void setBind_upper_limit(Integer bind_upper_limit) {
        this.bind_upper_limit = bind_upper_limit;
    }

    public Integer getRelease_rest_count() {
        return release_rest_count;
    }

    public void setRelease_rest_count(Integer release_rest_count) {
        this.release_rest_count = release_rest_count;
    }

    public Integer getRelease_upper_limit() {
        return release_upper_limit;
    }

    public void setRelease_upper_limit(Integer release_upper_limit) {
        this.release_upper_limit = release_upper_limit;
    }

    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public List<SimpleQrBindInfo> getQr_list() {
        return qr_list;
    }

    public void setQr_list(List<SimpleQrBindInfo> qr_list) {
        this.qr_list = qr_list;
    }
}
