package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-13 16:15
 **/
public class UpdateCommonPlanStatus {

    /**
     * 通用计划ID
     */
    private Long plan_id;
    /**
     * 可选值：1：设置为进行中 2：设置为暂停中 3：设置为已关闭计划
     * status为３时，不支持修改为其他值
     */
    private Integer status;

    public static UpdateCommonPlanStatusQueryBuilder builder() {
        return new UpdateCommonPlanStatusQueryBuilder();
    }

    public static class UpdateCommonPlanStatusQueryBuilder {
        private Long planId;

        private Integer status;

        public UpdateCommonPlanStatusQueryBuilder planId(Long planId) {
            this.planId = planId;
            return this;
        }

        public UpdateCommonPlanStatusQueryBuilder status(Integer status) {
            this.status = status;
            return this;
        }

        public UpdateCommonPlanStatus build() {
            UpdateCommonPlanStatus updateCommonPlanStatus = new UpdateCommonPlanStatus();
            updateCommonPlanStatus.setPlan_id(planId);
            updateCommonPlanStatus.setStatus(status);
            return updateCommonPlanStatus;
        }
    }

    public Long getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Long plan_id) {
        this.plan_id = plan_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
