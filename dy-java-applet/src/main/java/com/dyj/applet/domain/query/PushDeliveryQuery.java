package com.dyj.applet.domain.query;

import com.dyj.applet.domain.PushDeliveryItemOrderId;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * 推送核销状态请求值
 */
public class PushDeliveryQuery extends BaseQuery {

    /**
     * 开发者系统内的交易单号，称为外部单号。
     */
    private String out_order_no;
    /**
     * 核销的商铺 POI 信息，最多 1024 个字节。 注意：此字段为 string 类型，按下面结构填入所需字段后序列化成 string。 选填
     */
    private String poi_info;
    /**
     * 是否将整个订单核销。此参数设为 true 则将此订单的所有子单核销。 use_all 和 item_order_list 必须有一个有效值，要么指定 use_all=true 整单核销，要么使用 item_order_list 指定特定的商品单 id 核销。 注意：此参数设为 true 时只有 out_order_no 的所有商品单都为待使用状态才能成功核销。 选填
     */
    private Boolean use_all;
    /**
     * 指定需要核销的子单列表，此列表内的子单必须是上面 out_order_no 订单所对应的子单。 object 结构内的字段见下面介绍。 限制数组长度 <= 100
     */
    private List<PushDeliveryItemOrderId> item_order_list;

    public String getOut_order_no() {
        return out_order_no;
    }

    public PushDeliveryQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getPoi_info() {
        return poi_info;
    }

    public PushDeliveryQuery setPoi_info(String poi_info) {
        this.poi_info = poi_info;
        return this;
    }

    public Boolean getUse_all() {
        return use_all;
    }

    public PushDeliveryQuery setUse_all(Boolean use_all) {
        this.use_all = use_all;
        return this;
    }

    public List<PushDeliveryItemOrderId> getItem_order_list() {
        return item_order_list;
    }

    public PushDeliveryQuery setItem_order_list(List<PushDeliveryItemOrderId> item_order_list) {
        this.item_order_list = item_order_list;
        return this;
    }

    public static PushDeliveryQueryBuilder builder(){
        return new PushDeliveryQueryBuilder();
    }

    public static final class PushDeliveryQueryBuilder {
        private String out_order_no;
        private String poi_info;
        private Boolean use_all;
        private List<PushDeliveryItemOrderId> item_order_list;
        private Integer tenantId;
        private String clientKey;

        private PushDeliveryQueryBuilder() {
        }

        public PushDeliveryQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public PushDeliveryQueryBuilder poiInfo(String poiInfo) {
            this.poi_info = poiInfo;
            return this;
        }

        public PushDeliveryQueryBuilder useAll(Boolean useAll) {
            this.use_all = useAll;
            return this;
        }

        public PushDeliveryQueryBuilder itemOrderList(List<PushDeliveryItemOrderId> itemOrderList) {
            this.item_order_list = itemOrderList;
            return this;
        }

        public PushDeliveryQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public PushDeliveryQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public PushDeliveryQuery build() {
            PushDeliveryQuery pushDeliveryQuery = new PushDeliveryQuery();
            pushDeliveryQuery.setOut_order_no(out_order_no);
            pushDeliveryQuery.setPoi_info(poi_info);
            pushDeliveryQuery.setUse_all(use_all);
            pushDeliveryQuery.setItem_order_list(item_order_list);
            pushDeliveryQuery.setTenantId(tenantId);
            pushDeliveryQuery.setClientKey(clientKey);
            return pushDeliveryQuery;
        }
    }
}
