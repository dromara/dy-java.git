package com.dyj.applet.domain;

import java.util.List;

public class TradeQueryRefundResult {


    /**
     * 退款创建时间，13位毫秒时间戳
     */
    private Long create_at;
    /**
     * 退款结果信息，可以通过该字段了解退款失败原因 选填
     */
    private String message;
    /**
     * 系统订单信息，开放平台生成的订单号
     */
    private String order_id;
    /**
     * 开发者系统生成的退款单号，与抖音开平退款单号唯一关联 选填
     */
    private String out_refund_no;
    /**
     * 退款时间，13位毫秒时间戳，只有已退款才有退款时间 选填
     */
    private Long refund_at;
    /**
     * 系统退款单号，开放平台生成的退款单号
     */
    private String refund_id;
    /**
     * <p>退款来源，老的担保交易/1.0订单可能没有记录来源</p><ul><li>1: 用户发起退款</li><li>2: 开放者发起退款</li><li>3: 过期自动退款</li><li>4: 抖音客服退款</li><li>5: 预约失败自动发起退款</li><li>6: 开发者拒绝接单退款</li><li>7: 后约单触发先买单退款</li></ul> 选填
     */
    private Integer refund_source;
    /**
     * <ul><li>退款状态:退款中-PROCESSING</li><li>已退款-SUCCESS</li><li>退款失败-FAIL</li></ul>
     */
    private String refund_status;
    /**
     * 退款金额，单位[分]
     */
    private Long refund_total_amount;
    /**
     * 系统订单信息，开放平台生成的订单号 选填
     */
    private List<RefundItemOrderDetail> item_order_detail;
    /**
     * 退款审核信息 选填
     */
    private MerchantAuditDetail merchant_audit_detail;

    public Long getCreate_at() {
        return create_at;
    }

    public TradeQueryRefundResult setCreate_at(Long create_at) {
        this.create_at = create_at;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public TradeQueryRefundResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public TradeQueryRefundResult setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_refund_no() {
        return out_refund_no;
    }

    public TradeQueryRefundResult setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
        return this;
    }

    public Long getRefund_at() {
        return refund_at;
    }

    public TradeQueryRefundResult setRefund_at(Long refund_at) {
        this.refund_at = refund_at;
        return this;
    }

    public String getRefund_id() {
        return refund_id;
    }

    public TradeQueryRefundResult setRefund_id(String refund_id) {
        this.refund_id = refund_id;
        return this;
    }

    public Integer getRefund_source() {
        return refund_source;
    }

    public TradeQueryRefundResult setRefund_source(Integer refund_source) {
        this.refund_source = refund_source;
        return this;
    }

    public String getRefund_status() {
        return refund_status;
    }

    public TradeQueryRefundResult setRefund_status(String refund_status) {
        this.refund_status = refund_status;
        return this;
    }

    public Long getRefund_total_amount() {
        return refund_total_amount;
    }

    public TradeQueryRefundResult setRefund_total_amount(Long refund_total_amount) {
        this.refund_total_amount = refund_total_amount;
        return this;
    }

    public List<RefundItemOrderDetail> getItem_order_detail() {
        return item_order_detail;
    }

    public TradeQueryRefundResult setItem_order_detail(List<RefundItemOrderDetail> item_order_detail) {
        this.item_order_detail = item_order_detail;
        return this;
    }

    public MerchantAuditDetail getMerchant_audit_detail() {
        return merchant_audit_detail;
    }

    public TradeQueryRefundResult setMerchant_audit_detail(MerchantAuditDetail merchant_audit_detail) {
        this.merchant_audit_detail = merchant_audit_detail;
        return this;
    }
}
