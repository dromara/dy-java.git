package com.dyj.applet.domain;

public class CreatePayOrderFeeDetail {
    /**
     * <p>扣款项目金额，须>0</p>
     */
    private Long amount;
    /**
     * <p>扣款项目描述，长度<=512字节</p> 选填
     */
    private String description;
    /**
     * <p>扣款项目数量，须>0</p>
     */
    private Long quantity;
    /**
     * <p>扣款项目名称，长度<=256字节</p>
     */
    private String title;

    public Long getAmount() {
        return amount;
    }

    public CreatePayOrderFeeDetail setAmount(Long amount) {
        this.amount = amount;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CreatePayOrderFeeDetail setDescription(String description) {
        this.description = description;
        return this;
    }

    public Long getQuantity() {
        return quantity;
    }

    public CreatePayOrderFeeDetail setQuantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public CreatePayOrderFeeDetail setTitle(String title) {
        this.title = title;
        return this;
    }
}
