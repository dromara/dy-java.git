package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.SmallHomeRoomData;

public class SmallHomeRoomDataVo {

    /**
     *
     * 小程序app_id
     */
    private String app_id;
    /**
     *
     * 小程序名称
     */
    private String app_name;
    /**
     *
     * 是否还有更多数据
     */
    private Boolean has_more;
    /**
     * 查询这段时间内按直播间id维度聚合数据总数量
     */
    private Long total_num;

    private SmallHomeRoomData room_data;

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public Boolean getHas_more() {
        return has_more;
    }

    public void setHas_more(Boolean has_more) {
        this.has_more = has_more;
    }

    public Long getTotal_num() {
        return total_num;
    }

    public void setTotal_num(Long total_num) {
        this.total_num = total_num;
    }

    public SmallHomeRoomData getRoom_data() {
        return room_data;
    }

    public void setRoom_data(SmallHomeRoomData room_data) {
        this.room_data = room_data;
    }
}
