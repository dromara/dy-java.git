package com.dyj.applet.domain;

public class AnalysisProductDealData {

    /**
     * 支付总金额（单位：分）
     */
    private Long pay_amount;
    /**
     * 商品销售量
     */
    private Long pay_product_cnt;
    /**
     * 商品支付人数
     */
    private Long pay_user_cnt;
    /**
     * 商品ID
     */
    private Long product_id;
    /**
     *商品名称
     */
    private String product_name;

    public Long getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(Long pay_amount) {
        this.pay_amount = pay_amount;
    }

    public Long getPay_product_cnt() {
        return pay_product_cnt;
    }

    public void setPay_product_cnt(Long pay_product_cnt) {
        this.pay_product_cnt = pay_product_cnt;
    }

    public Long getPay_user_cnt() {
        return pay_user_cnt;
    }

    public void setPay_user_cnt(Long pay_user_cnt) {
        this.pay_user_cnt = pay_user_cnt;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }
}
