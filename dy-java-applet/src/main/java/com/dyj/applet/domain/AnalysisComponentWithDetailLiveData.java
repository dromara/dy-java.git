package com.dyj.applet.domain;

import java.util.List;

public class AnalysisComponentWithDetailLiveData {

    private Long Total;

    private List<ComponentWithDetailLiveData> DataList;

    public Long getTotal() {
        return Total;
    }

    public void setTotal(Long total) {
        Total = total;
    }

    public List<ComponentWithDetailLiveData> getDataList() {
        return DataList;
    }

    public void setDataList(List<ComponentWithDetailLiveData> dataList) {
        DataList = dataList;
    }

    public static class ComponentWithDetailLiveData {
        /**
         * 直播创建时间
         */
        private Long RoomCreateTime;
        /**
         * 直播时长
         */
        private Long RoomDuration;
        /**
         *
         * 直播间ID
         */
        private Long RoomId;
        private ComponentWithDetailData Data;
        private LiveWithShortData.ShortInfo ShortInfo;

        public Long getRoomCreateTime() {
            return RoomCreateTime;
        }

        public void setRoomCreateTime(Long roomCreateTime) {
            RoomCreateTime = roomCreateTime;
        }

        public Long getRoomDuration() {
            return RoomDuration;
        }

        public void setRoomDuration(Long roomDuration) {
            RoomDuration = roomDuration;
        }

        public Long getRoomId() {
            return RoomId;
        }

        public void setRoomId(Long roomId) {
            RoomId = roomId;
        }

        public ComponentWithDetailData getData() {
            return Data;
        }

        public void setData(ComponentWithDetailData data) {
            Data = data;
        }

        public LiveWithShortData.ShortInfo getShortInfo() {
            return ShortInfo;
        }

        public void setShortInfo(LiveWithShortData.ShortInfo shortInfo) {
            ShortInfo = shortInfo;
        }
    }
}
