package com.dyj.applet.domain.vo;

import java.util.List;

/**
 * 获取交易账单返回值
 */
public class GetBillVo {

    /**
     * <p>包含多个文件链接的数组</p><p>链接过期时间为6小时</p>
     */
    private List<String> bill_list;

    public List<String> getBill_list() {
        return bill_list;
    }

    public GetBillVo setBill_list(List<String> bill_list) {
        this.bill_list = bill_list;
        return this;
    }
}
