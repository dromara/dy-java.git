package com.dyj.applet.domain;

import java.util.Map;

public class SmallHomeOrderData {

    /**
     * key 订单号
     * value 订单数据
     */
    private Map<String,SmallHomeOrderValueData> room_data;

    public Map<String, SmallHomeOrderValueData> getRoom_data() {
        return room_data;
    }

    public void setRoom_data(Map<String, SmallHomeOrderValueData> room_data) {
        this.room_data = room_data;
    }

    public static class SmallHomeOrderValueData {

        /**
         * 小程序app_id
         */
        private String app_id;
        /**
         * 小程序名称
         */
        private String app_name;
        /**
         * 主播昵称
         */
        private String nick_name;
        /**
         * 目前订单状态，目前为3个枚举值：待使用/已完成/已退款
         */
        private Integer order_status;
        /**
         * 主播uid
         */
        private String anchor_id;
        /**
         * 核销订单金额(单位：分)
         */
        private Long delivery_success_amount;
        /**
         * 支付订单金额(单位：分)
         */
        private Long pay_amount;
        /**
         * 支付时间，秒级时间戳
         */
        private Long pay_time;
        /**
         * 已退款订单金额(单位：分)
         */
        private Long refund_success_amount;
        /**
         * 直播间id
         */
        private String room_id;

        public String getApp_id() {
            return app_id;
        }

        public void setApp_id(String app_id) {
            this.app_id = app_id;
        }

        public String getApp_name() {
            return app_name;
        }

        public void setApp_name(String app_name) {
            this.app_name = app_name;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }

        public Integer getOrder_status() {
            return order_status;
        }

        public void setOrder_status(Integer order_status) {
            this.order_status = order_status;
        }

        public String getAnchor_id() {
            return anchor_id;
        }

        public void setAnchor_id(String anchor_id) {
            this.anchor_id = anchor_id;
        }

        public Long getDelivery_success_amount() {
            return delivery_success_amount;
        }

        public void setDelivery_success_amount(Long delivery_success_amount) {
            this.delivery_success_amount = delivery_success_amount;
        }

        public Long getPay_amount() {
            return pay_amount;
        }

        public void setPay_amount(Long pay_amount) {
            this.pay_amount = pay_amount;
        }

        public Long getPay_time() {
            return pay_time;
        }

        public void setPay_time(Long pay_time) {
            this.pay_time = pay_time;
        }

        public Long getRefund_success_amount() {
            return refund_success_amount;
        }

        public void setRefund_success_amount(Long refund_success_amount) {
            this.refund_success_amount = refund_success_amount;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }
    }
}
