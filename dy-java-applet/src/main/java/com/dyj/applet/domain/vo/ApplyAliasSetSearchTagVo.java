package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.ApplyAliasSetSearchTagInfo;

import java.util.List;

public class ApplyAliasSetSearchTagVo {

    /**
     * 本月还剩余修改次数
     */
    private Integer month_available_times;

    /**
     * 每月总共可修改次数
     */
    private Integer month_total_times;

    /**
     * 搜索标签列表
     */
    private List<ApplyAliasSetSearchTagInfo> search_tag_list;

    public Integer getMonth_available_times() {
        return month_available_times;
    }

    public void setMonth_available_times(Integer month_available_times) {
        this.month_available_times = month_available_times;
    }

    public Integer getMonth_total_times() {
        return month_total_times;
    }

    public void setMonth_total_times(Integer month_total_times) {
        this.month_total_times = month_total_times;
    }

    public List<ApplyAliasSetSearchTagInfo> getSearch_tag_list() {
        return search_tag_list;
    }

    public void setSearch_tag_list(List<ApplyAliasSetSearchTagInfo> search_tag_list) {
        this.search_tag_list = search_tag_list;
    }
}
