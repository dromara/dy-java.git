package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-07-04 10:23
 **/
public class SimpleQrBindInfo {

    /**
     * 是否独占该链接作为前缀0：不独占1：独占
     */
    private Integer exclusive_qr_url_prefix;
    /**
     * 绑定的小程序路径
     */
    private String load_path;
    /**
     * 绑定的链接地址
     */
    private String qr_url;
    /**
     * 测试范围latest：测试online：线上
     */
    private String stage;
    /**
     * 状态0：待发布1：已发布2：已下线
     */
    private Integer status;

    public Integer getExclusive_qr_url_prefix() {
        return exclusive_qr_url_prefix;
    }

    public void setExclusive_qr_url_prefix(Integer exclusive_qr_url_prefix) {
        this.exclusive_qr_url_prefix = exclusive_qr_url_prefix;
    }

    public String getLoad_path() {
        return load_path;
    }

    public void setLoad_path(String load_path) {
        this.load_path = load_path;
    }

    public String getQr_url() {
        return qr_url;
    }

    public void setQr_url(String qr_url) {
        this.qr_url = qr_url;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
