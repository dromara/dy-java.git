package com.dyj.applet.domain;

import java.util.List;

/**
 * CPS 订单已核销记录信息
 */
public class QueryIndustryCpsDeliveryInfo {

    /**
     * 订单核销总金额，单位分，没有核销记录，为 0。
     */
    private Long total_delivery_amount;
    /**
     * 订单核销详细信息，如果没有核销记录，为 null。对于交易模板 1.0 和担保支付订单，为整单核销记录。对于交易系统，为商品单核销记录。
     */
    private List<QueryIndustryCpsDeliveryInfoDeliveryItem> delivery_items;

    public Long getTotal_delivery_amount() {
        return total_delivery_amount;
    }

    public QueryIndustryCpsDeliveryInfo setTotal_delivery_amount(Long total_delivery_amount) {
        this.total_delivery_amount = total_delivery_amount;
        return this;
    }

    public List<QueryIndustryCpsDeliveryInfoDeliveryItem> getDelivery_items() {
        return delivery_items;
    }

    public QueryIndustryCpsDeliveryInfo setDelivery_items(List<QueryIndustryCpsDeliveryInfoDeliveryItem> delivery_items) {
        this.delivery_items = delivery_items;
        return this;
    }
}
