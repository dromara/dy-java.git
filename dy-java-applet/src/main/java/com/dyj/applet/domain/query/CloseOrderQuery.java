package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class CloseOrderQuery extends BaseQuery {

    /**
     * 抖音开平内部交易订单号，通过预下单回调传给开发者服务，长度 < 64byte
     */
    private String order_id;

    public String getOrder_id() {
        return order_id;
    }

    public CloseOrderQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public static CloseOrderQueryBuilder builder() {
        return new CloseOrderQueryBuilder();
    }

    public static final class CloseOrderQueryBuilder {
        private String order_id;
        private Integer tenantId;
        private String clientKey;

        private CloseOrderQueryBuilder() {
        }

        public CloseOrderQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public CloseOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CloseOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CloseOrderQuery build() {
            CloseOrderQuery closeOrderQuery = new CloseOrderQuery();
            closeOrderQuery.setOrder_id(order_id);
            closeOrderQuery.setTenantId(tenantId);
            closeOrderQuery.setClientKey(clientKey);
            return closeOrderQuery;
        }
    }
}
