package com.dyj.applet.domain.query;

import com.dyj.applet.domain.SupplierSubmitTaskData;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-28 16:15
 **/
public class SupplierSubmitTaskQuery extends BaseQuery {

    private List<SupplierSubmitTaskData> match_data_list;

    public static SupplierSubmitTaskQueryBuilder builder() {
        return new SupplierSubmitTaskQueryBuilder();
    }

    public static class SupplierSubmitTaskQueryBuilder {
        private List<SupplierSubmitTaskData> matchDataList;

        private Integer tenantId;
        private String clientKey;

        public SupplierSubmitTaskQueryBuilder matchDataList(List<SupplierSubmitTaskData> matchDataList) {
            this.matchDataList = matchDataList;
            return this;
        }

        public SupplierSubmitTaskQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public SupplierSubmitTaskQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SupplierSubmitTaskQuery build() {
            SupplierSubmitTaskQuery supplierSubmitTaskQuery = new SupplierSubmitTaskQuery();
            supplierSubmitTaskQuery.setMatch_data_list(matchDataList);
            supplierSubmitTaskQuery.setTenantId(tenantId);
            supplierSubmitTaskQuery.setClientKey(clientKey);
            return supplierSubmitTaskQuery;
        }
    }

    public List<SupplierSubmitTaskData> getMatch_data_list() {
        return match_data_list;
    }

    public void setMatch_data_list(List<SupplierSubmitTaskData> match_data_list) {
        this.match_data_list = match_data_list;
    }
}
