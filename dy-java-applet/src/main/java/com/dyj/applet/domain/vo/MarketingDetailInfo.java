package com.dyj.applet.domain.vo;

public class MarketingDetailInfo {

    /**
     * 营销 id（用户身份 id、优惠券 id、积分 id 或者活动 id
     */
    private String id;

    /**
     * 营销类型:
     * •1：用户身份
     * •2：优惠券
     * •3: 积分
     * •4：活动
     */
    private Integer type;

    /**
     * 该营销策略优惠金额，单位分
     */
    private Integer discount_amount;

    /**
     * 营销名称
     */
    private String title;

    /**
     * 营销备注
     * 选填
     */
    private String note;

    /**
     * 优惠范围
     */
    private Integer discount_range;

    /**
     * 营销子类型
     */
    private String subtype;

    /**
     * 不同 type 含义不同，若 type = 3，value 则为积分值
     */
    private Long value;

    public String getId() {
        return id;
    }

    public MarketingDetailInfo setId(String id) {
        this.id = id;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public MarketingDetailInfo setType(Integer type) {
        this.type = type;
        return this;
    }

    public Integer getDiscount_amount() {
        return discount_amount;
    }

    public MarketingDetailInfo setDiscount_amount(Integer discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public MarketingDetailInfo setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getNote() {
        return note;
    }

    public MarketingDetailInfo setNote(String note) {
        this.note = note;
        return this;
    }

    public Integer getDiscount_range() {
        return discount_range;
    }

    public MarketingDetailInfo setDiscount_range(Integer discount_range) {
        this.discount_range = discount_range;
        return this;
    }

    public String getSubtype() {
        return subtype;
    }

    public MarketingDetailInfo setSubtype(String subtype) {
        this.subtype = subtype;
        return this;
    }

    public Long getValue() {
        return value;
    }

    public MarketingDetailInfo setValue(Long value) {
        this.value = value;
        return this;
    }
}
