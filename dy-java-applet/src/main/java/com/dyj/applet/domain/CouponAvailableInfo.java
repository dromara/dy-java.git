package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 16:37
 **/
public class CouponAvailableInfo {

    private SpuEntryInfo entry_info;

    private SpuExtInfo spu_ext_info;
    private SupplierExtInfo supplier_ext_info;

    public static CouponAvailableInfoBuilder builder() {
        return new CouponAvailableInfoBuilder();
    }

    public static class CouponAvailableInfoBuilder {
        private SpuEntryInfo entryInfo;
        private SpuExtInfo spuExtInfo;

        private SupplierExtInfo supplierExtInfo;

        public CouponAvailableInfoBuilder entryInfo(SpuEntryInfo entryInfo) {
            this.entryInfo = entryInfo;
            return this;
        }

        public CouponAvailableInfoBuilder spuExtInfo(SpuExtInfo spuExtInfo) {
            this.spuExtInfo = spuExtInfo;
            return this;
        }

        public CouponAvailableInfoBuilder supplierExtInfo(SupplierExtInfo supplierExtInfo) {
            this.supplierExtInfo = supplierExtInfo;
            return this;
        }

        public CouponAvailableInfo build() {
            CouponAvailableInfo couponAvailableInfo = new CouponAvailableInfo();
            couponAvailableInfo.setEntry_info(entryInfo);
            couponAvailableInfo.setSpu_ext_info(spuExtInfo);
            couponAvailableInfo.setSupplier_ext_info(supplierExtInfo);
            return couponAvailableInfo;
        }

    }


    public SpuEntryInfo getEntry_info() {
        return entry_info;
    }

    public void setEntry_info(SpuEntryInfo entry_info) {
        this.entry_info = entry_info;
    }

    public SpuExtInfo getSpu_ext_info() {
        return spu_ext_info;
    }

    public void setSpu_ext_info(SpuExtInfo spu_ext_info) {
        this.spu_ext_info = spu_ext_info;
    }

    public SupplierExtInfo getSupplier_ext_info() {
        return supplier_ext_info;
    }

    public void setSupplier_ext_info(SupplierExtInfo supplier_ext_info) {
        this.supplier_ext_info = supplier_ext_info;
    }
}
