package com.dyj.applet.domain;

/**
 * 可用券
 */
public class DeliveryPrepareCertificate {

    /**
     * 券 id，在验券接口传入
     */
    private String certificate_id;
    /**
     * 加密券码。在验券接口传入
     */
    private String encrypted_code;
    /**
     * 交易系统里对应的商品单 id
     */
    private String item_order_id;

    public String getCertificate_id() {
        return certificate_id;
    }

    public DeliveryPrepareCertificate setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
        return this;
    }

    public String getEncrypted_code() {
        return encrypted_code;
    }

    public DeliveryPrepareCertificate setEncrypted_code(String encrypted_code) {
        this.encrypted_code = encrypted_code;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public DeliveryPrepareCertificate setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }
}

