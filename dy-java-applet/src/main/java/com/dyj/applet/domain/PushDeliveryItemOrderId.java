package com.dyj.applet.domain;

/**
 * 指定需要核销的子单列表，此列表内的子单必须是上面 out_order_no 订单所对应的子单。 object 结构内的字段见下面介绍。 限制数组长度 <= 100
 * @author ws
 **/
public class PushDeliveryItemOrderId {

    /**
     * 需要核销的商品单 id
     */
    private String item_order_id;

    public String getItem_order_id() {
        return item_order_id;
    }

    public PushDeliveryItemOrderId setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }
}
