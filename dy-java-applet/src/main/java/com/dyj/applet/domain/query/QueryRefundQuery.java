package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 查询退款请求值
 */
public class QueryRefundQuery extends BaseQuery {

    /**
     * <p>抖音开平内部交易订单号，长度&lt;= 64byte。</p><p>注意：refund_id , out_refund_no , order_id 三选一，不能都不填。</p> 选填
     */
    private String order_id;
    /**
     * <p>开发者系统生成的退款单号。</p><p>注意：refund_id , out_refund_no , order_id 三选一，不能都不填。</p> 选填
     */
    private String out_refund_no;
    /**
     * <p>抖音开平内部交易退款单号，长度&lt;= 64byte。</p><p>注意：refund_id , out_refund_no , order_id 三选一，不能都不填。</p> 选填
     */
    private String refund_id;

    public String getOrder_id() {
        return order_id;
    }

    public QueryRefundQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_refund_no() {
        return out_refund_no;
    }

    public QueryRefundQuery setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
        return this;
    }

    public String getRefund_id() {
        return refund_id;
    }

    public QueryRefundQuery setRefund_id(String refund_id) {
        this.refund_id = refund_id;
        return this;
    }

    public static QueryRefundQueryBuilder builder() {
        return new QueryRefundQueryBuilder();
    }

    public static final class QueryRefundQueryBuilder {
        private String order_id;
        private String out_refund_no;
        private String refund_id;
        private Integer tenantId;
        private String clientKey;

        private QueryRefundQueryBuilder() {
        }

        public QueryRefundQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public QueryRefundQueryBuilder outRefundNo(String outRefundNo) {
            this.out_refund_no = outRefundNo;
            return this;
        }

        public QueryRefundQueryBuilder refundId(String refundId) {
            this.refund_id = refundId;
            return this;
        }

        public QueryRefundQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryRefundQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryRefundQuery build() {
            QueryRefundQuery queryRefundQuery = new QueryRefundQuery();
            queryRefundQuery.setOrder_id(order_id);
            queryRefundQuery.setOut_refund_no(out_refund_no);
            queryRefundQuery.setRefund_id(refund_id);
            queryRefundQuery.setTenantId(tenantId);
            queryRefundQuery.setClientKey(clientKey);
            return queryRefundQuery;
        }
    }
}
