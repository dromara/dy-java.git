package com.dyj.applet.domain.query;

import com.dyj.applet.domain.InteractRule;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;
import java.util.Map;

/**
 * @author danmo
 * @date 2024-05-27 18:35
 **/
public class CreateUserTaskQuery extends BaseQuery {

    /**
     * 小程序id
     */
    private String app_id;
    /**
     * 开始时间戳，秒级
     */
    private Long start_time;
    /**
     * 结束时间戳，秒级，任务持续时间最长一年
     */
    private Long end_time;
    /**
     * 挂载链接
     */
    private String mount_link;

    /**
     * 要求发布类型
     * 1：视频
     * 2：图文
     * 为空表明不做要求
     */
    private List<Integer> publish_type;

    /**
     * 话题标签，用户发布时需要带上的标签内容，可以设置多个，最多支持10个，每个长度上限20
     */
    private List<String> tags;

    /**
     * 规则类型
     * 1：流量类型：用户发布内容，满足类型标签条件即视为成功，不关注是否处于公开可见状态；
     * 2：长期任务类型：用户发布内容，需要保证是过审公开状态，并满足类型和标签条件，任务变动会产生webhook推送通知
     */
    private Integer rule_type;

    /**
     * 互动任务目标配置，key代表互动数据类型，key代表目标值。key的取值如下：
     * "like": 点赞数
     * "share": 分享数
     * "download": 下载数
     * "play": 播放数
     * "comment":评论数
     */
    private Map<String, InteractRule> interact_rules;

    /**
     * 目标拍抖音发布次数，用户需要发布拍抖音内容达到本参数设置次数，才视为整个任务完成
     */
    private Long target_count;
    /**
     * 任务类型
     * 2：拍抖音任务
     * 3：拍抖音互动任务
     */
    private Integer task_type;

    /**
     * 单用户可重复参与该任务的次数，发布一个视频视作参与一次，参与上限20次。
     */
    private Long max_count;


    public static CreateShareTaskQueryBuilder builder() {
        return new CreateShareTaskQueryBuilder();
    }

    public static class CreateShareTaskQueryBuilder {
        /**
         * 小程序id
         */
        private String appId;
        /**
         * 开始时间戳，秒级
         */
        private Long startTime;
        /**
         * 结束时间戳，秒级，任务持续时间最长一年
         */
        private Long endTime;
        /**
         * 挂载链接
         */
        private String mountLink;

        /**
         * 要求发布类型
         * 1：视频
         * 2：图文，为空表明不做要求
         */
        private List<Integer> publishType;

        /**
         * 话题标签，用户发布时需要带上的标签内容，可以设置多个，最多支持10个，每个长度上限20
         */
        private List<String> tags;

        /**
         * 规则类型
         * 1：流量类型：用户发布内容，满足类型标签条件即视为成功，不关注是否处于公开可见状态；
         * 2：长期任务类型：用户发布内容，需要保证是过审公开状态，并满足类型和标签条件，任务变动会产生webhook推送通知
         */
        private Integer ruleType;

        /**
         * 互动任务目标配置，key代表互动数据类型，key代表目标值。key的取值如下：
         * "like": 点赞数
         * "share": 分享数
         * "download": 下载数
         * "play": 播放数
         * "comment":评论数
         */
        private Map<String, InteractRule> interactRules;

        /**
         * 目标拍抖音发布次数，用户需要发布拍抖音内容达到本参数设置次数，才视为整个任务完成
         */
        private Long targetCount;
        /**
         * 任务类型
         * 2：拍抖音任务
         */
        private Integer taskType;

        /**
         * 单用户可重复参与该任务的次数，发布一个视频视作参与一次，参与上限20次。
         */
        private Long maxCount;

        private Integer tenantId;

        private String clientKey;

        public CreateShareTaskQueryBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }

        public CreateShareTaskQueryBuilder startTime(Long startTime) {
            this.startTime = startTime;
            return this;
        }

        public CreateShareTaskQueryBuilder endTime(Long endTime) {
            this.endTime = endTime;
            return this;
        }

        public CreateShareTaskQueryBuilder mountLink(String mountLink) {
            this.mountLink = mountLink;
            return this;
        }

        public CreateShareTaskQueryBuilder publishType(List<Integer> publishType) {
            this.publishType = publishType;
            return this;
        }

        public CreateShareTaskQueryBuilder tags(List<String> tags) {
            this.tags = tags;
            return this;
        }

        public CreateShareTaskQueryBuilder ruleType(Integer ruleType) {
            this.ruleType = ruleType;
            return this;
        }

        public CreateShareTaskQueryBuilder interactRules(Map<String, InteractRule> interactRules) {
            this.interactRules = interactRules;
            return this;
        }

        public CreateShareTaskQueryBuilder targetCount(Long targetCount) {
            this.targetCount = targetCount;
            return this;
        }

        public CreateShareTaskQueryBuilder taskType(Integer taskType) {
            this.taskType = taskType;
            return this;
        }

        public CreateShareTaskQueryBuilder maxCount(Long maxCount) {
            this.maxCount = maxCount;
            return this;
        }

        public CreateShareTaskQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateShareTaskQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateUserTaskQuery build() {
            CreateUserTaskQuery createShareTaskQuery = new CreateUserTaskQuery();
            createShareTaskQuery.setApp_id(appId);
            createShareTaskQuery.setStart_time(startTime);
            createShareTaskQuery.setEnd_time(endTime);
            createShareTaskQuery.setMount_link(mountLink);
            createShareTaskQuery.setPublish_type(publishType);
            createShareTaskQuery.setTags(tags);
            createShareTaskQuery.setRule_type(ruleType);
            createShareTaskQuery.setInteract_rules(interactRules);
            createShareTaskQuery.setTarget_count(targetCount);
            createShareTaskQuery.setTask_type(taskType);
            createShareTaskQuery.setMax_count(maxCount);
            createShareTaskQuery.setTenantId(tenantId);
            createShareTaskQuery.setClientKey(clientKey);
            return createShareTaskQuery;
        }
    }


    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public String getMount_link() {
        return mount_link;
    }

    public void setMount_link(String mount_link) {
        this.mount_link = mount_link;
    }

    public List<Integer> getPublish_type() {
        return publish_type;
    }

    public void setPublish_type(List<Integer> publish_type) {
        this.publish_type = publish_type;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Integer getRule_type() {
        return rule_type;
    }

    public void setRule_type(Integer rule_type) {
        this.rule_type = rule_type;
    }

    public Long getTarget_count() {
        return target_count;
    }

    public void setTarget_count(Long target_count) {
        this.target_count = target_count;
    }

    public Integer getTask_type() {
        return task_type;
    }

    public void setTask_type(Integer task_type) {
        this.task_type = task_type;
    }

    public Map<String, InteractRule> getInteract_rules() {
        return interact_rules;
    }

    public void setInteract_rules(Map<String, InteractRule> interact_rules) {
        this.interact_rules = interact_rules;
    }

    public Long getMax_count() {
        return max_count;
    }

    public void setMax_count(Long max_count) {
        this.max_count = max_count;
    }
}
