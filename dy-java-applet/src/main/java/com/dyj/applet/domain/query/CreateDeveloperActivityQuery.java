package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 创建开发者接口发券活动请求值
 */
public class CreateDeveloperActivityQuery extends BaseQuery {

    /**
     * 活动名称 选填
     */
    private String activity_name;
    /**
     * 小程序ID
     */
    private String app_id;
    /**
     * 券模板ID
     */
    private String coupon_meta_id;
    /**
     * 计划发放数量
     */
    private Long coupon_stock_number;
    /**
     * 开发者自定义的唯一键，保证计划创建幂等
     */
    private String merchant_activity_id;

    public String getActivity_name() {
        return activity_name;
    }

    public CreateDeveloperActivityQuery setActivity_name(String activity_name) {
        this.activity_name = activity_name;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public CreateDeveloperActivityQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public CreateDeveloperActivityQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public Long getCoupon_stock_number() {
        return coupon_stock_number;
    }

    public CreateDeveloperActivityQuery setCoupon_stock_number(Long coupon_stock_number) {
        this.coupon_stock_number = coupon_stock_number;
        return this;
    }

    public String getMerchant_activity_id() {
        return merchant_activity_id;
    }

    public CreateDeveloperActivityQuery setMerchant_activity_id(String merchant_activity_id) {
        this.merchant_activity_id = merchant_activity_id;
        return this;
    }

    public static CreateDeveloperActivityQueryBuilder builder() {
        return new CreateDeveloperActivityQueryBuilder();
    }


    public static final class CreateDeveloperActivityQueryBuilder {
        private String activity_name;
        private String app_id;
        private String coupon_meta_id;
        private Long coupon_stock_number;
        private String merchant_activity_id;
        private Integer tenantId;
        private String clientKey;

        private CreateDeveloperActivityQueryBuilder() {
        }

        public CreateDeveloperActivityQueryBuilder activityName(String activityName) {
            this.activity_name = activityName;
            return this;
        }

        public CreateDeveloperActivityQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public CreateDeveloperActivityQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public CreateDeveloperActivityQueryBuilder couponStockNumber(Long couponStockNumber) {
            this.coupon_stock_number = couponStockNumber;
            return this;
        }

        public CreateDeveloperActivityQueryBuilder merchantActivityId(String merchantActivityId) {
            this.merchant_activity_id = merchantActivityId;
            return this;
        }

        public CreateDeveloperActivityQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateDeveloperActivityQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateDeveloperActivityQuery build() {
            CreateDeveloperActivityQuery createDeveloperActivityQuery = new CreateDeveloperActivityQuery();
            createDeveloperActivityQuery.setActivity_name(activity_name);
            createDeveloperActivityQuery.setApp_id(app_id);
            createDeveloperActivityQuery.setCoupon_meta_id(coupon_meta_id);
            createDeveloperActivityQuery.setCoupon_stock_number(coupon_stock_number);
            createDeveloperActivityQuery.setMerchant_activity_id(merchant_activity_id);
            createDeveloperActivityQuery.setTenantId(tenantId);
            createDeveloperActivityQuery.setClientKey(clientKey);
            return createDeveloperActivityQuery;
        }
    }
}
