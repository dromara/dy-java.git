package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.DeliveryPrepareCertificate;

import java.util.List;

/**
 * 验券准备返回值
 */
public class DeliveryPrepareVo {

    /**
     * 错误码描述
     */
    private String description;
    /**
     * 错误码，0为成功
     */
    private Integer error_code;
    /**
     * 交易系统单号
     */
    private String order_id;
    /**
     * 外部单号，开发者系统的交易单号
     */
    private String out_order_no;
    /**
     * 一次验券的标识，在验券接口传入
     */
    private String verify_token;
    /**
     * 可用券列表
     */
    private List<DeliveryPrepareCertificate> certificates;

    public String getDescription() {
        return description;
    }

    public DeliveryPrepareVo setDescription(String description) {
        this.description = description;
        return this;
    }

    public Integer getError_code() {
        return error_code;
    }

    public DeliveryPrepareVo setError_code(Integer error_code) {
        this.error_code = error_code;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public DeliveryPrepareVo setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public DeliveryPrepareVo setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getVerify_token() {
        return verify_token;
    }

    public DeliveryPrepareVo setVerify_token(String verify_token) {
        this.verify_token = verify_token;
        return this;
    }

    public List<DeliveryPrepareCertificate> getCertificates() {
        return certificates;
    }

    public DeliveryPrepareVo setCertificates(List<DeliveryPrepareCertificate> certificates) {
        this.certificates = certificates;
        return this;
    }
}

