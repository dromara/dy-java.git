package com.dyj.applet.domain.vo;

public class QueryAdmissibleAuthVo {

    /**
     * <p>是否可准入</p><p>1-满足条件可准入</p><p>2-不满足准入条件</p>
     */
    private Byte admissibility;
    /**
     * <p>准入token</p> 选填
     */
    private String admissible_token;
    /**
     * <p>token过期时间，单位秒，多少秒后不可用</p> 选填
     */
    private Long expire_seconds;

    public Byte getAdmissibility() {
        return admissibility;
    }

    public QueryAdmissibleAuthVo setAdmissibility(Byte admissibility) {
        this.admissibility = admissibility;
        return this;
    }

    public String getAdmissible_token() {
        return admissible_token;
    }

    public QueryAdmissibleAuthVo setAdmissible_token(String admissible_token) {
        this.admissible_token = admissible_token;
        return this;
    }

    public Long getExpire_seconds() {
        return expire_seconds;
    }

    public QueryAdmissibleAuthVo setExpire_seconds(Long expire_seconds) {
        this.expire_seconds = expire_seconds;
        return this;
    }
}
