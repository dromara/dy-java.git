package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QueryIndustryCpsDeliveryInfo;
import com.dyj.applet.domain.QueryIndustryCpsInfo;
import com.dyj.applet.domain.QueryIndustryCpsPaymentInfo;
import com.dyj.applet.domain.QueryIndustryCpsRefundInfo;
import com.dyj.common.domain.vo.BaseVo;

/**
 * CPS 订单相关信息
 */
public class QueryIndustryCpsVo extends BaseVo {

    /**
     * 抖音开平侧订单号
     */
    private String order_id;
    /**
     * 开发者侧订单号，与 order_id一一对应
     */
    private String out_order_no;
    /**
     * CPS 相关信息
     */
    private QueryIndustryCpsInfo cps_info;
    /**
     * CPS 订单已核销记录信息
     */
    private QueryIndustryCpsDeliveryInfo delivery_info;
    /**
     * CPS 订单支付相关信息
     */
    private QueryIndustryCpsPaymentInfo payment_info;
    /**
     * CPS 订单已退款记录信息
     */
    private QueryIndustryCpsRefundInfo refund_info;

    public String getOrder_id() {
        return order_id;
    }

    public QueryIndustryCpsVo setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QueryIndustryCpsVo setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public QueryIndustryCpsInfo getCps_info() {
        return cps_info;
    }

    public QueryIndustryCpsVo setCps_info(QueryIndustryCpsInfo cps_info) {
        this.cps_info = cps_info;
        return this;
    }

    public QueryIndustryCpsDeliveryInfo getDelivery_info() {
        return delivery_info;
    }

    public QueryIndustryCpsVo setDelivery_info(QueryIndustryCpsDeliveryInfo delivery_info) {
        this.delivery_info = delivery_info;
        return this;
    }

    public QueryIndustryCpsPaymentInfo getPayment_info() {
        return payment_info;
    }

    public QueryIndustryCpsVo setPayment_info(QueryIndustryCpsPaymentInfo payment_info) {
        this.payment_info = payment_info;
        return this;
    }

    public QueryIndustryCpsRefundInfo getRefund_info() {
        return refund_info;
    }

    public QueryIndustryCpsVo setRefund_info(QueryIndustryCpsRefundInfo refund_info) {
        this.refund_info = refund_info;
        return this;
    }
}
