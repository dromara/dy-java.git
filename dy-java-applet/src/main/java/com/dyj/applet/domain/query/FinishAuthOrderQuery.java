package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class FinishAuthOrderQuery extends BaseQuery {

    /**
     * 信用单平台订单号，长度<=64byte
     *
     * 该字段通过 /api/trade_auth/v1/developer/create_auth_order/ 接口返回的 data.auth_order_id 字段获取
     */
    private String auth_order_id;

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public FinishAuthOrderQuery setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public static FinishAuthOrderQueryBuilder builder() {
        return new FinishAuthOrderQueryBuilder();
    }

    public static final class FinishAuthOrderQueryBuilder {
        private String auth_order_id;
        private Integer tenantId;
        private String clientKey;

        private FinishAuthOrderQueryBuilder() {
        }

        public FinishAuthOrderQueryBuilder authOrderId(String authOrderId) {
            this.auth_order_id = authOrderId;
            return this;
        }

        public FinishAuthOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public FinishAuthOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public FinishAuthOrderQuery build() {
            FinishAuthOrderQuery finishAuthOrderQuery = new FinishAuthOrderQuery();
            finishAuthOrderQuery.setAuth_order_id(auth_order_id);
            finishAuthOrderQuery.setTenantId(tenantId);
            finishAuthOrderQuery.setClientKey(clientKey);
            return finishAuthOrderQuery;
        }
    }
}
