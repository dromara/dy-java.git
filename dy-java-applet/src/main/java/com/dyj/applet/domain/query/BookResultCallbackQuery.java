package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class BookResultCallbackQuery extends BaseQuery {

    /**
     * 预约单id，len(book_id) <= 64 byte
     */
    private String book_id;

    /**
     * 预约结果
     * 1：成功
     * 2：失败
     */
    private Integer result;

    /**
     * 结果描述/备注信息
     */
    private String msg;

    /**
     * 接单方式
     * 1：电话接单
     * 2：商家后台接单
     */
    private Integer accept_type;

    public String getBook_id() {
        return book_id;
    }

    public BookResultCallbackQuery setBook_id(String book_id) {
        this.book_id = book_id;
        return this;
    }

    public Integer getResult() {
        return result;
    }

    public BookResultCallbackQuery setResult(Integer result) {
        this.result = result;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public BookResultCallbackQuery setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Integer getAccept_type() {
        return accept_type;
    }

    public BookResultCallbackQuery setAccept_type(Integer accept_type) {
        this.accept_type = accept_type;
        return this;
    }

    public static BookResultCallbackQueryBuilder builder(){
        return new BookResultCallbackQueryBuilder();
    }

    public static final class BookResultCallbackQueryBuilder {
        private String book_id;
        private Integer result;
        private String msg;
        private Integer accept_type;
        private Integer tenantId;
        private String clientKey;

        private BookResultCallbackQueryBuilder() {
        }

        public BookResultCallbackQueryBuilder bookId(String bookId) {
            this.book_id = bookId;
            return this;
        }

        public BookResultCallbackQueryBuilder result(Integer result) {
            this.result = result;
            return this;
        }

        public BookResultCallbackQueryBuilder msg(String msg) {
            this.msg = msg;
            return this;
        }

        public BookResultCallbackQueryBuilder acceptType(Integer acceptType) {
            this.accept_type = acceptType;
            return this;
        }

        public BookResultCallbackQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public BookResultCallbackQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public BookResultCallbackQuery build() {
            BookResultCallbackQuery bookResultCallbackQuery = new BookResultCallbackQuery();
            bookResultCallbackQuery.setBook_id(book_id);
            bookResultCallbackQuery.setResult(result);
            bookResultCallbackQuery.setMsg(msg);
            bookResultCallbackQuery.setAccept_type(accept_type);
            bookResultCallbackQuery.setTenantId(tenantId);
            bookResultCallbackQuery.setClientKey(clientKey);
            return bookResultCallbackQuery;
        }
    }
}
