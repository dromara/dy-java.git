package com.dyj.applet.domain;

/**
 * 次卡信息，仅次卡类型会返回
 */
public class QueryIndustryItemOrderInfoTimesCardInfo {

    /**
     * 商品单次现价，单位[分]
     */
    private Integer actual_amount_once;
    /**
     * 退款次数(退款中+退款成功)
     */
    private Integer refund_times;
    /**
     * 总计次数
     */
    private Integer total_times;
    /**
     * 可使用次数/剩余次数
     */
    private Integer usable_times;

    public Integer getActual_amount_once() {
        return actual_amount_once;
    }

    public QueryIndustryItemOrderInfoTimesCardInfo setActual_amount_once(Integer actual_amount_once) {
        this.actual_amount_once = actual_amount_once;
        return this;
    }

    public Integer getRefund_times() {
        return refund_times;
    }

    public QueryIndustryItemOrderInfoTimesCardInfo setRefund_times(Integer refund_times) {
        this.refund_times = refund_times;
        return this;
    }

    public Integer getTotal_times() {
        return total_times;
    }

    public QueryIndustryItemOrderInfoTimesCardInfo setTotal_times(Integer total_times) {
        this.total_times = total_times;
        return this;
    }

    public Integer getUsable_times() {
        return usable_times;
    }

    public QueryIndustryItemOrderInfoTimesCardInfo setUsable_times(Integer usable_times) {
        this.usable_times = usable_times;
        return this;
    }
}
