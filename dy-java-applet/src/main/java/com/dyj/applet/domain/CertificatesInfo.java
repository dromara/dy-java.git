package com.dyj.applet.domain;

public class CertificatesInfo {

    /**
     * 券ID
     */
    private String certificate_id;

    /**
     * 券相关的sku商品信息。详见下方sku_info参数字段说明
     */
    private CertificatesSkuInfo sku_info;

    /**
     * 券相关金额信息。详见下方amount参数字段说明
     */
    private UserCertificatesAmount amount;

    /**
     * 券有效时间，秒级
     */
    private Long expire_time;

    /**
     * 券有效开始时间，秒级
     */
    private Long start_time;

    /**
     * 三方码则返回明文code，抖音码不返回。
     * 选填
     */
    private String code;

    /**
     * 次卡信息。详见下方times_card_info参数字段说明
     */
    private TimesCardInfo times_card_info;

    public String getCertificate_id() {
        return certificate_id;
    }

    public CertificatesInfo setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
        return this;
    }

    public CertificatesSkuInfo getSku_info() {
        return sku_info;
    }

    public CertificatesInfo setSku_info(CertificatesSkuInfo sku_info) {
        this.sku_info = sku_info;
        return this;
    }

    public UserCertificatesAmount getAmount() {
        return amount;
    }

    public CertificatesInfo setAmount(UserCertificatesAmount amount) {
        this.amount = amount;
        return this;
    }

    public Long getExpire_time() {
        return expire_time;
    }

    public CertificatesInfo setExpire_time(Long expire_time) {
        this.expire_time = expire_time;
        return this;
    }

    public Long getStart_time() {
        return start_time;
    }

    public CertificatesInfo setStart_time(Long start_time) {
        this.start_time = start_time;
        return this;
    }

    public String getCode() {
        return code;
    }

    public CertificatesInfo setCode(String code) {
        this.code = code;
        return this;
    }

    public TimesCardInfo getTimes_card_info() {
        return times_card_info;
    }

    public CertificatesInfo setTimes_card_info(TimesCardInfo times_card_info) {
        this.times_card_info = times_card_info;
        return this;
    }
}
