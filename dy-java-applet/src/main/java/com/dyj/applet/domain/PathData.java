package com.dyj.applet.domain;

public class PathData {

    /**
     * 跳转路径
     */
    private String path;

    /**
     * path类型, 1-到店 2-到家 3-核销工具 4-详情页
     */
    private Integer path_type;

    public String getPath() {
        return path;
    }

    public PathData setPath(String path) {
        this.path = path;
        return this;
    }

    public Integer getPath_type() {
        return path_type;
    }

    public PathData setPath_type(Integer path_type) {
        this.path_type = path_type;
        return this;
    }
}
