package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-05-06 11:13
 **/
public class OperateGoodsProductQuery extends BaseQuery {
    /**
     *
     * 商品id(若有第三方id可不传）
     */
    private String product_id;

    /**
     * 商品第三方id
     */
    private String out_id;
    /**
     * 1-上线 2-下线
     */
    private Integer op_type;

    public static OperateGoodsProductQueryBuilder build() {
        return new OperateGoodsProductQueryBuilder();
    }

    public static final class OperateGoodsProductQueryBuilder {
        private String productId;
        private String outId;
        private Integer opType;
        private Integer tenantId;
        private String clientKey;

        public OperateGoodsProductQueryBuilder productId(String productId) {
            this.productId = productId;
            return this;
        }

        public OperateGoodsProductQueryBuilder outId(String outId) {
            this.outId = outId;
            return this;
        }

        public OperateGoodsProductQueryBuilder opType(Integer opType) {
            this.opType = opType;
            return this;
        }

        public OperateGoodsProductQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public OperateGoodsProductQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public OperateGoodsProductQuery build() {
            OperateGoodsProductQuery operateGoodsProductQuery = new OperateGoodsProductQuery();
            operateGoodsProductQuery.setProduct_id(productId);
            operateGoodsProductQuery.setOut_id(outId);
            operateGoodsProductQuery.setOp_type(opType);
            operateGoodsProductQuery.setTenantId(tenantId);
            operateGoodsProductQuery.setClientKey(clientKey);
            return operateGoodsProductQuery;
        }
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }

    public Integer getOp_type() {
        return op_type;
    }

    public void setOp_type(Integer op_type) {
        this.op_type = op_type;
    }
}
