package com.dyj.applet.domain.query;

import com.dyj.applet.domain.CreatePayOrderFeeDetail;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class CreatePayOrderQuery extends BaseQuery {

    /**
     * <p>信用单平台订单号，长度<=64byte</p>
     */
    private String auth_order_id;
    /**
     * <p>下单结果回调地址，https开头，长度<=512byte</p> 选填
     */
    private String notify_url;
    /**
     * <p>扣款单开发者的单号，长度<=64byte</p>
     */
    private String out_pay_order_no;
    /**
     * <p>扣款金额，总的扣款金额不能超过押金，扣款金额须>0</p>
     */
    private Long total_amount;
    /**
     * <p>扣款付费项目详情，长度不能大于20</p>
     */
    private List<CreatePayOrderFeeDetail> fee_detail_list;

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public CreatePayOrderQuery setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public CreatePayOrderQuery setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOut_pay_order_no() {
        return out_pay_order_no;
    }

    public CreatePayOrderQuery setOut_pay_order_no(String out_pay_order_no) {
        this.out_pay_order_no = out_pay_order_no;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public CreatePayOrderQuery setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public List<CreatePayOrderFeeDetail> getFee_detail_list() {
        return fee_detail_list;
    }

    public CreatePayOrderQuery setFee_detail_list(List<CreatePayOrderFeeDetail> fee_detail_list) {
        this.fee_detail_list = fee_detail_list;
        return this;
    }

    public static CreatePayOrderQueryBuilder builder() {
        return new CreatePayOrderQueryBuilder();
    }

    public static final class CreatePayOrderQueryBuilder {
        private String auth_order_id;
        private String notify_url;
        private String out_pay_order_no;
        private Long total_amount;
        private List<CreatePayOrderFeeDetail> fee_detail_list;
        private Integer tenantId;
        private String clientKey;

        private CreatePayOrderQueryBuilder() {
        }

        public CreatePayOrderQueryBuilder authOrderId(String authOrderId) {
            this.auth_order_id = authOrderId;
            return this;
        }

        public CreatePayOrderQueryBuilder notifyUrl(String notifyUrl) {
            this.notify_url = notifyUrl;
            return this;
        }

        public CreatePayOrderQueryBuilder outPayOrderNo(String outPayOrderNo) {
            this.out_pay_order_no = outPayOrderNo;
            return this;
        }

        public CreatePayOrderQueryBuilder totalAmount(Long totalAmount) {
            this.total_amount = totalAmount;
            return this;
        }

        public CreatePayOrderQueryBuilder feeDetailList(List<CreatePayOrderFeeDetail> feeDetailList) {
            this.fee_detail_list = feeDetailList;
            return this;
        }

        public CreatePayOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreatePayOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreatePayOrderQuery build() {
            CreatePayOrderQuery createPayOrderQuery = new CreatePayOrderQuery();
            createPayOrderQuery.setAuth_order_id(auth_order_id);
            createPayOrderQuery.setNotify_url(notify_url);
            createPayOrderQuery.setOut_pay_order_no(out_pay_order_no);
            createPayOrderQuery.setTotal_amount(total_amount);
            createPayOrderQuery.setFee_detail_list(fee_detail_list);
            createPayOrderQuery.setTenantId(tenantId);
            createPayOrderQuery.setClientKey(clientKey);
            return createPayOrderQuery;
        }
    }
}
