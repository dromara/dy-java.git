package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.PushDeliveryVerifyResult;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * 推送核销状态返回值
 */
public class PushDeliveryVo extends BaseVo {

    /**
     * 数组，每个券的验券结果
     */
    private List<PushDeliveryVerifyResult> verify_results;

    public List<PushDeliveryVerifyResult> getVerify_results() {
        return verify_results;
    }

    public PushDeliveryVo setVerify_results(List<PushDeliveryVerifyResult> verify_results) {
        this.verify_results = verify_results;
        return this;
    }
}
