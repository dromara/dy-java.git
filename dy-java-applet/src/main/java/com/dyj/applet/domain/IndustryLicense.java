package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 13:42
 **/
public class IndustryLicense {

    /**
     * 商家营业执行业许可证外部id
     */
    private String ext_id;

    /**
     * 商家行业许可证链接
     */
    private String url;

    public static IndustryLicenseBuilder builder() {
        return new IndustryLicenseBuilder();
    }

    public static class IndustryLicenseBuilder {
        private String extId;

        private String url;

        public IndustryLicenseBuilder extId(String extId) {
            this.extId = extId;
            return this;
        }

        public IndustryLicenseBuilder url(String url) {
            this.url = url;
            return this;
        }

        public IndustryLicense build() {
            IndustryLicense industryLicense = new IndustryLicense();
            industryLicense.setExt_id(extId);
            industryLicense.setUrl(url);
            return industryLicense;
        }
    }

    public String getExt_id() {
        return ext_id;
    }

    public void setExt_id(String ext_id) {
        this.ext_id = ext_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
