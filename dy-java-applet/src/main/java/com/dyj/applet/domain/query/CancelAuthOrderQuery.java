package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class CancelAuthOrderQuery extends BaseQuery {

    /**
     * 信用单平台订单号，长度<=64byte
     */
    private String auth_order_id;

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public CancelAuthOrderQuery setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public static CancelAuthOrderQueryBuilder builder() {
        return new CancelAuthOrderQueryBuilder();
    }

    public static final class CancelAuthOrderQueryBuilder {
        private String auth_order_id;
        private Integer tenantId;
        private String clientKey;

        private CancelAuthOrderQueryBuilder() {
        }

        public CancelAuthOrderQueryBuilder authOrderId(String authOrderId) {
            this.auth_order_id = authOrderId;
            return this;
        }

        public CancelAuthOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CancelAuthOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CancelAuthOrderQuery build() {
            CancelAuthOrderQuery cancelAuthOrderQuery = new CancelAuthOrderQuery();
            cancelAuthOrderQuery.setAuth_order_id(auth_order_id);
            cancelAuthOrderQuery.setTenantId(tenantId);
            cancelAuthOrderQuery.setClientKey(clientKey);
            return cancelAuthOrderQuery;
        }
    }
}
