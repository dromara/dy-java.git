package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 查询分账请求值
 */
public class QuerySettleQuery extends BaseQuery {

    /**
     * 小程序id
     */
    private String app_id;
    /**
     * <p>抖音开平侧订单 id，长度 <= 64字节</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">settle_id，order_id，out_settle_no和out_order_no4 个参数选填一个，查询优先级：settle_id > order_id > out_settle_no > out_order_no。例如：请求填写了settle_id 和 order_id，服务只会按 settle_id 来查询，忽略 order_id 。如果未查询到结果，会返回空数组。</li></ul> 选填
     */
    private String order_id;
    /**
     * <p>开发者侧订单 id，长度 <= 64 字节</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">settle_id，order_id，out_settle_no和out_order_no4 个参数选填一个，查询优先级：settle_id > order_id > out_settle_no > out_order_no。例如：请求填写了settle_id 和 order_id，服务只会按 settle_id 来查询，忽略 order_id 。如果未查询到结果，会返回空数组。</li></ul> 选填
     */
    private String out_order_no;
    /**
     * <p>开发者侧分账单 id，长度 <= 64字节</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">settle_id，order_id，out_settle_no和out_order_no4 个参数选填一个，查询优先级：settle_id > order_id > out_settle_no > out_order_no。例如：请求填写了settle_id 和 order_id，服务只会按 settle_id 来查询，忽略 order_id 。如果未查询到结果，会返回空数组。</li></ul> 选填
     */
    private String out_settle_no;
    /**
     * <p>小程序侧分账单id</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">settle_id，order_id，out_settle_no和out_order_no4 个参数选填一个，查询优先级：settle_id > order_id > out_settle_no > out_order_no。例如：请求填写了settle_id 和 order_id，服务只会按 settle_id 来查询，忽略 order_id 。如果未查询到结果，会返回空数组。</li></ul> 选填
     */
    private String settle_id;

    public String getApp_id() {
        return app_id;
    }

    public QuerySettleQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public QuerySettleQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QuerySettleQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getOut_settle_no() {
        return out_settle_no;
    }

    public QuerySettleQuery setOut_settle_no(String out_settle_no) {
        this.out_settle_no = out_settle_no;
        return this;
    }

    public String getSettle_id() {
        return settle_id;
    }

    public QuerySettleQuery setSettle_id(String settle_id) {
        this.settle_id = settle_id;
        return this;
    }

    public static QuerySettleQueryBuilder builder() {
        return new QuerySettleQueryBuilder();
    }

    public static final class QuerySettleQueryBuilder {
        private String app_id;
        private String order_id;
        private String out_order_no;
        private String out_settle_no;
        private String settle_id;
        private Integer tenantId;
        private String clientKey;

        private QuerySettleQueryBuilder() {
        }

        public QuerySettleQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public QuerySettleQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public QuerySettleQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public QuerySettleQueryBuilder outSettleNo(String outSettleNo) {
            this.out_settle_no = outSettleNo;
            return this;
        }

        public QuerySettleQueryBuilder settleId(String settleId) {
            this.settle_id = settleId;
            return this;
        }

        public QuerySettleQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QuerySettleQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QuerySettleQuery build() {
            QuerySettleQuery querySettleQuery = new QuerySettleQuery();
            querySettleQuery.setApp_id(app_id);
            querySettleQuery.setOrder_id(order_id);
            querySettleQuery.setOut_order_no(out_order_no);
            querySettleQuery.setOut_settle_no(out_settle_no);
            querySettleQuery.setSettle_id(settle_id);
            querySettleQuery.setTenantId(tenantId);
            querySettleQuery.setClientKey(clientKey);
            return querySettleQuery;
        }
    }
}
