package com.dyj.applet.domain;

public class TextAntiDirtContent {

    /**
     * 检测的文本内容
     */
    private String content;

    public String getContent() {
        return content;
    }

    public TextAntiDirtContent setContent(String content) {
        this.content = content;
        return this;
    }
}
