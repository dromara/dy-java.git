package com.dyj.applet.domain;

import java.util.List;

public class AnalysisUserRetentionData {

    private Long active_user_num;

    private String time;

    private List<AnalysisUserRetentionRate> retention_rate_list;

    public Long getActive_user_num() {
        return active_user_num;
    }

    public void setActive_user_num(Long active_user_num) {
        this.active_user_num = active_user_num;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<AnalysisUserRetentionRate> getRetention_rate_list() {
        return retention_rate_list;
    }

    public void setRetention_rate_list(List<AnalysisUserRetentionRate> retention_rate_list) {
        this.retention_rate_list = retention_rate_list;
    }

    public static class AnalysisUserRetentionRate {
        /**
         * x天后，x为1,2,3,4,5,6,7,14,30
         */
        private Integer day;

        /**
         * x天后的留存率
         */
        private Double rate;

        public Integer getDay() {
            return day;
        }

        public void setDay(Integer day) {
            this.day = day;
        }

        public Double getRate() {
            return rate;
        }

        public void setRate(Double rate) {
            this.rate = rate;
        }
    }
}
