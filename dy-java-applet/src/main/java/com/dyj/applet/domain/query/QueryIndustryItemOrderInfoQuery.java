package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * 查询券状态信息请求值
 */
public class QueryIndustryItemOrderInfoQuery extends BaseQuery {

    /**
     * 抖音开平内部交易订单号，该单号通过预下单回调传给开发者服务，长度 < 64byte。
     */
    private String order_id;
    /**
     * 交易系统订单号下的商品单号列表。 最多支持 20 个商品单传入，超过将报错。如不传，将返回此订单下的所有子单。
     */
    private List<String> item_order_id_list;

    public String getOrder_id() {
        return order_id;
    }

    public QueryIndustryItemOrderInfoQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public List<String> getItem_order_id_list() {
        return item_order_id_list;
    }

    public QueryIndustryItemOrderInfoQuery setItem_order_id_list(List<String> item_order_id_list) {
        this.item_order_id_list = item_order_id_list;
        return this;
    }

    public static QueryIndustryItemOrderInfoQueryBuilder builder() {
        return new QueryIndustryItemOrderInfoQueryBuilder();
    }

    public static final class QueryIndustryItemOrderInfoQueryBuilder {
        private String order_id;
        private List<String> item_order_id_list;
        private Integer tenantId;
        private String clientKey;

        private QueryIndustryItemOrderInfoQueryBuilder() {
        }

        public QueryIndustryItemOrderInfoQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public QueryIndustryItemOrderInfoQueryBuilder itemOrderIdList(List<String> itemOrderIdList) {
            this.item_order_id_list = itemOrderIdList;
            return this;
        }

        public QueryIndustryItemOrderInfoQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryIndustryItemOrderInfoQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryIndustryItemOrderInfoQuery build() {
            QueryIndustryItemOrderInfoQuery queryIndustryItemOrderInfoQuery = new QueryIndustryItemOrderInfoQuery();
            queryIndustryItemOrderInfoQuery.setOrder_id(order_id);
            queryIndustryItemOrderInfoQuery.setItem_order_id_list(item_order_id_list);
            queryIndustryItemOrderInfoQuery.setTenantId(tenantId);
            queryIndustryItemOrderInfoQuery.setClientKey(clientKey);
            return queryIndustryItemOrderInfoQuery;
        }
    }
}
