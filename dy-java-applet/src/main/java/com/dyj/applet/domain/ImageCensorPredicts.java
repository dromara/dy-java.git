package com.dyj.applet.domain;

public class ImageCensorPredicts {

    /**
     *  选填
     */
    private Boolean hit;
    /**
     *  选填
     */
    private String model_name;

    public Boolean getHit() {
        return hit;
    }

    public ImageCensorPredicts setHit(Boolean hit) {
        this.hit = hit;
        return this;
    }

    public String getModel_name() {
        return model_name;
    }

    public ImageCensorPredicts setModel_name(String model_name) {
        this.model_name = model_name;
        return this;
    }
}
