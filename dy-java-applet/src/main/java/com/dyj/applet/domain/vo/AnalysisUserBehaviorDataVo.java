package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisUserBehavior;
import com.dyj.applet.domain.AnalysisUserBehaviorSum;

import java.util.List;

public class AnalysisUserBehaviorDataVo {

    /**
     * 行为分析数据
     */
    private List<AnalysisUserBehavior> behaviors;

    private AnalysisUserBehaviorSum sum;

    public List<AnalysisUserBehavior> getBehaviors() {
        return behaviors;
    }

    public void setBehaviors(List<AnalysisUserBehavior> behaviors) {
        this.behaviors = behaviors;
    }

    public AnalysisUserBehaviorSum getSum() {
        return sum;
    }

    public void setSum(AnalysisUserBehaviorSum sum) {
        this.sum = sum;
    }
}
