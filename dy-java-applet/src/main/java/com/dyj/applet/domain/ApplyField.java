package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-07-17 10:02
 **/
public class ApplyField {

    /**
     * 申请字段名称
     */
    private String apply_field_name;
    /**
     * 申请字段描述
     */
    private String apply_field_desc;
    /**
     * 字段是否必填
     */
    private Boolean apply_field_is_required;
    /**
     * 申请字段key值，需要使用到能力申请接口
     */
    private String apply_field_key;
    /**
     * 字段类型：10-文本20-图片30-文件其中图片和文件资源，使用上传素材接口获取到的路径
     */
    private Integer apply_field_value_type;

    public String getApply_field_name() {
        return apply_field_name;
    }

    public void setApply_field_name(String apply_field_name) {
        this.apply_field_name = apply_field_name;
    }

    public String getApply_field_desc() {
        return apply_field_desc;
    }

    public void setApply_field_desc(String apply_field_desc) {
        this.apply_field_desc = apply_field_desc;
    }

    public Boolean getApply_field_is_required() {
        return apply_field_is_required;
    }

    public void setApply_field_is_required(Boolean apply_field_is_required) {
        this.apply_field_is_required = apply_field_is_required;
    }

    public String getApply_field_key() {
        return apply_field_key;
    }

    public void setApply_field_key(String apply_field_key) {
        this.apply_field_key = apply_field_key;
    }

    public Integer getApply_field_value_type() {
        return apply_field_value_type;
    }

    public void setApply_field_value_type(Integer apply_field_value_type) {
        this.apply_field_value_type = apply_field_value_type;
    }
}
