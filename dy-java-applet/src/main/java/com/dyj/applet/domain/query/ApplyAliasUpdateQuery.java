package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class ApplyAliasUpdateQuery extends BaseQuery {

    /**
     * 修改后小程序别名
     */
    private String after_alias;

    /**
     * 修改前小程序别名
     */
    private String before_alias;

    public static ApplyAliasUpdateQueryBuilder builder() {
        return new ApplyAliasUpdateQueryBuilder();
    }

    public static class ApplyAliasUpdateQueryBuilder {
        /**
         * 修改后小程序别名
         */
        private String afterAlias;

        /**
         * 修改前小程序别名
         */
        private String beforeAlias;

        private Integer tenantId;

        private String clientKey;

        public ApplyAliasUpdateQueryBuilder afterAlias(String afterAlias) {
            this.afterAlias = afterAlias;
            return this;
        }

        public ApplyAliasUpdateQueryBuilder beforeAlias(String beforeAlias) {
            this.beforeAlias = beforeAlias;
            return this;
        }

        public ApplyAliasUpdateQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public ApplyAliasUpdateQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public ApplyAliasUpdateQuery build() {
            ApplyAliasUpdateQuery applyAliasUpdateQuery = new ApplyAliasUpdateQuery();
            applyAliasUpdateQuery.setAfter_alias(afterAlias);
            applyAliasUpdateQuery.setBefore_alias(beforeAlias);
            applyAliasUpdateQuery.setTenantId(tenantId);
            applyAliasUpdateQuery.setClientKey(clientKey);
            return applyAliasUpdateQuery;
        }
    }

    public String getAfter_alias() {
        return after_alias;
    }

    public void setAfter_alias(String after_alias) {
        this.after_alias = after_alias;
    }

    public String getBefore_alias() {
        return before_alias;
    }

    public void setBefore_alias(String before_alias) {
        this.before_alias = before_alias;
    }
}
