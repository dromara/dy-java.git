package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-06-12 14:10
 **/
public class CreateDoudianAppVo {

    /**
     * 抖店应用appid
     */
    private String doudian_appid;

    public String getDoudian_appid() {
        return doudian_appid;
    }

    public void setDoudian_appid(String doudian_appid) {
        this.doudian_appid = doudian_appid;
    }
}
