package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

public class UploadImageMaterialQuery extends BaseTransactionMerchantQuery {

    /**
     * 图片下载链接
     */
    private String image_material_url;

    public String getImage_material_url() {
        return image_material_url;
    }

    public UploadImageMaterialQuery setImage_material_url(String image_material_url) {
        this.image_material_url = image_material_url;
        return this;
    }

    public static UploadImageMaterialQueryBuilder builder() {
        return new UploadImageMaterialQueryBuilder();
    }

    public static final class UploadImageMaterialQueryBuilder {
        private String image_material_url;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType;
        private Integer tenantId;
        private String clientKey;

        private UploadImageMaterialQueryBuilder() {
        }

        public UploadImageMaterialQueryBuilder imageMaterialUrl(String imageMaterialUrl) {
            this.image_material_url = imageMaterialUrl;
            return this;
        }

        public UploadImageMaterialQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public UploadImageMaterialQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public UploadImageMaterialQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public UploadImageMaterialQuery build() {
            UploadImageMaterialQuery uploadImageMaterialQuery = new UploadImageMaterialQuery();
            uploadImageMaterialQuery.setImage_material_url(image_material_url);
            uploadImageMaterialQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            uploadImageMaterialQuery.setTenantId(tenantId);
            uploadImageMaterialQuery.setClientKey(clientKey);
            return uploadImageMaterialQuery;
        }
    }
}
