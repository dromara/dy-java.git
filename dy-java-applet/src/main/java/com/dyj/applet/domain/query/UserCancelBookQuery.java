package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

import java.util.List;

public class UserCancelBookQuery extends UserInfoQuery {

    /**
     * 预约单id，len(book_id) <= 64 byte
     */
    private String book_id;

    /**
     * 取消原因
     */
    private List<String> cancel_reason;

    public String getBook_id() {
        return book_id;
    }

    public UserCancelBookQuery setBook_id(String book_id) {
        this.book_id = book_id;
        return this;
    }

    public List<String> getCancel_reason() {
        return cancel_reason;
    }

    public UserCancelBookQuery setCancel_reason(List<String> cancel_reason) {
        this.cancel_reason = cancel_reason;
        return this;
    }

    public static UserCancelBookQueryBuilder builder() {
        return new UserCancelBookQueryBuilder();
    }

    public static final class UserCancelBookQueryBuilder {
        private String book_id;
        private List<String> cancel_reason;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private UserCancelBookQueryBuilder() {
        }

        public UserCancelBookQueryBuilder bookId(String bookId) {
            this.book_id = bookId;
            return this;
        }

        public UserCancelBookQueryBuilder cancelReason(List<String> cancelReason) {
            this.cancel_reason = cancelReason;
            return this;
        }

        public UserCancelBookQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public UserCancelBookQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public UserCancelBookQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public UserCancelBookQuery build() {
            UserCancelBookQuery userCancelBookQuery = new UserCancelBookQuery();
            userCancelBookQuery.setBook_id(book_id);
            userCancelBookQuery.setCancel_reason(cancel_reason);
            userCancelBookQuery.setOpen_id(open_id);
            userCancelBookQuery.setTenantId(tenantId);
            userCancelBookQuery.setClientKey(clientKey);
            return userCancelBookQuery;
        }
    }
}
