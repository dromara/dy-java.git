package com.dyj.applet.domain.query;

import com.dyj.applet.domain.IndustryOrderGoodsInfo;
import com.dyj.applet.domain.IndustryOrderOrderEntrySchema;
import com.dyj.common.domain.query.UserInfoQuery;

import java.util.List;

/**
 * 生活服务交易系统->预下单->开发者发起下单查询值
 */
public class PreCreateIndustryOrderQuery extends UserInfoQuery {

    /**
     * 商品信息
     */
    private List<IndustryOrderGoodsInfo> goods_list;

    /**
     * 订单总价，单位分
     */
    private Long total_amount;

    /**
     * 用户手机号，长度 <= 128 byte
     */
    private String phone_num;

    /**
     * 用户姓名，长度 <= 64 byte
     */
    private String contact_name;

    /**
     * 下单备注信息，长度 <= 2048byte
     */
    private String extra;

    /**
     * 支付结果通知地址，必须是 HTTPS 类型。
     * 若不填，默认使用在行业模板配置-消息通知的支付结果通知地址。
     */
    private String pay_notify_url;

    /**
     * 开发者的单号，长度 <= 64 byte
     */
    private String out_order_no;

    /**
     * 支付超时时间，单位秒，例如 300 表示 300 秒后过期；不传或传 0 会使用默认值 300。
     */
    private Long pay_expire_seconds;

    /**
     * 订单详情页信息
     */
    private IndustryOrderOrderEntrySchema order_entry_schema;

    /**
     * 开发者自定义透传字段，不支持二进制，长度 <= 2048 byte
     */
    private String cp_extra;

    /**
     * 折扣金额，单位分
     */
    private Long discount_amount;

    public List<IndustryOrderGoodsInfo> getGoods_list() {
        return goods_list;
    }

    public PreCreateIndustryOrderQuery setGoods_list(List<IndustryOrderGoodsInfo> goods_list) {
        this.goods_list = goods_list;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public PreCreateIndustryOrderQuery setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public PreCreateIndustryOrderQuery setPhone_num(String phone_num) {
        this.phone_num = phone_num;
        return this;
    }

    public String getContact_name() {
        return contact_name;
    }

    public PreCreateIndustryOrderQuery setContact_name(String contact_name) {
        this.contact_name = contact_name;
        return this;
    }

    public String getExtra() {
        return extra;
    }

    public PreCreateIndustryOrderQuery setExtra(String extra) {
        this.extra = extra;
        return this;
    }

    public String getPay_notify_url() {
        return pay_notify_url;
    }

    public PreCreateIndustryOrderQuery setPay_notify_url(String pay_notify_url) {
        this.pay_notify_url = pay_notify_url;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public PreCreateIndustryOrderQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public Long getPay_expire_seconds() {
        return pay_expire_seconds;
    }

    public PreCreateIndustryOrderQuery setPay_expire_seconds(Long pay_expire_seconds) {
        this.pay_expire_seconds = pay_expire_seconds;
        return this;
    }

    public IndustryOrderOrderEntrySchema getOrder_entry_schema() {
        return order_entry_schema;
    }

    public PreCreateIndustryOrderQuery setOrder_entry_schema(IndustryOrderOrderEntrySchema order_entry_schema) {
        this.order_entry_schema = order_entry_schema;
        return this;
    }

    public String getCp_extra() {
        return cp_extra;
    }

    public PreCreateIndustryOrderQuery setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public Long getDiscount_amount() {
        return discount_amount;
    }

    public PreCreateIndustryOrderQuery setDiscount_amount(Long discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public static PreCreateIndustryOrderQueryBuilder builder() {
        return new PreCreateIndustryOrderQueryBuilder();
    }

    public static final class PreCreateIndustryOrderQueryBuilder {
        private List<IndustryOrderGoodsInfo> goods_list;
        private Long total_amount;
        private String phone_num;
        private String contact_name;
        private String extra;
        private String pay_notify_url;
        private String out_order_no;
        private Long pay_expire_seconds;
        private IndustryOrderOrderEntrySchema order_entry_schema;
        private String cp_extra;
        private Long discount_amount;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private PreCreateIndustryOrderQueryBuilder() {
        }

        public PreCreateIndustryOrderQueryBuilder goodsList(List<IndustryOrderGoodsInfo> goodsList) {
            this.goods_list = goodsList;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder totalAmount(Long totalAmount) {
            this.total_amount = totalAmount;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder phoneNum(String phoneNum) {
            this.phone_num = phoneNum;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder contactName(String contactName) {
            this.contact_name = contactName;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder extra(String extra) {
            this.extra = extra;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder pay_notify_url(String pay_notify_url) {
            this.pay_notify_url = pay_notify_url;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder payExpireSeconds(Long payExpireSeconds) {
            this.pay_expire_seconds = payExpireSeconds;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder orderEntrySchema(IndustryOrderOrderEntrySchema orderEntrySchema) {
            this.order_entry_schema = orderEntrySchema;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder cpExtra(String cpExtra) {
            this.cp_extra = cpExtra;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder discountAmount(Long discountAmount) {
            this.discount_amount = discountAmount;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public PreCreateIndustryOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public PreCreateIndustryOrderQuery build() {
            PreCreateIndustryOrderQuery preCreateIndustryOrderQuery = new PreCreateIndustryOrderQuery();
            preCreateIndustryOrderQuery.setGoods_list(goods_list);
            preCreateIndustryOrderQuery.setTotal_amount(total_amount);
            preCreateIndustryOrderQuery.setPhone_num(phone_num);
            preCreateIndustryOrderQuery.setContact_name(contact_name);
            preCreateIndustryOrderQuery.setExtra(extra);
            preCreateIndustryOrderQuery.setPay_notify_url(pay_notify_url);
            preCreateIndustryOrderQuery.setOut_order_no(out_order_no);
            preCreateIndustryOrderQuery.setPay_expire_seconds(pay_expire_seconds);
            preCreateIndustryOrderQuery.setOrder_entry_schema(order_entry_schema);
            preCreateIndustryOrderQuery.setCp_extra(cp_extra);
            preCreateIndustryOrderQuery.setDiscount_amount(discount_amount);
            preCreateIndustryOrderQuery.setOpen_id(open_id);
            preCreateIndustryOrderQuery.setTenantId(tenantId);
            preCreateIndustryOrderQuery.setClientKey(clientKey);
            return preCreateIndustryOrderQuery;
        }
    }
}
