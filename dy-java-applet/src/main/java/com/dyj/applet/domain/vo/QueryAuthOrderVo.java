package com.dyj.applet.domain.vo;

public class QueryAuthOrderVo {
    /**
     * <p>小程序 app_id</p>
     */
    private String app_id;
    /**
     * <p>信用单平台订单号，长度<=64byte</p>
     */
    private String auth_order_id;
    /**
     * <p>下单回调地址，https开头，长度<=512byte</p> 选填
     */
    private String notify_url;
    /**
     * <p>用户open id</p>
     */
    private String open_id;
    /**
     * <p>信用单开发者的单号，长度<=64byte</p>
     */
    private String out_auth_order_id;
    /**
     * <p>免押场景</p><p>1-租车</p>
     */
    private Long scene;
    /**
     * <p>信用模板ID，完成进件和接入信用免押签约之后会提供</p>
     */
    private String service_id;
    /**
     * <p>用户签约完成时间，时间毫秒</p> 选填
     */
    private Long sign_time;
    /**
     * <ul><li>TOBESERVED: 待服务（用户未签约）</li><li>SERVING：服务中（用户已签约）</li><li>DONE: 服务完成</li><li>CANCEL: 服务撤销</li><li>TIMEOUT: 用户未签约自动撤销</li></ul>
     */
    private String status;

    public String getApp_id() {
        return app_id;
    }

    public QueryAuthOrderVo setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public QueryAuthOrderVo setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public QueryAuthOrderVo setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOpen_id() {
        return open_id;
    }

    public QueryAuthOrderVo setOpen_id(String open_id) {
        this.open_id = open_id;
        return this;
    }

    public String getOut_auth_order_id() {
        return out_auth_order_id;
    }

    public QueryAuthOrderVo setOut_auth_order_id(String out_auth_order_id) {
        this.out_auth_order_id = out_auth_order_id;
        return this;
    }

    public Long getScene() {
        return scene;
    }

    public QueryAuthOrderVo setScene(Long scene) {
        this.scene = scene;
        return this;
    }

    public String getService_id() {
        return service_id;
    }

    public QueryAuthOrderVo setService_id(String service_id) {
        this.service_id = service_id;
        return this;
    }

    public Long getSign_time() {
        return sign_time;
    }

    public QueryAuthOrderVo setSign_time(Long sign_time) {
        this.sign_time = sign_time;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public QueryAuthOrderVo setStatus(String status) {
        this.status = status;
        return this;
    }
}
