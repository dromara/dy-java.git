package com.dyj.applet.domain.vo;

public class CreatePayOrderVo {

    /**
     * <p>扣款单平台订单号，长度<=64byte</p>
     */
    private String pay_order_id;

    public String getPay_order_id() {
        return pay_order_id;
    }

    public CreatePayOrderVo setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }
}
