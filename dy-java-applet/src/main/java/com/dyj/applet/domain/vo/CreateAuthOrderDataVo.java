package com.dyj.applet.domain.vo;

public class CreateAuthOrderDataVo {
    /**
     * <p>信用单平台订单号，长度<=64byte</p>
     */
    private String auth_order_id;

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public CreateAuthOrderDataVo setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }
}
