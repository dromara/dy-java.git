package com.dyj.applet.domain;

import java.util.List;

/**
 * CPS 订单已退款记录信息
 */
public class QueryIndustryCpsRefundInfo {

    /**
     * 订单退款总金额，单位分，没有已退款记录，为 0
     */
    private Long total_refund_amount;
    /**
     * 订单已退款详细信息，如果没有已退款记录，为 null对于交易模板 1.0 和担保支付订单，为整单退款记录对于交易系统，为商品单退款记录
     */
    private List<QueryIndustryCpsRefundInfoRefundItem> refund_items;

    public Long getTotal_refund_amount() {
        return total_refund_amount;
    }

    public QueryIndustryCpsRefundInfo setTotal_refund_amount(Long total_refund_amount) {
        this.total_refund_amount = total_refund_amount;
        return this;
    }

    public List<QueryIndustryCpsRefundInfoRefundItem> getRefund_items() {
        return refund_items;
    }

    public QueryIndustryCpsRefundInfo setRefund_items(List<QueryIndustryCpsRefundInfoRefundItem> refund_items) {
        this.refund_items = refund_items;
        return this;
    }
}
