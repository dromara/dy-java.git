package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-06-06 09:53
 **/
public class AptKfUrlVo {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
