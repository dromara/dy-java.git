package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 同步退款审核结果请求值
 */
public class MerchantAuditCallbackQuery extends BaseQuery {

    /**
     * 不同意退款信息(不同意退款时必填)，长度 <= 512 byte 选填
     */
    private String deny_message;
    /**
     * <p>审核状态，</p><ul><li>1-同意退款 </li><li>2-不同意退款</li></ul>
     */
    private Integer refund_audit_status;
    /**
     * <p>交易系统侧退款单号，长度 &lt;= 64 byte</p>
     */
    private String refund_id;

    public String getDeny_message() {
        return deny_message;
    }

    public MerchantAuditCallbackQuery setDeny_message(String deny_message) {
        this.deny_message = deny_message;
        return this;
    }

    public Integer getRefund_audit_status() {
        return refund_audit_status;
    }

    public MerchantAuditCallbackQuery setRefund_audit_status(Integer refund_audit_status) {
        this.refund_audit_status = refund_audit_status;
        return this;
    }

    public String getRefund_id() {
        return refund_id;
    }

    public MerchantAuditCallbackQuery setRefund_id(String refund_id) {
        this.refund_id = refund_id;
        return this;
    }

    public static MerchantAuditCallbackQueryBuilder builder() {
        return new MerchantAuditCallbackQueryBuilder();
    }

    public static final class MerchantAuditCallbackQueryBuilder {
        private String deny_message;
        private Integer refund_audit_status;
        private String refund_id;
        private Integer tenantId;
        private String clientKey;

        private MerchantAuditCallbackQueryBuilder() {
        }

        public MerchantAuditCallbackQueryBuilder denyMessage(String denyMessage) {
            this.deny_message = denyMessage;
            return this;
        }

        public MerchantAuditCallbackQueryBuilder refundAuditStatus(Integer refundAuditStatus) {
            this.refund_audit_status = refundAuditStatus;
            return this;
        }

        public MerchantAuditCallbackQueryBuilder refundId(String refundId) {
            this.refund_id = refundId;
            return this;
        }

        public MerchantAuditCallbackQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public MerchantAuditCallbackQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public MerchantAuditCallbackQuery build() {
            MerchantAuditCallbackQuery merchantAuditCallbackQuery = new MerchantAuditCallbackQuery();
            merchantAuditCallbackQuery.setDeny_message(deny_message);
            merchantAuditCallbackQuery.setRefund_audit_status(refund_audit_status);
            merchantAuditCallbackQuery.setRefund_id(refund_id);
            merchantAuditCallbackQuery.setTenantId(tenantId);
            merchantAuditCallbackQuery.setClientKey(clientKey);
            return merchantAuditCallbackQuery;
        }
    }
}
