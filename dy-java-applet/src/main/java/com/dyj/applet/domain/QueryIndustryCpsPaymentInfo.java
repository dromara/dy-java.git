package com.dyj.applet.domain;

/**
 * CPS 订单支付相关信息
 */
public class QueryIndustryCpsPaymentInfo {

    /**
     * 渠道支付单号，如微信的支付单号
     */
    private String channel_pay_id;
    /**
     * 预下单时开发者定义的透传信息
     */
    private String cp_extra;
    /**
     * 结果描述信息，如失败原因
     */
    private String message;
    /**
     * 订单状态，
     */
    private String order_status;
    /**
     * 支付渠道枚举：
     */
    private Integer pay_channel;
    /**
     * 支付时间，格式：2021-12-12 00:00:00
     */
    private String pay_time;
    /**
     * 卖家商户号 id
     */
    private String seller_uid;
    /**
     * 订单实际支付金额，单位分
     */
    private Long total_fee;

    public String getChannel_pay_id() {
        return channel_pay_id;
    }

    public QueryIndustryCpsPaymentInfo setChannel_pay_id(String channel_pay_id) {
        this.channel_pay_id = channel_pay_id;
        return this;
    }

    public String getCp_extra() {
        return cp_extra;
    }

    public QueryIndustryCpsPaymentInfo setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public QueryIndustryCpsPaymentInfo setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getOrder_status() {
        return order_status;
    }

    public QueryIndustryCpsPaymentInfo setOrder_status(String order_status) {
        this.order_status = order_status;
        return this;
    }

    public Integer getPay_channel() {
        return pay_channel;
    }

    public QueryIndustryCpsPaymentInfo setPay_channel(Integer pay_channel) {
        this.pay_channel = pay_channel;
        return this;
    }

    public String getPay_time() {
        return pay_time;
    }

    public QueryIndustryCpsPaymentInfo setPay_time(String pay_time) {
        this.pay_time = pay_time;
        return this;
    }

    public String getSeller_uid() {
        return seller_uid;
    }

    public QueryIndustryCpsPaymentInfo setSeller_uid(String seller_uid) {
        this.seller_uid = seller_uid;
        return this;
    }

    public Long getTotal_fee() {
        return total_fee;
    }

    public QueryIndustryCpsPaymentInfo setTotal_fee(Long total_fee) {
        this.total_fee = total_fee;
        return this;
    }
}
