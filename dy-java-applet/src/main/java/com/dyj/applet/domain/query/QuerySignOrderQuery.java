package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class QuerySignOrderQuery extends BaseQuery {

    /**
     * <p>平台侧签约单的单号，长度<=64byte，auth_order_id 与 out_auth_order_no 二选一</p> 选填
     */
    private String auth_order_id;
    /**
     * <p>开发者侧签约单的单号，长度<=64byte，</p><p>auth_order_id 与 out_auth_order_no 二选一</p> 选填
     */
    private String out_auth_order_no;

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public QuerySignOrderQuery setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public String getOut_auth_order_no() {
        return out_auth_order_no;
    }

    public QuerySignOrderQuery setOut_auth_order_no(String out_auth_order_no) {
        this.out_auth_order_no = out_auth_order_no;
        return this;
    }

    public static QuerySignOrderQueryBuilder builder() {
        return new QuerySignOrderQueryBuilder();
    }

    public static final class QuerySignOrderQueryBuilder {
        private String auth_order_id;
        private String out_auth_order_no;
        private Integer tenantId;
        private String clientKey;

        private QuerySignOrderQueryBuilder() {
        }

        public QuerySignOrderQueryBuilder authOrderId(String authOrderId) {
            this.auth_order_id = authOrderId;
            return this;
        }

        public QuerySignOrderQueryBuilder outAuthOrderNo(String outAuthOrderNo) {
            this.out_auth_order_no = outAuthOrderNo;
            return this;
        }

        public QuerySignOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QuerySignOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QuerySignOrderQuery build() {
            QuerySignOrderQuery querySignOrderQuery = new QuerySignOrderQuery();
            querySignOrderQuery.setAuth_order_id(auth_order_id);
            querySignOrderQuery.setOut_auth_order_no(out_auth_order_no);
            querySignOrderQuery.setTenantId(tenantId);
            querySignOrderQuery.setClientKey(clientKey);
            return querySignOrderQuery;
        }
    }
}
