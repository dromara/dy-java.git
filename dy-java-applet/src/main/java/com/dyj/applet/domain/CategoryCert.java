package com.dyj.applet.domain;

import java.util.List;

/**
 * 已上传的所有资质信息
 */
public class CategoryCert {

    /**
     * 资质 id
     */
    private String id;

    /**
     * 资质名称
     */
    private String name;

    /**
     * 是否视频资质
     */
    private Integer is_video;

    /**
     * 已上传的资质的文件路径列表
     */
    private List<String> path_list;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIs_video() {
        return is_video;
    }

    public void setIs_video(Integer is_video) {
        this.is_video = is_video;
    }

    public List<String> getPath_list() {
        return path_list;
    }

    public void setPath_list(List<String> path_list) {
        this.path_list = path_list;
    }
}
