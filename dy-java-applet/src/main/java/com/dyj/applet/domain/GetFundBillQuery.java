package com.dyj.applet.domain;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 获取资金账单请求值
 */
public class GetFundBillQuery extends BaseQuery {

    /**
     * <p>账户类型</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">FEE:手续费账户</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">REFUND_DEPOSIT:退款备用金账户</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">PAY_TO_CHANGE:打款到零钱账户</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">WX:微信</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">ALIPAY:支付宝</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">DOUYIN:抖音支付</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">YZT:聚合账户</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">ALL:所有账户类型</li></ul>
     */
    private String account_type;
    /**
     * <p>和商户号绑定的 app_id</p>
     */
    private String app_id;
    /**
     * <p>交易完成时间</p><p>日账单：20220101，</p><p>月账单：202201</p>
     */
    private String bill_date;
    /**
     * 商户号
     */
    private String merchant_id;
    /**
     * <p>收支类型</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">INCOME:收入</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">EXPENDITURE: 支出</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">ALL:所有收支类型</li></ul>
     */
    private String payment_type;
    /**
     * <p>交易类型</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">RECHARGE:充值</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">PAY:打款</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">REFUND:退款</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">DEAL:交易</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">WITHDRAW:提现</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">ROLLBACK:退回</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">ADJUST_IN: 调账入</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">ADJUST_OUT调账出</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">PENALTY:扣罚</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">SETTLE:分账</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">RETURN_SETTLE_IN:退分账入</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">RETURN_SETTLE_OUT:退分账出</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">ALL:所有交易类型</li></ul>
     */
    private String trade_type;

    public String getAccount_type() {
        return account_type;
    }

    public GetFundBillQuery setAccount_type(String account_type) {
        this.account_type = account_type;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public GetFundBillQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getBill_date() {
        return bill_date;
    }

    public GetFundBillQuery setBill_date(String bill_date) {
        this.bill_date = bill_date;
        return this;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public GetFundBillQuery setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
        return this;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public GetFundBillQuery setPayment_type(String payment_type) {
        this.payment_type = payment_type;
        return this;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public GetFundBillQuery setTrade_type(String trade_type) {
        this.trade_type = trade_type;
        return this;
    }

    public static GetFundBillQueryBuilder builder() {
        return new GetFundBillQueryBuilder();
    }

    public static final class GetFundBillQueryBuilder {
        private String account_type;
        private String app_id;
        private String bill_date;
        private String merchant_id;
        private String payment_type;
        private String trade_type;
        private Integer tenantId;
        private String clientKey;

        private GetFundBillQueryBuilder() {
        }

        public GetFundBillQueryBuilder accountType(String accountType) {
            this.account_type = accountType;
            return this;
        }

        public GetFundBillQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public GetFundBillQueryBuilder billDate(String billDate) {
            this.bill_date = billDate;
            return this;
        }

        public GetFundBillQueryBuilder merchantId(String merchantId) {
            this.merchant_id = merchantId;
            return this;
        }

        public GetFundBillQueryBuilder paymentType(String paymentType) {
            this.payment_type = paymentType;
            return this;
        }

        public GetFundBillQueryBuilder tradeType(String tradeType) {
            this.trade_type = tradeType;
            return this;
        }

        public GetFundBillQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public GetFundBillQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public GetFundBillQuery build() {
            GetFundBillQuery getFundBillQuery = new GetFundBillQuery();
            getFundBillQuery.setAccount_type(account_type);
            getFundBillQuery.setApp_id(app_id);
            getFundBillQuery.setBill_date(bill_date);
            getFundBillQuery.setMerchant_id(merchant_id);
            getFundBillQuery.setPayment_type(payment_type);
            getFundBillQuery.setTrade_type(trade_type);
            getFundBillQuery.setTenantId(tenantId);
            getFundBillQuery.setClientKey(clientKey);
            return getFundBillQuery;
        }
    }
}
