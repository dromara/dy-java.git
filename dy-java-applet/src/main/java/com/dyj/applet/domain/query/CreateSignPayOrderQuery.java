package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class CreateSignPayOrderQuery extends BaseQuery {
    /**
     * <p>平台侧签约单的单号，长度<=64byte</p>
     */
    private String auth_order_id;
    /**
     * <p>代扣超时时间，单位[秒]。不传则默认超时时间为5分钟</p><p>最少30秒，不能超过48小时</p> 选填
     */
    private Long expire_seconds;
    /**
     * <p>开发者自定义收款商户号</p><p>限定在小程序绑定的商户号内</p>
     */
    private String merchant_uid;
    /**
     * <p>下单结果回调地址，https开头，长度<=512byte</p> 选填
     */
    private String notify_url;
    /**
     * <p>开发者侧代扣单的单号，长度<=64byte</p>
     */
    private String out_pay_order_no;
    /**
     * <p>指定扣款金额（单位：分），不能超过模板配置金额。如果不填，使用模板配置金额进行扣款。</p> 选填
     */
    private Long total_amount;

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public CreateSignPayOrderQuery setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public Long getExpire_seconds() {
        return expire_seconds;
    }

    public CreateSignPayOrderQuery setExpire_seconds(Long expire_seconds) {
        this.expire_seconds = expire_seconds;
        return this;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public CreateSignPayOrderQuery setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public CreateSignPayOrderQuery setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOut_pay_order_no() {
        return out_pay_order_no;
    }

    public CreateSignPayOrderQuery setOut_pay_order_no(String out_pay_order_no) {
        this.out_pay_order_no = out_pay_order_no;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public CreateSignPayOrderQuery setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public static CreateSignPayOrderQueryBuilder builder() {
        return new CreateSignPayOrderQueryBuilder();
    }

    public static final class CreateSignPayOrderQueryBuilder {
        private String auth_order_id;
        private Long expire_seconds;
        private String merchant_uid;
        private String notify_url;
        private String out_pay_order_no;
        private Long total_amount;
        private Integer tenantId;
        private String clientKey;

        private CreateSignPayOrderQueryBuilder() {
        }

        public CreateSignPayOrderQueryBuilder authOrderId(String authOrderId) {
            this.auth_order_id = authOrderId;
            return this;
        }

        public CreateSignPayOrderQueryBuilder expireSeconds(Long expireSeconds) {
            this.expire_seconds = expireSeconds;
            return this;
        }

        public CreateSignPayOrderQueryBuilder merchantUid(String merchantUid) {
            this.merchant_uid = merchantUid;
            return this;
        }

        public CreateSignPayOrderQueryBuilder notifyUrl(String notifyUrl) {
            this.notify_url = notifyUrl;
            return this;
        }

        public CreateSignPayOrderQueryBuilder outPayOrderNo(String outPayOrderNo) {
            this.out_pay_order_no = outPayOrderNo;
            return this;
        }

        public CreateSignPayOrderQueryBuilder totalAmount(Long totalAmount) {
            this.total_amount = totalAmount;
            return this;
        }

        public CreateSignPayOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateSignPayOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateSignPayOrderQuery build() {
            CreateSignPayOrderQuery createSignPayOrderQuery = new CreateSignPayOrderQuery();
            createSignPayOrderQuery.setAuth_order_id(auth_order_id);
            createSignPayOrderQuery.setExpire_seconds(expire_seconds);
            createSignPayOrderQuery.setMerchant_uid(merchant_uid);
            createSignPayOrderQuery.setNotify_url(notify_url);
            createSignPayOrderQuery.setOut_pay_order_no(out_pay_order_no);
            createSignPayOrderQuery.setTotal_amount(total_amount);
            createSignPayOrderQuery.setTenantId(tenantId);
            createSignPayOrderQuery.setClientKey(clientKey);
            return createSignPayOrderQuery;
        }
    }
}
