package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-05-06 10:56
 **/
public class SaveGoodsProductVo {
    /**
     * 创建/更新商品成功后产生，与老接口spu_id一致
     */
    private String product_id;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
