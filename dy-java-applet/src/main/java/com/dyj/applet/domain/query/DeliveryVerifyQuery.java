package com.dyj.applet.domain.query;

import com.dyj.applet.domain.DeliveryVerifyCertificate;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * 验券请求值
 */
public class DeliveryVerifyQuery extends BaseQuery {

    /**
     * 交易系统单号，从验券准备接口获取。
     */
    private String order_id;
    /**
     * 核销的商铺 POI 信息，最多 1024 个字节。 注意： 选填
     */
    private String poi_info;
    /**
     * 验券标识，从验券准备接口获取的，用于幂等。
     */
    private String verify_token;
    /**
     * 验券准备接口返回的加密券码，传入几份表示核销几份。
     */
    private List<DeliveryVerifyCertificate> certificates;

    public String getOrder_id() {
        return order_id;
    }

    public DeliveryVerifyQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getPoi_info() {
        return poi_info;
    }

    public DeliveryVerifyQuery setPoi_info(String poi_info) {
        this.poi_info = poi_info;
        return this;
    }

    public String getVerify_token() {
        return verify_token;
    }

    public DeliveryVerifyQuery setVerify_token(String verify_token) {
        this.verify_token = verify_token;
        return this;
    }

    public List<DeliveryVerifyCertificate> getCertificates() {
        return certificates;
    }

    public DeliveryVerifyQuery setCertificates(List<DeliveryVerifyCertificate> certificates) {
        this.certificates = certificates;
        return this;
    }

    public static DeliveryVerifyQueryBuilder builder() {
        return new DeliveryVerifyQueryBuilder();
    }

    public static final class DeliveryVerifyQueryBuilder {
        private String order_id;
        private String poi_info;
        private String verify_token;
        private List<DeliveryVerifyCertificate> certificates;
        private Integer tenantId;
        private String clientKey;

        private DeliveryVerifyQueryBuilder() {
        }

        public DeliveryVerifyQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public DeliveryVerifyQueryBuilder poiInfo(String poiInfo) {
            this.poi_info = poiInfo;
            return this;
        }

        public DeliveryVerifyQueryBuilder verifyToken(String verifyToken) {
            this.verify_token = verifyToken;
            return this;
        }

        public DeliveryVerifyQueryBuilder certificates(List<DeliveryVerifyCertificate> certificates) {
            this.certificates = certificates;
            return this;
        }

        public DeliveryVerifyQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public DeliveryVerifyQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public DeliveryVerifyQuery build() {
            DeliveryVerifyQuery deliveryVerifyQuery = new DeliveryVerifyQuery();
            deliveryVerifyQuery.setOrder_id(order_id);
            deliveryVerifyQuery.setPoi_info(poi_info);
            deliveryVerifyQuery.setVerify_token(verify_token);
            deliveryVerifyQuery.setCertificates(certificates);
            deliveryVerifyQuery.setTenantId(tenantId);
            deliveryVerifyQuery.setClientKey(clientKey);
            return deliveryVerifyQuery;
        }
    }
}
