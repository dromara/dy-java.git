package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 查询卷模板查询条件
 */
public class QueryCouponMetaQuery extends BaseQuery {

    /**
     * 卷模板id
     */
    private String coupon_meta_id;

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public QueryCouponMetaQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public static QueryCouponMetaQueryBuilder builder() {
        return new QueryCouponMetaQueryBuilder();
    }


    public static final class QueryCouponMetaQueryBuilder {
        private String coupon_meta_id;
        private Integer tenantId;
        private String clientKey;

        private QueryCouponMetaQueryBuilder() {
        }


        public QueryCouponMetaQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public QueryCouponMetaQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryCouponMetaQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryCouponMetaQuery build() {
            QueryCouponMetaQuery queryCouponMetaQuery = new QueryCouponMetaQuery();
            queryCouponMetaQuery.setCoupon_meta_id(coupon_meta_id);
            queryCouponMetaQuery.setTenantId(tenantId);
            queryCouponMetaQuery.setClientKey(clientKey);
            return queryCouponMetaQuery;
        }
    }
}
