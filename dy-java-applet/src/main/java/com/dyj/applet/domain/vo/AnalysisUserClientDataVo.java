package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisAttribute;

import java.util.List;

public class AnalysisUserClientDataVo {

    /**
     * 客户端版本分布
     */
    private List<AnalysisAttribute> app_version;
    /**
     * 终端品牌分布
     */
    private List<AnalysisAttribute> brand;
    /**
     * 基础库版本分布
     */
    private List<AnalysisAttribute> lib_version;
    /**
     * 终端机型分布
     */
    private List<AnalysisAttribute> model;

    public List<AnalysisAttribute> getApp_version() {
        return app_version;
    }

    public void setApp_version(List<AnalysisAttribute> app_version) {
        this.app_version = app_version;
    }

    public List<AnalysisAttribute> getBrand() {
        return brand;
    }

    public void setBrand(List<AnalysisAttribute> brand) {
        this.brand = brand;
    }

    public List<AnalysisAttribute> getLib_version() {
        return lib_version;
    }

    public void setLib_version(List<AnalysisAttribute> lib_version) {
        this.lib_version = lib_version;
    }

    public List<AnalysisAttribute> getModel() {
        return model;
    }

    public void setModel(List<AnalysisAttribute> model) {
        this.model = model;
    }
}
