package com.dyj.applet.domain;

import java.util.List;

/**
 * 经营地址
 */
public class CreateSubMerchantBusinessLicense {

    /**
     * <p>证件照开始时间, 格式 yyyymmdd</p>
     */
    private String begin_date;
    /**
     * <p>证件号，身份证号/营业执照号</p>
     */
    private String business_license_code;
    /**
     * <p>根据商户类型传入对应的证件类型</p><p><strong>商户类型-证件类型 对应关系</strong></p><p>个人/小微：个人身份证</p><p>企业：营业执照（三证合一）</p><p>个体工商户：营业执照（三证合一）</p><p>事业单位：事业单位法人证书</p><p>民办非企业组织：民办非企业登记证书</p><p>社会团体：社会团体法人登记证书</p><p>党政及国家机关：党政及国家机关资格证</p><p><strong>证件类型枚举值</strong></p><p>1: 个人身份证</p><p>3: 营业执照（三证合一）</p><p>4: 民办非企业登记证书</p><p>5: 社会团体法人登记证书</p><p>6: 事业单位法人证书</p><p>7: 党政及国家机关资格证</p>
     */
    private Integer business_license_type;
    /**
     * <p>证件照结束时间, 格式yyyymmdd, 长期填 99991231</p>
     */
    private String end_date;
    /**
     * <p>证件照副照片部分, 身份证的话，是国徽面 营业照可以不传副照 ,图片id, 参数格式详见图片下面 图片信息参数</p> 选填
     */
    private List<ChannelAndUrl> business_license_back_picurl;
    /**
     * <p>证件照主照片</p>
     */
    private List<ChannelAndUrl> business_license_picurl;

    public String getBegin_date() {
        return begin_date;
    }

    public CreateSubMerchantBusinessLicense setBegin_date(String begin_date) {
        this.begin_date = begin_date;
        return this;
    }

    public String getBusiness_license_code() {
        return business_license_code;
    }

    public CreateSubMerchantBusinessLicense setBusiness_license_code(String business_license_code) {
        this.business_license_code = business_license_code;
        return this;
    }

    public Integer getBusiness_license_type() {
        return business_license_type;
    }

    public CreateSubMerchantBusinessLicense setBusiness_license_type(Integer business_license_type) {
        this.business_license_type = business_license_type;
        return this;
    }

    public String getEnd_date() {
        return end_date;
    }

    public CreateSubMerchantBusinessLicense setEnd_date(String end_date) {
        this.end_date = end_date;
        return this;
    }

    public List<ChannelAndUrl> getBusiness_license_back_picurl() {
        return business_license_back_picurl;
    }

    public CreateSubMerchantBusinessLicense setBusiness_license_back_picurl(List<ChannelAndUrl> business_license_back_picurl) {
        this.business_license_back_picurl = business_license_back_picurl;
        return this;
    }

    public List<ChannelAndUrl> getBusiness_license_picurl() {
        return business_license_picurl;
    }

    public CreateSubMerchantBusinessLicense setBusiness_license_picurl(List<ChannelAndUrl> business_license_picurl) {
        this.business_license_picurl = business_license_picurl;
        return this;
    }

    public static CreateSubMerchantBusinessLicenseBuilder builder(){
        return new CreateSubMerchantBusinessLicenseBuilder();
    }


    public static final class CreateSubMerchantBusinessLicenseBuilder {
        private String begin_date;
        private String business_license_code;
        private Integer business_license_type;
        private String end_date;
        private List<ChannelAndUrl> business_license_back_picurl;
        private List<ChannelAndUrl> business_license_picurl;

        private CreateSubMerchantBusinessLicenseBuilder() {
        }

        public CreateSubMerchantBusinessLicenseBuilder beginDate(String beginDate) {
            this.begin_date = beginDate;
            return this;
        }

        public CreateSubMerchantBusinessLicenseBuilder businessLicenseCode(String businessLicenseCode) {
            this.business_license_code = businessLicenseCode;
            return this;
        }

        public CreateSubMerchantBusinessLicenseBuilder businessLicenseType(Integer businessLicenseType) {
            this.business_license_type = businessLicenseType;
            return this;
        }

        public CreateSubMerchantBusinessLicenseBuilder endDate(String endDate) {
            this.end_date = endDate;
            return this;
        }

        public CreateSubMerchantBusinessLicenseBuilder businessLicenseBackPicurl(List<ChannelAndUrl> businessLicenseBackPicurl) {
            this.business_license_back_picurl = businessLicenseBackPicurl;
            return this;
        }

        public CreateSubMerchantBusinessLicenseBuilder businessLicensePicurl(List<ChannelAndUrl> businessLicensePicurl) {
            this.business_license_picurl = businessLicensePicurl;
            return this;
        }

        public CreateSubMerchantBusinessLicense build() {
            CreateSubMerchantBusinessLicense createSubMerchantBusinessLicense = new CreateSubMerchantBusinessLicense();
            createSubMerchantBusinessLicense.setBegin_date(begin_date);
            createSubMerchantBusinessLicense.setBusiness_license_code(business_license_code);
            createSubMerchantBusinessLicense.setBusiness_license_type(business_license_type);
            createSubMerchantBusinessLicense.setEnd_date(end_date);
            createSubMerchantBusinessLicense.setBusiness_license_back_picurl(business_license_back_picurl);
            createSubMerchantBusinessLicense.setBusiness_license_picurl(business_license_picurl);
            return createSubMerchantBusinessLicense;
        }
    }
}
