package com.dyj.applet.domain.query;

import com.dyj.applet.domain.BookMarkupInfo;
import com.dyj.applet.domain.ItemBookInfo;
import com.dyj.common.domain.query.UserInfoQuery;

import java.util.List;

/**
 * 生活服务交易系统->预约->创建预约单
 */
public class CreateBookQuery extends UserInfoQuery {

    /**
     * 抖音侧订单号
     */
    private String order_id;

    /**
     * 外部预约单号
     */
    private String out_book_no;

    /**
     * 每个item的预约信息，详见ItemBookInfo
     */
    private List<ItemBookInfo> item_book_info_list;

    /**
     * 加价信息，详见MarkupInfo
     * 选填
     */
    private BookMarkupInfo markup_info;

    public String getOrder_id() {
        return order_id;
    }

    public CreateBookQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_book_no() {
        return out_book_no;
    }

    public CreateBookQuery setOut_book_no(String out_book_no) {
        this.out_book_no = out_book_no;
        return this;
    }

    public List<ItemBookInfo> getItem_book_info_list() {
        return item_book_info_list;
    }

    public CreateBookQuery setItem_book_info_list(List<ItemBookInfo> item_book_info_list) {
        this.item_book_info_list = item_book_info_list;
        return this;
    }

    public BookMarkupInfo getMarkup_info() {
        return markup_info;
    }

    public CreateBookQuery setMarkup_info(BookMarkupInfo markup_info) {
        this.markup_info = markup_info;
        return this;
    }

    public static CreateBookQueryBuilder builder() {
        return new CreateBookQueryBuilder();
    }

    public static final class CreateBookQueryBuilder {
        private String order_id;
        private String out_book_no;
        private List<ItemBookInfo> item_book_info_list;
        private BookMarkupInfo markup_info;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private CreateBookQueryBuilder() {
        }

        public CreateBookQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public CreateBookQueryBuilder outBookNo(String outBookNo) {
            this.out_book_no = outBookNo;
            return this;
        }

        public CreateBookQueryBuilder itemBookInfoList(List<ItemBookInfo> itemBookInfoList) {
            this.item_book_info_list = itemBookInfoList;
            return this;
        }

        public CreateBookQueryBuilder markupInfo(BookMarkupInfo markupInfo) {
            this.markup_info = markupInfo;
            return this;
        }

        public CreateBookQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public CreateBookQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateBookQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateBookQuery build() {
            CreateBookQuery createBookQuery = new CreateBookQuery();
            createBookQuery.setOrder_id(order_id);
            createBookQuery.setOut_book_no(out_book_no);
            createBookQuery.setItem_book_info_list(item_book_info_list);
            createBookQuery.setMarkup_info(markup_info);
            createBookQuery.setOpen_id(open_id);
            createBookQuery.setTenantId(tenantId);
            createBookQuery.setClientKey(clientKey);
            return createBookQuery;
        }
    }
}
