package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-24 14:12
 **/
public class AptUserLike {

    /**
     *
     */
    private Long new_like;

    /**
     * yyyy-MM-dd日期
     */
    private String date;

    public Long getNew_like() {
        return new_like;
    }

    public void setNew_like(Long new_like) {
        this.new_like = new_like;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
