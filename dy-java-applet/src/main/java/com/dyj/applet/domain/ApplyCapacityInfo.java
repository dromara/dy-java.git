package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-07-17 09:56
 **/
public class ApplyCapacityInfo {

    /**
     * 能力key，能力唯一标识
     */
    private String capacity_key;
    /**
     * 能力名称
     */
    private String capacity_name;
    /**
     * 能力描述
     */
    private String description;
    /**
     * 申请能力的条件
     */
    private List<ApplyCondition> apply_condition;
    /**
     * 申请能力所需填写的字段
     */
    private List<ApplyField> apply_field_list;
    /**
     *适用宿主
     */
    private String host_app;
    /**
     * 是否已经授予权限集
     */
    private Boolean is_auth_permission;
    /**
     * 是否可申请开通
     */
    private Boolean is_can_apply;
    /**
     * 是否实验室能力
     */
    private Integer is_lab;
    /**
     * 实验室能力描述
     */
    private String lab_description;
    /**
     * 能力所需的权限集：1：开发管理权限2：基本信息设置权限3：运营管理权限4：数据分析权限5：广告管理权限6：支付服务权限7：流量主权限8：小程序推广计划-任务管理权限
     */
    private List<Integer> permission_list;
    /**
     * 能力状态：1-申请中；2-申请成功；3-申请拒绝；4-能力关闭；5-能力封禁；6-可申请但未申请；7-不可申请
     */
    private String status;


    public String getCapacity_key() {
        return capacity_key;
    }

    public void setCapacity_key(String capacity_key) {
        this.capacity_key = capacity_key;
    }

    public String getCapacity_name() {
        return capacity_name;
    }

    public void setCapacity_name(String capacity_name) {
        this.capacity_name = capacity_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ApplyCondition> getApply_condition() {
        return apply_condition;
    }

    public void setApply_condition(List<ApplyCondition> apply_condition) {
        this.apply_condition = apply_condition;
    }

    public List<ApplyField> getApply_field_list() {
        return apply_field_list;
    }

    public void setApply_field_list(List<ApplyField> apply_field_list) {
        this.apply_field_list = apply_field_list;
    }

    public String getHost_app() {
        return host_app;
    }

    public void setHost_app(String host_app) {
        this.host_app = host_app;
    }

    public Boolean getIs_auth_permission() {
        return is_auth_permission;
    }

    public void setIs_auth_permission(Boolean is_auth_permission) {
        this.is_auth_permission = is_auth_permission;
    }

    public Boolean getIs_can_apply() {
        return is_can_apply;
    }

    public void setIs_can_apply(Boolean is_can_apply) {
        this.is_can_apply = is_can_apply;
    }

    public Integer getIs_lab() {
        return is_lab;
    }

    public void setIs_lab(Integer is_lab) {
        this.is_lab = is_lab;
    }

    public String getLab_description() {
        return lab_description;
    }

    public void setLab_description(String lab_description) {
        this.lab_description = lab_description;
    }

    public List<Integer> getPermission_list() {
        return permission_list;
    }

    public void setPermission_list(List<Integer> permission_list) {
        this.permission_list = permission_list;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
