package com.dyj.applet.domain.query;

import com.dyj.applet.domain.LivePlanProduct;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-13 16:34
 **/
public class SaveShortVideoPlanQuery extends SaveLivePlanQuery {

    private Long commission_duration;

    private Long start_time;

    private Long end_time;

    public static SaveShortVideoPlanQueryBuilder builder() {
        return new SaveShortVideoPlanQueryBuilder();
    }

    public static class SaveShortVideoPlanQueryBuilder extends SaveLivePlanQuery.SaveLivePlanQueryBuilder {
        private Long commissionDuration;

        private String merchantPhone;

        private String planId;

        private List<LivePlanProduct> productList;

        private String planName;

        private String clientKey;

        private Integer tenantId;

        private List<String> douyinIdList;

        private Long startTime;

        private Long endTime;

        public SaveShortVideoPlanQueryBuilder commissionDuration(Long commissionDuration) {
            this.commissionDuration = commissionDuration;
            return this;
        }

        public SaveShortVideoPlanQueryBuilder merchantPhone(String merchantPhone) {
            this.merchantPhone = merchantPhone;
            return this;
        }

        public SaveShortVideoPlanQueryBuilder planId(String planId) {
            this.planId = planId;
            return this;
        }

        public SaveShortVideoPlanQueryBuilder productList(List<LivePlanProduct> productList) {
            this.productList = productList;
            return this;
        }

        public SaveShortVideoPlanQueryBuilder planName(String planName) {
            this.planName = planName;
            return this;
        }

        public SaveShortVideoPlanQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SaveShortVideoPlanQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public SaveShortVideoPlanQueryBuilder douyinIdList(List<String> douyinIdList) {
            this.douyinIdList = douyinIdList;
            return this;
        }

        public SaveShortVideoPlanQueryBuilder startTime(Long startTime) {
            this.startTime = startTime;
            return this;
        }

        public SaveShortVideoPlanQueryBuilder endTime(Long endTime) {
            this.endTime = endTime;
            return this;
        }

        public SaveShortVideoPlanQuery build() {
            SaveShortVideoPlanQuery saveShortVideoPlanQuery = new SaveShortVideoPlanQuery();
            saveShortVideoPlanQuery.setCommission_duration(commissionDuration);
            saveShortVideoPlanQuery.setMerchant_phone(merchantPhone);
            saveShortVideoPlanQuery.setPlan_id(planId);
            saveShortVideoPlanQuery.setProduct_list(productList);
            saveShortVideoPlanQuery.setPlan_name(planName);
            saveShortVideoPlanQuery.setClientKey(clientKey);
            saveShortVideoPlanQuery.setTenantId(tenantId);
            saveShortVideoPlanQuery.setDouyin_id_list(douyinIdList);
            saveShortVideoPlanQuery.setStart_time(startTime);
            saveShortVideoPlanQuery.setEnd_time(endTime);
            return saveShortVideoPlanQuery;
        }
    }


    public Long getCommission_duration() {
        return commission_duration;
    }

    public void setCommission_duration(Long commission_duration) {
        this.commission_duration = commission_duration;
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }
}
