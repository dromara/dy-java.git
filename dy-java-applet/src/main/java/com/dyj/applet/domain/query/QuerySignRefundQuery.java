package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class QuerySignRefundQuery extends BaseQuery {

    /**
     * <p>开发者侧退款单的单号，长度<=64byte</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">pay_order_id ， pay_refund_id ， out_pay_refund_no 三选一</li></ul> 选填
     */
    private String out_pay_refund_no;
    /**
     * <p>平台侧代扣单的单号，长度<=64byte。</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">pay_order_id ， pay_refund_id ， out_pay_refund_no 三选一</li></ul> 选填
     */
    private String pay_order_id;
    /**
     * <p>平台侧退款单的单号，长度<=64byte</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">pay_order_id ， pay_refund_id ， out_pay_refund_no 三选一</li></ul> 选填
     */
    private String pay_refund_id;

    public String getOut_pay_refund_no() {
        return out_pay_refund_no;
    }

    public QuerySignRefundQuery setOut_pay_refund_no(String out_pay_refund_no) {
        this.out_pay_refund_no = out_pay_refund_no;
        return this;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public QuerySignRefundQuery setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public String getPay_refund_id() {
        return pay_refund_id;
    }

    public QuerySignRefundQuery setPay_refund_id(String pay_refund_id) {
        this.pay_refund_id = pay_refund_id;
        return this;
    }

    public static QuerySignRefundQueryBuilder builder() {
        return new QuerySignRefundQueryBuilder();
    }

    public static final class QuerySignRefundQueryBuilder {
        private String out_pay_refund_no;
        private String pay_order_id;
        private String pay_refund_id;
        private Integer tenantId;
        private String clientKey;

        private QuerySignRefundQueryBuilder() {
        }

        public QuerySignRefundQueryBuilder outPayRefundNo(String outPayRefundNo) {
            this.out_pay_refund_no = outPayRefundNo;
            return this;
        }

        public QuerySignRefundQueryBuilder payOrderId(String payOrderId) {
            this.pay_order_id = payOrderId;
            return this;
        }

        public QuerySignRefundQueryBuilder payRefundId(String payRefundId) {
            this.pay_refund_id = payRefundId;
            return this;
        }

        public QuerySignRefundQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QuerySignRefundQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QuerySignRefundQuery build() {
            QuerySignRefundQuery querySignRefundQuery = new QuerySignRefundQuery();
            querySignRefundQuery.setOut_pay_refund_no(out_pay_refund_no);
            querySignRefundQuery.setPay_order_id(pay_order_id);
            querySignRefundQuery.setPay_refund_id(pay_refund_id);
            querySignRefundQuery.setTenantId(tenantId);
            querySignRefundQuery.setClientKey(clientKey);
            return querySignRefundQuery;
        }
    }
}
