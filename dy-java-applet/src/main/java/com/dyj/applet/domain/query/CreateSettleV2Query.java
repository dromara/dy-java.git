package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class CreateSettleV2Query extends BaseQuery {
    /**
     * 开发者自定义透传字段，长度 <= 2048 字节，不支持二进制 选填
     */
    private String cp_extra;
    /**
     * 开平侧item_order_id, 按券分账时必填，长度 <= 64 字节注意：2023.03.31号后下单的订单才可以使用该字段进行按券分账 选填
     */
    private String item_order_id;
    /**
     * 分账结果通知地址，若不填，默认使用在行业模板配置-消息通知中指定的回调地址 选填
     */
    private String notify_url;
    /**
     * 开发者侧订单 id，长度 <= 64 字节，与唯一 order_id 关联
     */
    private String out_order_no;
    /**
     * 开发者侧分账单 id，分账请求的唯一标识，长度 <= 64 字节
     */
    private String out_settle_no;
    /**
     * 分账描述，长度 <= 512 字节
     */
    private String settle_desc;
    /**
     * 其他分账方（除卖家之外的），长度 <= 512 字节 选填
     */
    private String settle_params;

    public String getCp_extra() {
        return cp_extra;
    }

    public CreateSettleV2Query setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public CreateSettleV2Query setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public CreateSettleV2Query setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public CreateSettleV2Query setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getOut_settle_no() {
        return out_settle_no;
    }

    public CreateSettleV2Query setOut_settle_no(String out_settle_no) {
        this.out_settle_no = out_settle_no;
        return this;
    }

    public String getSettle_desc() {
        return settle_desc;
    }

    public CreateSettleV2Query setSettle_desc(String settle_desc) {
        this.settle_desc = settle_desc;
        return this;
    }

    public String getSettle_params() {
        return settle_params;
    }

    public CreateSettleV2Query setSettle_params(String settle_params) {
        this.settle_params = settle_params;
        return this;
    }

    public static CreateSettleV2QueryBuilder builder() {
        return new CreateSettleV2QueryBuilder();
    }

    public static final class CreateSettleV2QueryBuilder {
        private String cp_extra;
        private String item_order_id;
        private String notify_url;
        private String out_order_no;
        private String out_settle_no;
        private String settle_desc;
        private String settle_params;
        private Integer tenantId;
        private String clientKey;

        private CreateSettleV2QueryBuilder() {
        }

        public CreateSettleV2QueryBuilder cpExtra(String cpExtra) {
            this.cp_extra = cpExtra;
            return this;
        }

        public CreateSettleV2QueryBuilder itemOrderId(String itemOrderId) {
            this.item_order_id = itemOrderId;
            return this;
        }

        public CreateSettleV2QueryBuilder notifyUrl(String notifyUrl) {
            this.notify_url = notifyUrl;
            return this;
        }

        public CreateSettleV2QueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public CreateSettleV2QueryBuilder outSettleNo(String outSettleNo) {
            this.out_settle_no = outSettleNo;
            return this;
        }

        public CreateSettleV2QueryBuilder settleDesc(String settleDesc) {
            this.settle_desc = settleDesc;
            return this;
        }

        public CreateSettleV2QueryBuilder settleParams(String settleParams) {
            this.settle_params = settleParams;
            return this;
        }

        public CreateSettleV2QueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateSettleV2QueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateSettleV2Query build() {
            CreateSettleV2Query createSettleV2Query = new CreateSettleV2Query();
            createSettleV2Query.setCp_extra(cp_extra);
            createSettleV2Query.setItem_order_id(item_order_id);
            createSettleV2Query.setNotify_url(notify_url);
            createSettleV2Query.setOut_order_no(out_order_no);
            createSettleV2Query.setOut_settle_no(out_settle_no);
            createSettleV2Query.setSettle_desc(settle_desc);
            createSettleV2Query.setSettle_params(settle_params);
            createSettleV2Query.setTenantId(tenantId);
            createSettleV2Query.setClientKey(clientKey);
            return createSettleV2Query;
        }
    }
}
