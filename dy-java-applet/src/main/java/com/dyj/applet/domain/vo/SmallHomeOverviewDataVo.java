package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.SmallHomeOverviewData;

public class SmallHomeOverviewDataVo {
    /**
     *
     * 小程序app_id
     */
    private String app_id;
    /**
     *
     * 小程序名称
     */
    private String app_name;
    private SmallHomeOverviewData over_view_data;

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public SmallHomeOverviewData getOver_view_data() {
        return over_view_data;
    }

    public void setOver_view_data(SmallHomeOverviewData over_view_data) {
        this.over_view_data = over_view_data;
    }
}
