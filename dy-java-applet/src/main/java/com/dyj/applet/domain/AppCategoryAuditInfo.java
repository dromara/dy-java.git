package com.dyj.applet.domain;

import java.util.List;

public class AppCategoryAuditInfo {

    /**
     * 服务类目Ids, etc. "158,159,259"
     */
    private String category;

    /**
     * 添加服务类目的资质审核状态 1-审核中 2-审核通过 3-审核拒绝
     */
    private Integer category_audit_state;

    /**
     *
     */
    private List<CategoryCert> category_cert;

    /**
     * 已上传资质ids, etc. "79,117,158"
     */
    private String category_cert_ids;

    /**
     * 服务类目名称, etc. "社交类-社交-直播"
     */
    private String category_name;

    /**
     * 审核拒绝原因
     */
    private String reason;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getCategory_audit_state() {
        return category_audit_state;
    }

    public void setCategory_audit_state(Integer category_audit_state) {
        this.category_audit_state = category_audit_state;
    }

    public List<CategoryCert> getCategory_cert() {
        return category_cert;
    }

    public void setCategory_cert(List<CategoryCert> category_cert) {
        this.category_cert = category_cert;
    }

    public String getCategory_cert_ids() {
        return category_cert_ids;
    }

    public void setCategory_cert_ids(String category_cert_ids) {
        this.category_cert_ids = category_cert_ids;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
