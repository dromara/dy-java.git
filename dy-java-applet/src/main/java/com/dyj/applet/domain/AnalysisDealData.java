package com.dyj.applet.domain;

public class AnalysisDealData {

    /**
     * 创建订单金额（单位：分）
     */
    private Long create_order_amount;
    /**
     * 创建订单数
     */
    private Long create_order_count;
    /**
     * 支付订单金额（单位：分）
     */
    private Long pay_order_amount;
    /**
     * 支付订单数
     */
    private Long pay_order_count;

    /**
     * 退款成功金额
     */
    private Long refund_success_order_amount;

    /**
     * 退款成功订单数
     */
    private Long refund_success_order_cnt;
    /**
     * 退款成功用户数
     */
    private Long refund_success_user_cnt;
    /**
     * 动销商品数，该时间段内创建的订单，且销量>=1的商品sku数量
     */
    private Long sale_product_cnt;
    /**
     * 支付人数，该时间段内创建的订单，且订单支付成功的去重客户数
     */
    private Long success_user_cnt;
    /**
     * 日期，格式为“2006-01-02”
     */
    private String time;


    public Long getCreate_order_amount() {
        return create_order_amount;
    }

    public void setCreate_order_amount(Long create_order_amount) {
        this.create_order_amount = create_order_amount;
    }

    public Long getCreate_order_count() {
        return create_order_count;
    }

    public void setCreate_order_count(Long create_order_count) {
        this.create_order_count = create_order_count;
    }

    public Long getPay_order_amount() {
        return pay_order_amount;
    }

    public void setPay_order_amount(Long pay_order_amount) {
        this.pay_order_amount = pay_order_amount;
    }

    public Long getPay_order_count() {
        return pay_order_count;
    }

    public void setPay_order_count(Long pay_order_count) {
        this.pay_order_count = pay_order_count;
    }

    public Long getRefund_success_order_amount() {
        return refund_success_order_amount;
    }

    public void setRefund_success_order_amount(Long refund_success_order_amount) {
        this.refund_success_order_amount = refund_success_order_amount;
    }

    public Long getRefund_success_order_cnt() {
        return refund_success_order_cnt;
    }

    public void setRefund_success_order_cnt(Long refund_success_order_cnt) {
        this.refund_success_order_cnt = refund_success_order_cnt;
    }

    public Long getRefund_success_user_cnt() {
        return refund_success_user_cnt;
    }

    public void setRefund_success_user_cnt(Long refund_success_user_cnt) {
        this.refund_success_user_cnt = refund_success_user_cnt;
    }

    public Long getSale_product_cnt() {
        return sale_product_cnt;
    }

    public void setSale_product_cnt(Long sale_product_cnt) {
        this.sale_product_cnt = sale_product_cnt;
    }

    public Long getSuccess_user_cnt() {
        return success_user_cnt;
    }

    public void setSuccess_user_cnt(Long success_user_cnt) {
        this.success_user_cnt = success_user_cnt;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
