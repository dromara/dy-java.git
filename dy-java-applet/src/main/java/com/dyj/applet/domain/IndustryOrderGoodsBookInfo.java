package com.dyj.applet.domain;

/**
 * 预约信息
 */
public class IndustryOrderGoodsBookInfo {

    /**
     * 预约类型，
     * 1：不需要预约
     * 2：在线预约
     */
    private Integer book_type;

    /**
     * 取消政策，
     * 1：预约后不可取消
     * 2：预约后可取消
     * 3：预约中可取消，预约成功须提前 x 小时取消
     * 选填
     */
    private Integer cancel_policy;

    /**
     * 提前取消的小时限制
     * 选填
     */
    private Integer cancel_advance_hour;

    public Integer getBook_type() {
        return book_type;
    }

    public IndustryOrderGoodsBookInfo setBook_type(Integer book_type) {
        this.book_type = book_type;
        return this;
    }

    public Integer getCancel_policy() {
        return cancel_policy;
    }

    public IndustryOrderGoodsBookInfo setCancel_policy(Integer cancel_policy) {
        this.cancel_policy = cancel_policy;
        return this;
    }

    public Integer getCancel_advance_hour() {
        return cancel_advance_hour;
    }

    public IndustryOrderGoodsBookInfo setCancel_advance_hour(Integer cancel_advance_hour) {
        this.cancel_advance_hour = cancel_advance_hour;
        return this;
    }
}
