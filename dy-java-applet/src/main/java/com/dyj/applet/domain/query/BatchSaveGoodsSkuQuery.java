package com.dyj.applet.domain.query;

import com.dyj.applet.domain.SkuStruct;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 13:47
 **/
public class BatchSaveGoodsSkuQuery extends BaseQuery {
    /**
     * 商品id
     */
    private String product_id;
    /**
     *
     * 商品外部id
     */
    private String out_id;
    /**
     * SKU列表
     */
    private List<SkuStruct> sku_list;

    public static BatchSaveGoodsSkuQueryBuilder builder() {
        return new BatchSaveGoodsSkuQueryBuilder();
    }

    public static class BatchSaveGoodsSkuQueryBuilder {
        private String productId;
        private String outId;
        private List<SkuStruct> skuList;
        private Integer tenantId;
        private String clientKey;

        public BatchSaveGoodsSkuQueryBuilder productId(String productId) {
            this.productId = productId;
            return this;
        }
        public BatchSaveGoodsSkuQueryBuilder outId(String outId) {
            this.outId = outId;
            return this;
        }
        public BatchSaveGoodsSkuQueryBuilder skuList(List<SkuStruct> skuList) {
            this.skuList = skuList;
            return this;
        }
        public BatchSaveGoodsSkuQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }
        public BatchSaveGoodsSkuQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }
        public BatchSaveGoodsSkuQuery build() {
            BatchSaveGoodsSkuQuery batchSaveGoodsSkuQuery = new BatchSaveGoodsSkuQuery();
            batchSaveGoodsSkuQuery.setProduct_id(productId);
            batchSaveGoodsSkuQuery.setOut_id(outId);
            batchSaveGoodsSkuQuery.setSku_list(skuList);
            batchSaveGoodsSkuQuery.setTenantId(tenantId);
            batchSaveGoodsSkuQuery.setClientKey(clientKey);
            return batchSaveGoodsSkuQuery;
        }

    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }

    public List<SkuStruct> getSku_list() {
        return sku_list;
    }

    public void setSku_list(List<SkuStruct> sku_list) {
        this.sku_list = sku_list;
    }
}
