package com.dyj.applet.domain;

import java.util.List;

public class QuerySubscriptionTemplate {

    /**
     * 类目名称
     */
    private String category_name;
    /**
     * 订阅消息类型
     */
    private Integer classification;
    /**
     * 模板id
     */
    private Long template_id;
    /**
     * 模板标题
     */
    private String title;
    /**
     * 模版适用的宿主APP列表，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条
     */
    private List<String> host_list;
    /**
     * 模板关键字列表
     */
    private List<String> keyword_list;

    public String getCategory_name() {
        return category_name;
    }

    public QuerySubscriptionTemplate setCategory_name(String category_name) {
        this.category_name = category_name;
        return this;
    }

    public Integer getClassification() {
        return classification;
    }

    public QuerySubscriptionTemplate setClassification(Integer classification) {
        this.classification = classification;
        return this;
    }

    public Long getTemplate_id() {
        return template_id;
    }

    public QuerySubscriptionTemplate setTemplate_id(Long template_id) {
        this.template_id = template_id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public QuerySubscriptionTemplate setTitle(String title) {
        this.title = title;
        return this;
    }

    public List<String> getHost_list() {
        return host_list;
    }

    public QuerySubscriptionTemplate setHost_list(List<String> host_list) {
        this.host_list = host_list;
        return this;
    }

    public List<String> getKeyword_list() {
        return keyword_list;
    }

    public QuerySubscriptionTemplate setKeyword_list(List<String> keyword_list) {
        this.keyword_list = keyword_list;
        return this;
    }
}
