package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-27 18:50
 **/
public class UserTaskInfo {

    /**
     * 任务是否已完成
     */
    private String completed;

    /**
     * 成功数
     */
    private Long success_count;

    /**
     * 目标数
     */
    private Long target_count;
    /**
     * 任务ID
     */
    private String task_id;
    /**
     * 错误信息
     */
    private String err_msg;
    /**
     * 错误码
     */
    private Integer err_no;

    /**
     * 任务是否有效
     */
    private Boolean is_valid;

    /**
     * 任务最多参与次数
     */
    private Long max_count;
    /**
     * 视频信息
     */
    private List<UserTaskVideoInfo> video_info;

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public Long getSuccess_count() {
        return success_count;
    }

    public void setSuccess_count(Long success_count) {
        this.success_count = success_count;
    }

    public Long getTarget_count() {
        return target_count;
    }

    public void setTarget_count(Long target_count) {
        this.target_count = target_count;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getErr_msg() {
        return err_msg;
    }

    public void setErr_msg(String err_msg) {
        this.err_msg = err_msg;
    }

    public Integer getErr_no() {
        return err_no;
    }

    public void setErr_no(Integer err_no) {
        this.err_no = err_no;
    }

    public Boolean getIs_valid() {
        return is_valid;
    }

    public void setIs_valid(Boolean is_valid) {
        this.is_valid = is_valid;
    }

    public List<UserTaskVideoInfo> getVideo_info() {
        return video_info;
    }

    public void setVideo_info(List<UserTaskVideoInfo> video_info) {
        this.video_info = video_info;
    }
}
