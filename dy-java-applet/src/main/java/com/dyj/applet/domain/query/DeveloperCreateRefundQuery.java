package com.dyj.applet.domain.query;

import com.dyj.applet.domain.CreateRefundTimesCardRefundType;
import com.dyj.applet.domain.RefundItemOrderDetail;
import com.dyj.applet.domain.RefundOrderEntrySchema;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * 生活服务交易系统->退货退款->开发者发起退款请求值
 */
public class DeveloperCreateRefundQuery extends BaseQuery {

    /**
     * 开发者侧订单号，长度 <= 64 byte
     */
    private String out_order_no;

    /**
     * 开发者侧退款单号，长度 <= 64 byte
     */
    private String out_refund_no;

    /**
     * 开发者自定义透传字段，不支持二进制，长度 <= 2048 byte
     */
    private String cp_extra;

    /**
     * <p>退款单的跳转的schema，参考<a href="/docs/resource/zh-CN/mini-app/develop/server/trade-system/general/common-param" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">通用参数-关于 xxx_entry_schema 的前置说明</a></p>
     */
    private RefundOrderEntrySchema order_entry_schema;
    /**
     * <p>退款结果通知地址，必须是 HTTPS 类型， 长度 &lt;= 512 byte 。若不填，则默认使用在解决方案配置-消息通知中指定的回调地址，配置方式参考<a href="/docs/resource/zh-CN/mini-app/open-capacity/Industry/industry_mode/guide/" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">解决方案配置文档</a></p> 选填
     */
    private String notify_url;

    /**
     * 需要发起退款的商品单信息 选填
     */
    private List<RefundItemOrderDetail> item_order_detail;

    /**
     * 次卡退款必填 选填
     */
    private CreateRefundTimesCardRefundType times_card_refund_param;

    public String getOut_order_no() {
        return out_order_no;
    }

    public DeveloperCreateRefundQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getOut_refund_no() {
        return out_refund_no;
    }

    public DeveloperCreateRefundQuery setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
        return this;
    }

    public String getCp_extra() {
        return cp_extra;
    }

    public DeveloperCreateRefundQuery setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public RefundOrderEntrySchema getOrder_entry_schema() {
        return order_entry_schema;
    }

    public DeveloperCreateRefundQuery setOrder_entry_schema(RefundOrderEntrySchema order_entry_schema) {
        this.order_entry_schema = order_entry_schema;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public DeveloperCreateRefundQuery setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public List<RefundItemOrderDetail> getItem_order_detail() {
        return item_order_detail;
    }

    public DeveloperCreateRefundQuery setItem_order_detail(List<RefundItemOrderDetail> item_order_detail) {
        this.item_order_detail = item_order_detail;
        return this;
    }

    public CreateRefundTimesCardRefundType getTimes_card_refund_param() {
        return times_card_refund_param;
    }

    public DeveloperCreateRefundQuery setTimes_card_refund_param(CreateRefundTimesCardRefundType times_card_refund_param) {
        this.times_card_refund_param = times_card_refund_param;
        return this;
    }

    public static DeveloperCreateRefundQueryBuilder builder() {
        return new DeveloperCreateRefundQueryBuilder();
    }

    public static final class DeveloperCreateRefundQueryBuilder {
        private String out_order_no;
        private String out_refund_no;
        private String cp_extra;
        private RefundOrderEntrySchema order_entry_schema;
        private String notify_url;
        private List<RefundItemOrderDetail> item_order_detail;
        private CreateRefundTimesCardRefundType times_card_refund_param;
        private Integer tenantId;
        private String clientKey;

        private DeveloperCreateRefundQueryBuilder() {
        }

        public DeveloperCreateRefundQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public DeveloperCreateRefundQueryBuilder outRefundNo(String outRefundNo) {
            this.out_refund_no = outRefundNo;
            return this;
        }

        public DeveloperCreateRefundQueryBuilder cpExtra(String cpExtra) {
            this.cp_extra = cpExtra;
            return this;
        }

        public DeveloperCreateRefundQueryBuilder orderEntrySchema(RefundOrderEntrySchema orderEntrySchema) {
            this.order_entry_schema = orderEntrySchema;
            return this;
        }

        public DeveloperCreateRefundQueryBuilder notifyUrl(String notifyUrl) {
            this.notify_url = notifyUrl;
            return this;
        }

        public DeveloperCreateRefundQueryBuilder itemOrderDetail(List<RefundItemOrderDetail> itemOrderDetail) {
            this.item_order_detail = itemOrderDetail;
            return this;
        }

        public DeveloperCreateRefundQueryBuilder timesCardRefundParam(CreateRefundTimesCardRefundType timesCardRefundParam) {
            this.times_card_refund_param = timesCardRefundParam;
            return this;
        }

        public DeveloperCreateRefundQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public DeveloperCreateRefundQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public DeveloperCreateRefundQuery build() {
            DeveloperCreateRefundQuery developerCreateRefundQuery = new DeveloperCreateRefundQuery();
            developerCreateRefundQuery.setOut_order_no(out_order_no);
            developerCreateRefundQuery.setOut_refund_no(out_refund_no);
            developerCreateRefundQuery.setCp_extra(cp_extra);
            developerCreateRefundQuery.setOrder_entry_schema(order_entry_schema);
            developerCreateRefundQuery.setNotify_url(notify_url);
            developerCreateRefundQuery.setItem_order_detail(item_order_detail);
            developerCreateRefundQuery.setTimes_card_refund_param(times_card_refund_param);
            developerCreateRefundQuery.setTenantId(tenantId);
            developerCreateRefundQuery.setClientKey(clientKey);
            return developerCreateRefundQuery;
        }
    }
}
