package com.dyj.applet.domain;

public class AnalysisComponentSourceData {

    /**
     * 场景值
     */
    private String Scenes;
    /**
     * 场景值名称
     */
    private String ScenesName;

    private ComponentSourceData Data;

    public String getScenes() {
        return Scenes;
    }

    public void setScenes(String scenes) {
        Scenes = scenes;
    }

    public String getScenesName() {
        return ScenesName;
    }

    public void setScenesName(String scenesName) {
        ScenesName = scenesName;
    }

    public ComponentSourceData getData() {
        return Data;
    }

    public void setData(ComponentSourceData data) {
        Data = data;
    }

    public static class ComponentSourceData {
        /**
         * 组件点击次数
         */
        private Long ComponentClickPv;
        /**
         * 组件点击用户数
         */
        private Long ComponentClickUv;
        /**
         * 组件留资次数
         */
        private Long ComponentReportPv;
        /**
         * 组件留资用户数
         */
        private Long ComponentReportUv;
        /**
         * 组件曝光次数
         */
        private Long ComponentShowPv;
        /**
         * 组件访问用户数
         */
        private Long ComponentShowUv;

        public Long getComponentClickPv() {
            return ComponentClickPv;
        }

        public void setComponentClickPv(Long componentClickPv) {
            ComponentClickPv = componentClickPv;
        }

        public Long getComponentClickUv() {
            return ComponentClickUv;
        }

        public void setComponentClickUv(Long componentClickUv) {
            ComponentClickUv = componentClickUv;
        }

        public Long getComponentReportPv() {
            return ComponentReportPv;
        }

        public void setComponentReportPv(Long componentReportPv) {
            ComponentReportPv = componentReportPv;
        }

        public Long getComponentReportUv() {
            return ComponentReportUv;
        }

        public void setComponentReportUv(Long componentReportUv) {
            ComponentReportUv = componentReportUv;
        }

        public Long getComponentShowPv() {
            return ComponentShowPv;
        }

        public void setComponentShowPv(Long componentShowPv) {
            ComponentShowPv = componentShowPv;
        }

        public Long getComponentShowUv() {
            return ComponentShowUv;
        }

        public void setComponentShowUv(Long componentShowUv) {
            ComponentShowUv = componentShowUv;
        }
    }
}
