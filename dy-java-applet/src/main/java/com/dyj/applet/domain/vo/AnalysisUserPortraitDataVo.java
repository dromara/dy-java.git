package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisAttribute;

import java.util.List;

public class AnalysisUserPortraitDataVo {

    /**
     * 总用户数
     */
    private Long total_user;

    /**
     * 用户年龄画像
     */
    private List<AnalysisAttribute> age;
    /**
     * 用户城市画像
     */
    private List<AnalysisAttribute> city;
    /**
     * 用户性别画像
     */
    private List<AnalysisAttribute> gender;
    /**
     * 用户省份画像
     */
    private List<AnalysisAttribute> province;

    public Long getTotal_user() {
        return total_user;
    }

    public void setTotal_user(Long total_user) {
        this.total_user = total_user;
    }

    public List<AnalysisAttribute> getAge() {
        return age;
    }

    public void setAge(List<AnalysisAttribute> age) {
        this.age = age;
    }

    public List<AnalysisAttribute> getCity() {
        return city;
    }

    public void setCity(List<AnalysisAttribute> city) {
        this.city = city;
    }

    public List<AnalysisAttribute> getGender() {
        return gender;
    }

    public void setGender(List<AnalysisAttribute> gender) {
        this.gender = gender;
    }

    public List<AnalysisAttribute> getProvince() {
        return province;
    }

    public void setProvince(List<AnalysisAttribute> province) {
        this.province = province;
    }


}
