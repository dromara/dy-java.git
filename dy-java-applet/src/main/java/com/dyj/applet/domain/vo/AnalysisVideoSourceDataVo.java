package com.dyj.applet.domain.vo;

import java.util.List;

public class AnalysisVideoSourceDataVo {

    private List<AnalysisVideoSourceData> DataList;

    public static class AnalysisVideoSourceData{
        /**
         * 场景
         */
        private String Scenes;
        /**
         * 场景名称
         */
        private String ScenesName;
        /**
         * 公共数据
         */
        private AnalysisVideoSourceCommonData CommonData;

        public String getScenes() {
            return Scenes;
        }

        public void setScenes(String scenes) {
            Scenes = scenes;
        }

        public String getScenesName() {
            return ScenesName;
        }

        public void setScenesName(String scenesName) {
            ScenesName = scenesName;
        }

        public AnalysisVideoSourceCommonData getCommonData() {
            return CommonData;
        }

        public void setCommonData(AnalysisVideoSourceCommonData commonData) {
            CommonData = commonData;
        }
    }


    public static class AnalysisVideoSourceCommonData{
        /**
         * 进入小程序次数
         */
        private Long MpDrainagePv;
        /**
         * 进入小程序人数
         */
        private Long MpDrainageUv;
        /**
         *
         * 小程序曝光次数
         */
        private Long MpShowPv;
        /**
         *
         * 小程序曝光人数
         */
        private Long MpShowUv;

        public Long getMpDrainagePv() {
            return MpDrainagePv;
        }

        public void setMpDrainagePv(Long mpDrainagePv) {
            MpDrainagePv = mpDrainagePv;
        }

        public Long getMpDrainageUv() {
            return MpDrainageUv;
        }

        public void setMpDrainageUv(Long mpDrainageUv) {
            MpDrainageUv = mpDrainageUv;
        }

        public Long getMpShowPv() {
            return MpShowPv;
        }

        public void setMpShowPv(Long mpShowPv) {
            MpShowPv = mpShowPv;
        }

        public Long getMpShowUv() {
            return MpShowUv;
        }

        public void setMpShowUv(Long mpShowUv) {
            MpShowUv = mpShowUv;
        }
    }

}
