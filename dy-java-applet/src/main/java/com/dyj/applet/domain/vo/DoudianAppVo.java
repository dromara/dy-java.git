package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-06-12 14:13
 **/
public class DoudianAppVo {

    /**
     * 应用绑定状态
     * 1: 账户未绑定
     * 2: 应用未绑定
     * 3: 应用已绑定
     */
    private String bind_status;

    /**
     * 抖店应用appid
     */
    private String doudian_appid;

    /**
     * 抖店应用名称
     */
    private String doudian_app_name;

    public String getBind_status() {
        return bind_status;
    }

    public void setBind_status(String bind_status) {
        this.bind_status = bind_status;
    }

    public String getDoudian_appid() {
        return doudian_appid;
    }

    public void setDoudian_appid(String doudian_appid) {
        this.doudian_appid = doudian_appid;
    }

    public String getDoudian_app_name() {
        return doudian_app_name;
    }

    public void setDoudian_app_name(String doudian_app_name) {
        this.doudian_app_name = doudian_app_name;
    }
}
