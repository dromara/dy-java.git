package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-07-12 11:49
 **/
public class CapacityApplyInfo {

    /**
     * 使用场景描述
     */
    private String scene_desc;

    /**
     * 场景示例图列表，使用上传素材接口获取到的路径
     */
    private List<String> img_uri_list;

    public static CapacityApplyInfoBuilder builder() {
        return new CapacityApplyInfoBuilder();
    }

    public static class CapacityApplyInfoBuilder {
        /**
         * 使用场景描述
         */
        private String sceneDesc;

        /**
         * 场景示例图列表，使用上传素材接口获取到的路径
         */
        private List<String> imgUriList;



        public CapacityApplyInfoBuilder sceneDesc(String sceneDesc) {
            this.sceneDesc = sceneDesc;
            return this;
        }

        public CapacityApplyInfoBuilder imgUriList(List<String> imgUriList) {
            this.imgUriList = imgUriList;
            return this;
        }

        public CapacityApplyInfo build() {
            CapacityApplyInfo capacityApplyInfo = new CapacityApplyInfo();
            capacityApplyInfo.setScene_desc(sceneDesc);
            capacityApplyInfo.setImg_uri_list(imgUriList);
            return capacityApplyInfo;
        }
    }

    public String getScene_desc() {
        return scene_desc;
    }

    public void setScene_desc(String scene_desc) {
        this.scene_desc = scene_desc;
    }

    public List<String> getImg_uri_list() {
        return img_uri_list;
    }

    public void setImg_uri_list(List<String> img_uri_list) {
        this.img_uri_list = img_uri_list;
    }
}
