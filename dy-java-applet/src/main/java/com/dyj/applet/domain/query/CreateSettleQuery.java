package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 发起分账请求值
 */
public class CreateSettleQuery extends BaseQuery {

    /**
     * 小程序id
     */
    private String app_id;
    /**
     * <p>开发者自定义透传字段，不支持二进制，会在查询分账接口cp_extra字段原样返回，长度 <= 2048 字节</p> 选填
     */
    private String ext;
    /**
     * <p>开平侧item_order_id, 按券分账时必填，长度 <= 64 字节</p> 选填
     */
    private String item_order_id;
    /**
     * <p>分账结果通知地址，若不填，默认使用在行业模板配置-消息通知中指定的回调地址</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">必须是 HTTPS 类型</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">长度 <= 512 字节</li></ul> 选填
     */
    private String notify_url;
    /**
     * <p>开发者侧订单 id，长度 <= 64 字节，与唯一 order_id 关联</p>
     */
    private String out_order_no;
    /**
     * <p>开发者侧分账单 id，分账请求的唯一标识，长度 &lt;= 64 字节</p>
     */
    private String out_settle_no;
    /**
     * <p>分账描述，长度 &lt;= 512 字节</p>
     */
    private String settle_desc;
    /**
     * <p>其他分账方（除卖家之外的），长度 &lt;= 512 字节</p><ul><li>merchant_uid  string <a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/open-capacity/guaranteed-payment/merchant" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">进件商户 id</a>（除了卖家以外的其他分账方）</li><li>amount int64 分账金额，单位[分]，amount&gt;0</li></ul> 选填
     */
    private String settle_params;

    public String getApp_id() {
        return app_id;
    }

    public CreateSettleQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getExt() {
        return ext;
    }

    public CreateSettleQuery setExt(String ext) {
        this.ext = ext;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public CreateSettleQuery setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public CreateSettleQuery setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public CreateSettleQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getOut_settle_no() {
        return out_settle_no;
    }

    public CreateSettleQuery setOut_settle_no(String out_settle_no) {
        this.out_settle_no = out_settle_no;
        return this;
    }

    public String getSettle_desc() {
        return settle_desc;
    }

    public CreateSettleQuery setSettle_desc(String settle_desc) {
        this.settle_desc = settle_desc;
        return this;
    }

    public String getSettle_params() {
        return settle_params;
    }

    public CreateSettleQuery setSettle_params(String settle_params) {
        this.settle_params = settle_params;
        return this;
    }

    public static CreateSettleQueryBuilder builder() {
        return new CreateSettleQueryBuilder();
    }

    public static final class CreateSettleQueryBuilder {
        private String app_id;
        private String ext;
        private String item_order_id;
        private String notify_url;
        private String out_order_no;
        private String out_settle_no;
        private String settle_desc;
        private String settle_params;
        private Integer tenantId;
        private String clientKey;

        private CreateSettleQueryBuilder() {
        }

        public CreateSettleQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public CreateSettleQueryBuilder ext(String ext) {
            this.ext = ext;
            return this;
        }

        public CreateSettleQueryBuilder itemOrderId(String itemOrderId) {
            this.item_order_id = itemOrderId;
            return this;
        }

        public CreateSettleQueryBuilder notifyUrl(String notifyUrl) {
            this.notify_url = notifyUrl;
            return this;
        }

        public CreateSettleQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public CreateSettleQueryBuilder outSettleNo(String outSettleNo) {
            this.out_settle_no = outSettleNo;
            return this;
        }

        public CreateSettleQueryBuilder settleDesc(String settleDesc) {
            this.settle_desc = settleDesc;
            return this;
        }

        public CreateSettleQueryBuilder settleParams(String settleParams) {
            this.settle_params = settleParams;
            return this;
        }

        public CreateSettleQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateSettleQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateSettleQuery build() {
            CreateSettleQuery createSettleQuery = new CreateSettleQuery();
            createSettleQuery.setApp_id(app_id);
            createSettleQuery.setExt(ext);
            createSettleQuery.setItem_order_id(item_order_id);
            createSettleQuery.setNotify_url(notify_url);
            createSettleQuery.setOut_order_no(out_order_no);
            createSettleQuery.setOut_settle_no(out_settle_no);
            createSettleQuery.setSettle_desc(settle_desc);
            createSettleQuery.setSettle_params(settle_params);
            createSettleQuery.setTenantId(tenantId);
            createSettleQuery.setClientKey(clientKey);
            return createSettleQuery;
        }
    }
}
