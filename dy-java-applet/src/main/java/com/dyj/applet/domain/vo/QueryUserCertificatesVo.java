package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QueryCertificatesOrderInfo;

import java.util.List;

public class QueryUserCertificatesVo {

    /**
     * 抖音订单列表
     */
    private List<QueryCertificatesOrderInfo> orders;

    /**
     * 该用户待使用订单总数
     */
    private Long total;

    public List<QueryCertificatesOrderInfo> getOrders() {
        return orders;
    }

    public QueryUserCertificatesVo setOrders(List<QueryCertificatesOrderInfo> orders) {
        this.orders = orders;
        return this;
    }

    public Long getTotal() {
        return total;
    }

    public QueryUserCertificatesVo setTotal(Long total) {
        this.total = total;
        return this;
    }
}
