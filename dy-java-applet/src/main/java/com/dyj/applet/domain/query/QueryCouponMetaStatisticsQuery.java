package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 查询券模板发放统计数据请求值
 */
public class QueryCouponMetaStatisticsQuery extends BaseQuery {

    /**
     * <p>开平接口发券活动ID</p> 选填
     */
    private String activity_id;
    /**
     * <p>小程序appid</p>
     */
    private String app_id;
    /**
     * <p>已创建的抖音开平券模板id</p>
     */
    private String coupon_meta_id;
    /**
     * <p>主播抖音号</p> 选填
     */
    private String talent_account;
    /**
     * <ul><li>主播open_id（主播在开发者小程序内的open_id）</li><li>talent_open_id、talent_account 两个字段只需填写一个，若全部填写以talent_account为准。</li></ul> 选填
     */
    private String talent_open_id;

    public String getActivity_id() {
        return activity_id;
    }

    public QueryCouponMetaStatisticsQuery setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public QueryCouponMetaStatisticsQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public QueryCouponMetaStatisticsQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public String getTalent_account() {
        return talent_account;
    }

    public QueryCouponMetaStatisticsQuery setTalent_account(String talent_account) {
        this.talent_account = talent_account;
        return this;
    }

    public String getTalent_open_id() {
        return talent_open_id;
    }

    public QueryCouponMetaStatisticsQuery setTalent_open_id(String talent_open_id) {
        this.talent_open_id = talent_open_id;
        return this;
    }

    public static QueryCouponMetaStatisticsQueryBuilder builder(){
        return new QueryCouponMetaStatisticsQueryBuilder();
    }

    public static final class QueryCouponMetaStatisticsQueryBuilder {
        private String activity_id;
        private String app_id;
        private String coupon_meta_id;
        private String talent_account;
        private String talent_open_id;
        private Integer tenantId;
        private String clientKey;

        private QueryCouponMetaStatisticsQueryBuilder() {
        }

        public QueryCouponMetaStatisticsQueryBuilder activityId(String activityId) {
            this.activity_id = activityId;
            return this;
        }

        public QueryCouponMetaStatisticsQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public QueryCouponMetaStatisticsQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public QueryCouponMetaStatisticsQueryBuilder talentAccount(String talentAccount) {
            this.talent_account = talentAccount;
            return this;
        }

        public QueryCouponMetaStatisticsQueryBuilder talentOpenId(String talentOpenId) {
            this.talent_open_id = talentOpenId;
            return this;
        }

        public QueryCouponMetaStatisticsQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryCouponMetaStatisticsQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryCouponMetaStatisticsQuery build() {
            QueryCouponMetaStatisticsQuery queryCouponMetaStatisticsQuery = new QueryCouponMetaStatisticsQuery();
            queryCouponMetaStatisticsQuery.setActivity_id(activity_id);
            queryCouponMetaStatisticsQuery.setApp_id(app_id);
            queryCouponMetaStatisticsQuery.setCoupon_meta_id(coupon_meta_id);
            queryCouponMetaStatisticsQuery.setTalent_account(talent_account);
            queryCouponMetaStatisticsQuery.setTalent_open_id(talent_open_id);
            queryCouponMetaStatisticsQuery.setTenantId(tenantId);
            queryCouponMetaStatisticsQuery.setClientKey(clientKey);
            return queryCouponMetaStatisticsQuery;
        }
    }
}
