package com.dyj.applet.domain;

/**
 * 订单 CPS 信息
 */
public class QueryCpsCpsItem {


    /**
     * <p>佣金，单位分</p>
     */
    private Long commission_amount;
    /**
     * <p>分佣比例，万分位</p>
     */
    private Integer commission_rate;
    /**
     * <p>达人抖音号</p>
     */
    private String commission_user_douyinid;
    /**
     * <p>达人抖音昵称</p>
     */
    private String commission_user_nickname;
    /**
     * <p>短视频/直播间 ID</p>
     */
    private Long item_id;
    /**
     * <p>抖音开平侧的商品单号，只存在交易系统</p>
     */
    private String item_order_id;
    /**
     * <p>售价，单位分</p>
     */
    private Long sell_amount;
    /**
     * <p>分佣类型：</p><ul><li>1：短视频</li><li>2：直播间</li></ul>
     */
    private Integer source_type;
    /**
     * <p>CPS 订单状态，交易系统下为子单状态</p><ul><li>0：未支付；</li><li>1：已支付(待使用)；</li><li>2：已退款(不分佣)；</li><li>3：已分账(已分佣)；</li></ul>
     */
    private Integer status;
    /**
     * <p>cps任务的id</p>
     */
    private String task_id;

    public Long getCommission_amount() {
        return commission_amount;
    }

    public QueryCpsCpsItem setCommission_amount(Long commission_amount) {
        this.commission_amount = commission_amount;
        return this;
    }

    public Integer getCommission_rate() {
        return commission_rate;
    }

    public QueryCpsCpsItem setCommission_rate(Integer commission_rate) {
        this.commission_rate = commission_rate;
        return this;
    }

    public String getCommission_user_douyinid() {
        return commission_user_douyinid;
    }

    public QueryCpsCpsItem setCommission_user_douyinid(String commission_user_douyinid) {
        this.commission_user_douyinid = commission_user_douyinid;
        return this;
    }

    public String getCommission_user_nickname() {
        return commission_user_nickname;
    }

    public QueryCpsCpsItem setCommission_user_nickname(String commission_user_nickname) {
        this.commission_user_nickname = commission_user_nickname;
        return this;
    }

    public Long getItem_id() {
        return item_id;
    }

    public QueryCpsCpsItem setItem_id(Long item_id) {
        this.item_id = item_id;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public QueryCpsCpsItem setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public Long getSell_amount() {
        return sell_amount;
    }

    public QueryCpsCpsItem setSell_amount(Long sell_amount) {
        this.sell_amount = sell_amount;
        return this;
    }

    public Integer getSource_type() {
        return source_type;
    }

    public QueryCpsCpsItem setSource_type(Integer source_type) {
        this.source_type = source_type;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public QueryCpsCpsItem setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getTask_id() {
        return task_id;
    }

    public QueryCpsCpsItem setTask_id(String task_id) {
        this.task_id = task_id;
        return this;
    }
}
