package com.dyj.applet.domain;

import java.util.List;

/**
 * 查询订单 CPS 返回
 */
public class QueryTransactionCps {

    /**
     * <p>抖音开平侧订单号</p>
     */
    private String order_id;
    /**
     * <p>开发者侧订单号，与 order_id 一一对应</p> 选填
     */
    private String out_order_no;
    /**
     * <p>订单总佣金，单位：<strong>分</strong></p>
     */
    private Long total_commission_amount;
    /**
     * <p>订单 CPS 信息</p>
     */
    private List<QueryCpsCpsItem> cps_item_list;

    public String getOrder_id() {
        return order_id;
    }

    public QueryTransactionCps setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QueryTransactionCps setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public Long getTotal_commission_amount() {
        return total_commission_amount;
    }

    public QueryTransactionCps setTotal_commission_amount(Long total_commission_amount) {
        this.total_commission_amount = total_commission_amount;
        return this;
    }

    public List<QueryCpsCpsItem> getCps_item_list() {
        return cps_item_list;
    }

    public QueryTransactionCps setCps_item_list(List<QueryCpsCpsItem> cps_item_list) {
        this.cps_item_list = cps_item_list;
        return this;
    }
}
