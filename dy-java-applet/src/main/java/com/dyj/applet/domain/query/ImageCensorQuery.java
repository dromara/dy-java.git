package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class ImageCensorQuery extends BaseQuery {

    /**
     * 小程序id
     */
    private String app_id;
    /**
     * 图片链接 选填
     */
    private String image;
    /**
     * 图片base64数据 选填
     */
    private String image_data;

    public String getApp_id() {
        return app_id;
    }

    public ImageCensorQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getImage() {
        return image;
    }

    public ImageCensorQuery setImage(String image) {
        this.image = image;
        return this;
    }

    public String getImage_data() {
        return image_data;
    }

    public ImageCensorQuery setImage_data(String image_data) {
        this.image_data = image_data;
        return this;
    }

    public static ImageCensorQueryBuilder builder() {
        return new ImageCensorQueryBuilder();
    }


    public static final class ImageCensorQueryBuilder {
        private String app_id;
        private String image;
        private String image_data;
        private Integer tenantId;
        private String clientKey;

        private ImageCensorQueryBuilder() {
        }

        public ImageCensorQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public ImageCensorQueryBuilder image(String image) {
            this.image = image;
            return this;
        }

        public ImageCensorQueryBuilder imageData(String imageData) {
            this.image_data = imageData;
            return this;
        }

        public ImageCensorQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public ImageCensorQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public ImageCensorQuery build() {
            ImageCensorQuery imageCensorQuery = new ImageCensorQuery();
            imageCensorQuery.setApp_id(app_id);
            imageCensorQuery.setImage(image);
            imageCensorQuery.setImage_data(image_data);
            imageCensorQuery.setTenantId(tenantId);
            imageCensorQuery.setClientKey(clientKey);
            return imageCensorQuery;
        }
    }
}
