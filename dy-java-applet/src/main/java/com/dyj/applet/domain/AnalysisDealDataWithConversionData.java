package com.dyj.applet.domain;

public class AnalysisDealDataWithConversionData {

    /**
     * 创建订单数
     */
    private Long CreateOrderCount;
    /**
     * 创建订单人数
     */
    private Long CreateUserCount;
    /**
     * 进入小程序次数
     */
    private Long MpDrainagePv;
    /**
     * 进入小程序人数
     */
    private Long MpDrainageUv;
    /**
     * 小程序曝光次数
     */
    private Long MpShowPv;
    /**
     * 小程序曝光人数
     */
    private Long MpShowUv;
    /**
     * 支付订单数
     */
    private Long PayOrderCount;
    /**
     * 支付订单人数
     */
    private Long PayPeopleCount;
    /**
     * 退款订单
     */
    private Long RefundOrderCount;
    /**
     * 退款人数
     */
    private Long RefundPeopleCount;

    public Long getCreateOrderCount() {
        return CreateOrderCount;
    }

    public void setCreateOrderCount(Long createOrderCount) {
        CreateOrderCount = createOrderCount;
    }

    public Long getCreateUserCount() {
        return CreateUserCount;
    }

    public void setCreateUserCount(Long createUserCount) {
        CreateUserCount = createUserCount;
    }

    public Long getMpDrainagePv() {
        return MpDrainagePv;
    }

    public void setMpDrainagePv(Long mpDrainagePv) {
        MpDrainagePv = mpDrainagePv;
    }

    public Long getMpDrainageUv() {
        return MpDrainageUv;
    }

    public void setMpDrainageUv(Long mpDrainageUv) {
        MpDrainageUv = mpDrainageUv;
    }

    public Long getMpShowPv() {
        return MpShowPv;
    }

    public void setMpShowPv(Long mpShowPv) {
        MpShowPv = mpShowPv;
    }

    public Long getMpShowUv() {
        return MpShowUv;
    }

    public void setMpShowUv(Long mpShowUv) {
        MpShowUv = mpShowUv;
    }

    public Long getPayOrderCount() {
        return PayOrderCount;
    }

    public void setPayOrderCount(Long payOrderCount) {
        PayOrderCount = payOrderCount;
    }

    public Long getPayPeopleCount() {
        return PayPeopleCount;
    }

    public void setPayPeopleCount(Long payPeopleCount) {
        PayPeopleCount = payPeopleCount;
    }

    public Long getRefundOrderCount() {
        return RefundOrderCount;
    }

    public void setRefundOrderCount(Long refundOrderCount) {
        RefundOrderCount = refundOrderCount;
    }

    public Long getRefundPeopleCount() {
        return RefundPeopleCount;
    }

    public void setRefundPeopleCount(Long refundPeopleCount) {
        RefundPeopleCount = refundPeopleCount;
    }
}
