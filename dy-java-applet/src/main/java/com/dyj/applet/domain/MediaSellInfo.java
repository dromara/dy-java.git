package com.dyj.applet.domain;

public class MediaSellInfo {

    /**
     * 短视频/直播间ID
     */
    private String content_id;
    /**
     * 该直播间/短视频的唯一标识
     */
    private String content_open_id;
    /**
     * 1:直播，2:短视频，3:全部
     */
    private Integer content_type;
    /**
     * 达人该直播间/短视频的带货GMV，单位分
     */
    private Long gmv;
    /**
     * 达人该直播间/短视频的观看人数/播放量
     */
    private Long play_cnt;
    /**
     * 达人该直播间/短视频的带货佣金，单位分
     */
    private Long talent_commission;
    /**
     * 达人该直播间/短视频的已核销GMV，单位分
     */
    private Long used_gmv;

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getContent_open_id() {
        return content_open_id;
    }

    public void setContent_open_id(String content_open_id) {
        this.content_open_id = content_open_id;
    }

    public Integer getContent_type() {
        return content_type;
    }

    public void setContent_type(Integer content_type) {
        this.content_type = content_type;
    }

    public Long getGmv() {
        return gmv;
    }

    public void setGmv(Long gmv) {
        this.gmv = gmv;
    }

    public Long getPlay_cnt() {
        return play_cnt;
    }

    public void setPlay_cnt(Long play_cnt) {
        this.play_cnt = play_cnt;
    }

    public Long getTalent_commission() {
        return talent_commission;
    }

    public void setTalent_commission(Long talent_commission) {
        this.talent_commission = talent_commission;
    }

    public Long getUsed_gmv() {
        return used_gmv;
    }

    public void setUsed_gmv(Long used_gmv) {
        this.used_gmv = used_gmv;
    }
}
