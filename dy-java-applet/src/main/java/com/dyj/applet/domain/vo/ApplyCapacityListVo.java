package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.ApplyCapacityInfo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-07-17 09:55
 **/
public class ApplyCapacityListVo {

    private List<ApplyCapacityInfo> app_capacity_info_list;

    public List<ApplyCapacityInfo> getApp_capacity_info_list() {
        return app_capacity_info_list;
    }

    public void setApp_capacity_info_list(List<ApplyCapacityInfo> app_capacity_info_list) {
        this.app_capacity_info_list = app_capacity_info_list;
    }
}
