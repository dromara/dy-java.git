package com.dyj.applet.domain.vo;

import java.util.List;

public class PriceCalculationDetail {

    /**
     * 算价类型，
     * •1：商户单维度，item 单价格按分摊比例计算。
     * •2：item 单维度，item 单价格由开发者计算。
     */
    private Integer calculation_type;

    /**
     * 商品算价结果
     * 选填
     */
    private List<GoodsDiscountDetail> goods_discount_detail;

    /**
     * 订单算价结果
     * 选填
     */
    private OrderDiscountDetail order_discount_detail;

    /**
     * 单商品算价结果
     * 选填
     */
    private List<ItemDiscountDetail> item_discount_detail;

}
