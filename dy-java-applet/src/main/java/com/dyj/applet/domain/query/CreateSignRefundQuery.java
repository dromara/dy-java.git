package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class CreateSignRefundQuery extends BaseQuery {

    /**
     * <p>退款结果回调地址，https开头</p> 选填
     */
    private String notify_url;
    /**
     * <p>开发者侧退款单的单号，长度<=64byte</p>
     */
    private String out_pay_refund_no;
    /**
     * <p>平台侧代扣单的单号，长度<=64byte</p>
     */
    private String pay_order_id;
    /**
     * <p>退款原因，长度<=256byte</p> 选填
     */
    private String refund_reason;
    /**
     * <p>退款总金额，单位[分]</p>
     */
    private Long refund_total_amount;

    public String getNotify_url() {
        return notify_url;
    }

    public CreateSignRefundQuery setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOut_pay_refund_no() {
        return out_pay_refund_no;
    }

    public CreateSignRefundQuery setOut_pay_refund_no(String out_pay_refund_no) {
        this.out_pay_refund_no = out_pay_refund_no;
        return this;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public CreateSignRefundQuery setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public String getRefund_reason() {
        return refund_reason;
    }

    public CreateSignRefundQuery setRefund_reason(String refund_reason) {
        this.refund_reason = refund_reason;
        return this;
    }

    public Long getRefund_total_amount() {
        return refund_total_amount;
    }

    public CreateSignRefundQuery setRefund_total_amount(Long refund_total_amount) {
        this.refund_total_amount = refund_total_amount;
        return this;
    }

    public static CreateSignRefundQueryBuilder builder() {
        return new CreateSignRefundQueryBuilder();
    }

    public static final class CreateSignRefundQueryBuilder {
        private String notify_url;
        private String out_pay_refund_no;
        private String pay_order_id;
        private String refund_reason;
        private Long refund_total_amount;
        private Integer tenantId;
        private String clientKey;

        private CreateSignRefundQueryBuilder() {
        }

        public CreateSignRefundQueryBuilder notifyUrl(String notifyUrl) {
            this.notify_url = notifyUrl;
            return this;
        }

        public CreateSignRefundQueryBuilder outPayRefundNo(String outPayRefundNo) {
            this.out_pay_refund_no = outPayRefundNo;
            return this;
        }

        public CreateSignRefundQueryBuilder payOrderId(String payOrderId) {
            this.pay_order_id = payOrderId;
            return this;
        }

        public CreateSignRefundQueryBuilder refundReason(String refundReason) {
            this.refund_reason = refundReason;
            return this;
        }

        public CreateSignRefundQueryBuilder refundTotalAmount(Long refundTotalAmount) {
            this.refund_total_amount = refundTotalAmount;
            return this;
        }

        public CreateSignRefundQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateSignRefundQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateSignRefundQuery build() {
            CreateSignRefundQuery createSignRefundQuery = new CreateSignRefundQuery();
            createSignRefundQuery.setNotify_url(notify_url);
            createSignRefundQuery.setOut_pay_refund_no(out_pay_refund_no);
            createSignRefundQuery.setPay_order_id(pay_order_id);
            createSignRefundQuery.setRefund_reason(refund_reason);
            createSignRefundQuery.setRefund_total_amount(refund_total_amount);
            createSignRefundQuery.setTenantId(tenantId);
            createSignRefundQuery.setClientKey(clientKey);
            return createSignRefundQuery;
        }
    }
}
