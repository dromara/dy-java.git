package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-06-24 16:11
 **/
public class AdSettlement {

    /**
     * 结算单名称
     */
    private String settlement_name;
    /**
     * 结算周期，格式“2006-01”
     */
    private String settlement_period;
    /**
     * 结算单编号
     */
    private String settlement_serial;
    /**
     * 结算金额，单位：分
     */
    private Long settlement_total_amount;
    /**
     * 结算状态0：未完成1：已完成
     */
    private Integer status;
    /**
     * 发票税率
     */
    private Double tax_rate;

    public String getSettlement_name() {
        return settlement_name;
    }

    public void setSettlement_name(String settlement_name) {
        this.settlement_name = settlement_name;
    }

    public String getSettlement_period() {
        return settlement_period;
    }

    public void setSettlement_period(String settlement_period) {
        this.settlement_period = settlement_period;
    }

    public String getSettlement_serial() {
        return settlement_serial;
    }

    public void setSettlement_serial(String settlement_serial) {
        this.settlement_serial = settlement_serial;
    }

    public Long getSettlement_total_amount() {
        return settlement_total_amount;
    }

    public void setSettlement_total_amount(Long settlement_total_amount) {
        this.settlement_total_amount = settlement_total_amount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getTax_rate() {
        return tax_rate;
    }

    public void setTax_rate(Double tax_rate) {
        this.tax_rate = tax_rate;
    }
}
