package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class AnalysisVideoSourceQuery extends BaseQuery {

    /**
     * 开始时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
     */
    private Long start_time;

    /**
     * 结束时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据
     * query_bind_type=0，end_time就传2023-03-20 00:00:00对应的时间戳
     * query_bind_type=1，end_time就传2023-03-20 23:59:59对应的时间戳
     */
    private Long end_time;

    /**
     * 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；不传表示查全部端数据
     */
    private String host_name;

    /**
     * 查询的抖音号列表，限制长度在200以内，即一次性最多查询 200个抖音号 所对应的自挂载,达人挂载视频/自挂载，达人挂载直播间所产生的数据
     */
    private List<String> aweme_short_id_list;

    private static AnalysisVideoSourceQueryBuilder builder() {
        return new AnalysisVideoSourceQueryBuilder();
    }

    public static class AnalysisVideoSourceQueryBuilder {
        /**
         * 开始时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
         */
        private Long startTime;

        /**
         * 结束时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据
         * query_bind_type=0，end_time就传2023-03-20 00:00:00对应的时间戳
         * query_bind_type=1，end_time就传2023-03-20 23:59:59对应的时间戳
         */
        private Long endTime;

        /**
         * 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；不传表示查全部端数据
         */
        private String hostName;

        /**
         * 查询的抖音号列表，限制长度在200以内，即一次性最多查询 200个抖音号 所对应的自挂载,达人挂载视频/自挂载，达人挂载直播间所产生的数据
         */
        private List<String> awemeShortIdList;

        private Integer tenantId;

        private String clientKey;

        public AnalysisVideoSourceQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public AnalysisVideoSourceQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public AnalysisVideoSourceQueryBuilder startTime(Long startTime) {
            this.startTime = startTime;
            return this;
        }

        public AnalysisVideoSourceQueryBuilder endTime(Long endTime) {
            this.endTime = endTime;
            return this;
        }

        public AnalysisVideoSourceQueryBuilder hostName(String hostName) {
            this.hostName = hostName;
            return this;
        }

        public AnalysisVideoSourceQueryBuilder awemeShortIdList(List<String> awemeShortIdList) {
            this.awemeShortIdList = awemeShortIdList;
            return this;
        }

        public AnalysisVideoSourceQuery build() {
            AnalysisVideoSourceQuery analysisVideoSourceQuery = new AnalysisVideoSourceQuery();
            analysisVideoSourceQuery.setClientKey(clientKey);
            analysisVideoSourceQuery.setTenantId(tenantId);
            analysisVideoSourceQuery.setStart_time(startTime);
            analysisVideoSourceQuery.setEnd_time(endTime);
            analysisVideoSourceQuery.setHost_name(hostName);
            analysisVideoSourceQuery.setAweme_short_id_list(awemeShortIdList);
            return analysisVideoSourceQuery;
        }
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public String getHost_name() {
        return host_name;
    }

    public void setHost_name(String host_name) {
        this.host_name = host_name;
    }

    public List<String> getAweme_short_id_list() {
        return aweme_short_id_list;
    }

    public void setAweme_short_id_list(List<String> aweme_short_id_list) {
        this.aweme_short_id_list = aweme_short_id_list;
    }
}
