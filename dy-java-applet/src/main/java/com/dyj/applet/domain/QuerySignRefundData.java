package com.dyj.applet.domain;

public class QuerySignRefundData {

    /**
     * <p>小程序 app_id</p>
     */
    private String app_id;
    /**
     * <p>退款创建时间，13 位毫秒时间戳</p>
     */
    private Long create_at;
    /**
     * <p>退款结果信息，可以通过该字段了解退款失败原因</p> 选填
     */
    private String message;
    /**
     * <p>退款结果回调地址，https开头</p> 选填
     */
    private String notify_url;
    /**
     * <p>开发者侧退款单的单号，长度<=64byte</p>
     */
    private String out_pay_refund_no;
    /**
     * <p>平台侧扣款单的单号，长度<=64byte</p>
     */
    private String pay_order_id;
    /**
     * <p>平台侧退款单的单号，长度<=64byte</p>
     */
    private String pay_refund_id;
    /**
     * <p>退款成功时间，13 位毫秒时间戳，仅退款成功有退款时间</p> 选填
     */
    private Long refund_at;
    /**
     * <p>退款原因，长度<=256byte</p> 选填
     */
    private String refund_reason;
    /**
     * <p>退款总金额，单位[分]</p>
     */
    private Long refund_total_amount;
    /**
     * <p>退款结果状态，状态枚举：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">PROCESSING: 退款中</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">SUCCESS：退款成功</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">FAIL：退款失败</li></ul>
     */
    private String status;

    public String getApp_id() {
        return app_id;
    }

    public QuerySignRefundData setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public Long getCreate_at() {
        return create_at;
    }

    public QuerySignRefundData setCreate_at(Long create_at) {
        this.create_at = create_at;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public QuerySignRefundData setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public QuerySignRefundData setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOut_pay_refund_no() {
        return out_pay_refund_no;
    }

    public QuerySignRefundData setOut_pay_refund_no(String out_pay_refund_no) {
        this.out_pay_refund_no = out_pay_refund_no;
        return this;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public QuerySignRefundData setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public String getPay_refund_id() {
        return pay_refund_id;
    }

    public QuerySignRefundData setPay_refund_id(String pay_refund_id) {
        this.pay_refund_id = pay_refund_id;
        return this;
    }

    public Long getRefund_at() {
        return refund_at;
    }

    public QuerySignRefundData setRefund_at(Long refund_at) {
        this.refund_at = refund_at;
        return this;
    }

    public String getRefund_reason() {
        return refund_reason;
    }

    public QuerySignRefundData setRefund_reason(String refund_reason) {
        this.refund_reason = refund_reason;
        return this;
    }

    public Long getRefund_total_amount() {
        return refund_total_amount;
    }

    public QuerySignRefundData setRefund_total_amount(Long refund_total_amount) {
        this.refund_total_amount = refund_total_amount;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public QuerySignRefundData setStatus(String status) {
        this.status = status;
        return this;
    }
}
