package com.dyj.applet.domain;

import java.util.List;

/**
 * 营销算价->商品维度用户选用的营销信息
 */
public class MarketingGoodsMarketingInfo {

    /**
     * 商品id
     */
    private String goods_id;

    /**
     * 购买数量
     */
    private Integer quantity;

    /**
     * 商品总价，单位分
     */
    private Long total_amount;

    /**
     * 用户所选营销策略信息，仅包含订单维度优惠信息 选填
     */
    private List<MarketingBundle> selected_marketing;

    public String getGoods_id() {
        return goods_id;
    }

    public MarketingGoodsMarketingInfo setGoods_id(String goods_id) {
        this.goods_id = goods_id;
        return this;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public MarketingGoodsMarketingInfo setQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public MarketingGoodsMarketingInfo setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public List<MarketingBundle> getSelected_marketing() {
        return selected_marketing;
    }

    public MarketingGoodsMarketingInfo setSelected_marketing(List<MarketingBundle> selected_marketing) {
        this.selected_marketing = selected_marketing;
        return this;
    }
}
