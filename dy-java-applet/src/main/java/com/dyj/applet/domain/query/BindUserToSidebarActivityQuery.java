package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

/**
 * 复访营销活动实时圈选用户请求值
 */
public class BindUserToSidebarActivityQuery extends UserInfoQuery {

    /**
     * 福利活动id
     */
    private String activity_id;

    public String getActivity_id() {
        return activity_id;
    }

    public BindUserToSidebarActivityQuery setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }

    public static BindUserToSidebarActivityQueryBuilder builder(){
        return new BindUserToSidebarActivityQueryBuilder();
    }


    public static final class BindUserToSidebarActivityQueryBuilder {
        private String activity_id;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private BindUserToSidebarActivityQueryBuilder() {
        }

        public static BindUserToSidebarActivityQueryBuilder aBindUserToSidebarActivityQuery() {
            return new BindUserToSidebarActivityQueryBuilder();
        }

        public BindUserToSidebarActivityQueryBuilder activityId(String activityId) {
            this.activity_id = activityId;
            return this;
        }

        public BindUserToSidebarActivityQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public BindUserToSidebarActivityQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public BindUserToSidebarActivityQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public BindUserToSidebarActivityQuery build() {
            BindUserToSidebarActivityQuery bindUserToSidebarActivityQuery = new BindUserToSidebarActivityQuery();
            bindUserToSidebarActivityQuery.setActivity_id(activity_id);
            bindUserToSidebarActivityQuery.setOpen_id(open_id);
            bindUserToSidebarActivityQuery.setTenantId(tenantId);
            bindUserToSidebarActivityQuery.setClientKey(clientKey);
            return bindUserToSidebarActivityQuery;
        }
    }
}
