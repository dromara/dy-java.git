package com.dyj.applet.domain.query;

import com.dyj.applet.domain.RefundItemOrderDetail;
import com.dyj.applet.domain.RefundOrderEntrySchema;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class TradeCreateRefundQuery extends BaseQuery {


    /**
     * 开发者侧订单号，长度 <= 64 byte
     */
    private String out_order_no;

    /**
     * 开发者侧退款单号，长度 <= 64 byte
     */
    private String out_refund_no;

    /**
     * 开发者自定义透传字段，不支持二进制，长度 <= 2048 byte
     */
    private String cp_extra;

    /**
     * <p>退款单的跳转的schema，参考<a href="/docs/resource/zh-CN/mini-app/develop/server/trade-system/general/common-param" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">通用参数-关于 xxx_entry_schema 的前置说明</a></p>
     */
    private RefundOrderEntrySchema order_entry_schema;
    /**
     * <p>退款结果通知地址，必须是 HTTPS 类型， 长度 &lt;= 512 byte 。若不填，则默认使用在解决方案配置-消息通知中指定的回调地址，配置方式参考<a href="/docs/resource/zh-CN/mini-app/open-capacity/Industry/industry_mode/guide/" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">解决方案配置文档</a></p> 选填
     */
    private String notify_url;

    /**
     * 需要发起退款的商品单信息 选填
     */
    private List<RefundItemOrderDetail> item_order_detail;

    /**
     * 退款总金额，单位分
     * •担保交易订单必传
     * •交易系统订单不能传该字段
     */
    private Long refund_total_amount;


    public String getOut_order_no() {
        return out_order_no;
    }

    public TradeCreateRefundQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getOut_refund_no() {
        return out_refund_no;
    }

    public TradeCreateRefundQuery setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
        return this;
    }

    public String getCp_extra() {
        return cp_extra;
    }

    public TradeCreateRefundQuery setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public RefundOrderEntrySchema getOrder_entry_schema() {
        return order_entry_schema;
    }

    public TradeCreateRefundQuery setOrder_entry_schema(RefundOrderEntrySchema order_entry_schema) {
        this.order_entry_schema = order_entry_schema;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public TradeCreateRefundQuery setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public List<RefundItemOrderDetail> getItem_order_detail() {
        return item_order_detail;
    }

    public TradeCreateRefundQuery setItem_order_detail(List<RefundItemOrderDetail> item_order_detail) {
        this.item_order_detail = item_order_detail;
        return this;
    }

    public Long getRefund_total_amount() {
        return refund_total_amount;
    }

    public TradeCreateRefundQuery setRefund_total_amount(Long refund_total_amount) {
        this.refund_total_amount = refund_total_amount;
        return this;
    }

    public static TradeCreateRefundQueryBuilder builder() {
        return new TradeCreateRefundQueryBuilder();
    }

    public static final class TradeCreateRefundQueryBuilder {
        private String out_order_no;
        private String out_refund_no;
        private String cp_extra;
        private RefundOrderEntrySchema order_entry_schema;
        private String notify_url;
        private List<RefundItemOrderDetail> item_order_detail;
        private Long refund_total_amount;
        private Integer tenantId;
        private String clientKey;

        private TradeCreateRefundQueryBuilder() {
        }

        public TradeCreateRefundQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public TradeCreateRefundQueryBuilder outRefundNo(String outRefundNo) {
            this.out_refund_no = outRefundNo;
            return this;
        }

        public TradeCreateRefundQueryBuilder cpExtra(String cpExtra) {
            this.cp_extra = cpExtra;
            return this;
        }

        public TradeCreateRefundQueryBuilder orderEntrySchema(RefundOrderEntrySchema orderEntrySchema) {
            this.order_entry_schema = orderEntrySchema;
            return this;
        }

        public TradeCreateRefundQueryBuilder notifyUrl(String notifyUrl) {
            this.notify_url = notifyUrl;
            return this;
        }

        public TradeCreateRefundQueryBuilder itemOrderDetail(List<RefundItemOrderDetail> itemOrderDetail) {
            this.item_order_detail = itemOrderDetail;
            return this;
        }

        public TradeCreateRefundQueryBuilder refundTotalAmount(Long refundTotalAmount) {
            this.refund_total_amount = refundTotalAmount;
            return this;
        }

        public TradeCreateRefundQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public TradeCreateRefundQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public TradeCreateRefundQuery build() {
            TradeCreateRefundQuery tradeCreateRefundQuery = new TradeCreateRefundQuery();
            tradeCreateRefundQuery.setOut_order_no(out_order_no);
            tradeCreateRefundQuery.setOut_refund_no(out_refund_no);
            tradeCreateRefundQuery.setCp_extra(cp_extra);
            tradeCreateRefundQuery.setOrder_entry_schema(order_entry_schema);
            tradeCreateRefundQuery.setNotify_url(notify_url);
            tradeCreateRefundQuery.setItem_order_detail(item_order_detail);
            tradeCreateRefundQuery.setRefund_total_amount(refund_total_amount);
            tradeCreateRefundQuery.setTenantId(tenantId);
            tradeCreateRefundQuery.setClientKey(clientKey);
            return tradeCreateRefundQuery;
        }
    }
}
