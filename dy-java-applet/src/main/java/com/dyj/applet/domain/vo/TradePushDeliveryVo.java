package com.dyj.applet.domain.vo;

public class TradePushDeliveryVo {

    /**
     * 核销记录id，签证交易下用于调用tt.confirm
     * 选填
     */
    private String delivery_id;

    public String getDelivery_id() {
        return delivery_id;
    }

    public TradePushDeliveryVo setDelivery_id(String delivery_id) {
        this.delivery_id = delivery_id;
        return this;
    }
}
