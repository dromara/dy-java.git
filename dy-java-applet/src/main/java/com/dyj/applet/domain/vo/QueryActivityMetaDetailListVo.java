package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QueryActivityMetaDetail;

import java.util.List;

/**
 * 查询授权用户发放的活动信息返回值
 */
public class QueryActivityMetaDetailListVo {

    /**
     * 返回活动信息列表总条目
     */
    private Long total;
    /**
     * 活动列表
     */
    private List<QueryActivityMetaDetail> activity_list;


    public Long getTotal() {
        return total;
    }

    public QueryActivityMetaDetailListVo setTotal(Long total) {
        this.total = total;
        return this;
    }

    public List<QueryActivityMetaDetail> getActivity_list() {
        return activity_list;
    }

    public QueryActivityMetaDetailListVo setActivity_list(List<QueryActivityMetaDetail> activity_list) {
        this.activity_list = activity_list;
        return this;
    }
}
