package com.dyj.applet.domain;

import java.util.List;

/**
 * CPS 相关信息
 */
public class QueryIndustryCpsInfo {

    /**
     * 订单总佣金，单位分
     */
    private Long total_commission_amount;
    /**
     * 订单 CPS 信息。对于交易模板 1.0 和担保支付订单，Array 长度为 1
     */
    private List<QueryIndustryCpsInfoCpsItem> cps_item_list;

    public Long getTotal_commission_amount() {
        return total_commission_amount;
    }

    public QueryIndustryCpsInfo setTotal_commission_amount(Long total_commission_amount) {
        this.total_commission_amount = total_commission_amount;
        return this;
    }

    public List<QueryIndustryCpsInfoCpsItem> getCps_item_list() {
        return cps_item_list;
    }

    public QueryIndustryCpsInfo setCps_item_list(List<QueryIndustryCpsInfoCpsItem> cps_item_list) {
        this.cps_item_list = cps_item_list;
        return this;
    }
}
