package com.dyj.applet.domain;

public class UserCertificatesAmount {

    /**
     * 券原始金额，单位分
     */
    private Integer original_amount;

    /**
     * 用户实付金额，单位分
     */
    private Integer pay_amount;

    /**
     * 商家营销金额，单位分
     * 选填
     */
    private Integer merchant_ticket_amount;

    /**
     * 支付优惠金额，单位分
     * 选填
     */
    private Integer payment_discount_amount;

    /**
     * 券实付金额（=用户实付金额+支付优惠金额），单位分
     * 选填
     */
    private Integer coupon_pay_amount;

    /**
     * 平台补贴金额，单位分
     * 选填
     */
    private Integer platform_ticket_amount;

    public Integer getOriginal_amount() {
        return original_amount;
    }

    public UserCertificatesAmount setOriginal_amount(Integer original_amount) {
        this.original_amount = original_amount;
        return this;
    }

    public Integer getPay_amount() {
        return pay_amount;
    }

    public UserCertificatesAmount setPay_amount(Integer pay_amount) {
        this.pay_amount = pay_amount;
        return this;
    }

    public Integer getMerchant_ticket_amount() {
        return merchant_ticket_amount;
    }

    public UserCertificatesAmount setMerchant_ticket_amount(Integer merchant_ticket_amount) {
        this.merchant_ticket_amount = merchant_ticket_amount;
        return this;
    }

    public Integer getPayment_discount_amount() {
        return payment_discount_amount;
    }

    public UserCertificatesAmount setPayment_discount_amount(Integer payment_discount_amount) {
        this.payment_discount_amount = payment_discount_amount;
        return this;
    }

    public Integer getCoupon_pay_amount() {
        return coupon_pay_amount;
    }

    public UserCertificatesAmount setCoupon_pay_amount(Integer coupon_pay_amount) {
        this.coupon_pay_amount = coupon_pay_amount;
        return this;
    }

    public Integer getPlatform_ticket_amount() {
        return platform_ticket_amount;
    }

    public UserCertificatesAmount setPlatform_ticket_amount(Integer platform_ticket_amount) {
        this.platform_ticket_amount = platform_ticket_amount;
        return this;
    }
}
