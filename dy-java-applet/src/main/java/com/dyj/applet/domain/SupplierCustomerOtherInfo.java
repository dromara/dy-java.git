package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 13:44
 **/
public class SupplierCustomerOtherInfo {

    /**
     * 其他补充材料外部id
     */
    private String ext_id;

    /**
     * 其他补充材料url
     */
    private String url;

    public static SupplierCustomerOtherInfoBuilder builder() {
        return new SupplierCustomerOtherInfoBuilder();
    }

    public static class SupplierCustomerOtherInfoBuilder {
        private String extId;

        private String url;

        public SupplierCustomerOtherInfoBuilder extId(String extId) {
            this.extId = extId;
            return this;
        }

        public SupplierCustomerOtherInfoBuilder url(String url) {
            this.url = url;
            return this;
        }

        public SupplierCustomerOtherInfo build() {
            SupplierCustomerOtherInfo supplierCustomerOtherInfo = new SupplierCustomerOtherInfo();
            supplierCustomerOtherInfo.setExt_id(extId);
            supplierCustomerOtherInfo.setUrl(url);
            return supplierCustomerOtherInfo;
        }
    }

    public String getExt_id() {
        return ext_id;
    }

    public void setExt_id(String ext_id) {
        this.ext_id = ext_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
