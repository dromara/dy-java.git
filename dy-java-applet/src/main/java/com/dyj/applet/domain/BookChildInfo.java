package com.dyj.applet.domain;

import java.util.List;

/**
 * 每个item的预约信息，详见ItemBookInfo
 */
public class BookChildInfo {

    /**
     * 商品id
     */
    private String goods_id;

    /**
     * 商品sku_id，len <= 64 byte
     * 选填
     */
    private String sku_id;


    /**
     * 预售单的item_order_id，如果不指定的话，会自动分配。
     */
    private String item_order_id;

    /**
     * 预约门店的poiId，实际存储的是int64类型的值
     */
    private String poi_id;

    /**
     * 预约的开始时间（ms），13位毫秒时间戳
     */
    private Long book_start_time;

    /**
     *
     * 预约的结束时间（ms），13位毫秒时间戳
     * 注意：需满足 当前时间< book_start_time < book_end_time，并且book_end_time必须是180天之内
     */
    private Long book_end_time;


    /**
     * 用户信息，详见UserInfo
     * 选填
     */
    private List<BookUserInfo> user_info_list;

    /**
     * 该预约单关联的加价单item_order_id，，len <= 64 byte
     * 选填
     */
    private String markup_item_order_id;

    public String getGoods_id() {
        return goods_id;
    }

    public BookChildInfo setGoods_id(String goods_id) {
        this.goods_id = goods_id;
        return this;
    }

    public String getSku_id() {
        return sku_id;
    }

    public BookChildInfo setSku_id(String sku_id) {
        this.sku_id = sku_id;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public BookChildInfo setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public BookChildInfo setPoi_id(String poi_id) {
        this.poi_id = poi_id;
        return this;
    }

    public Long getBook_start_time() {
        return book_start_time;
    }

    public BookChildInfo setBook_start_time(Long book_start_time) {
        this.book_start_time = book_start_time;
        return this;
    }

    public Long getBook_end_time() {
        return book_end_time;
    }

    public BookChildInfo setBook_end_time(Long book_end_time) {
        this.book_end_time = book_end_time;
        return this;
    }

    public List<BookUserInfo> getUser_info_list() {
        return user_info_list;
    }

    public BookChildInfo setUser_info_list(List<BookUserInfo> user_info_list) {
        this.user_info_list = user_info_list;
        return this;
    }

    public String getMarkup_item_order_id() {
        return markup_item_order_id;
    }

    public BookChildInfo setMarkup_item_order_id(String markup_item_order_id) {
        this.markup_item_order_id = markup_item_order_id;
        return this;
    }
}
