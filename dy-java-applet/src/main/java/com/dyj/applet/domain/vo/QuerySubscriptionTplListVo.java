package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QuerySubscriptionTemplate;

import java.util.List;

public class QuerySubscriptionTplListVo {
    /**
     * 记录总数
     */
    private Long total_count;
    /**
     * 订阅消息模板列表
     */
    private List<QuerySubscriptionTemplate> template_list;

    public Long getTotal_count() {
        return total_count;
    }

    public QuerySubscriptionTplListVo setTotal_count(Long total_count) {
        this.total_count = total_count;
        return this;
    }

    public List<QuerySubscriptionTemplate> getTemplate_list() {
        return template_list;
    }

    public QuerySubscriptionTplListVo setTemplate_list(List<QuerySubscriptionTemplate> template_list) {
        this.template_list = template_list;
        return this;
    }
}
