package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisProductDealData;

import java.util.List;

public class AnalysisProductDealDataVo {
    /**
     * 记录总数
     */
    private Long total;
    /**
     * 商品交易数据
     */
    private List<AnalysisProductDealData> deal_data_list;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<AnalysisProductDealData> getDeal_data_list() {
        return deal_data_list;
    }

    public void setDeal_data_list(List<AnalysisProductDealData> deal_data_list) {
        this.deal_data_list = deal_data_list;
    }
}
