package com.dyj.applet.domain.vo;

import java.util.Map;

public class QueryFunctionConfigStatusVo {

    /**
     * <p>功能配置状态</p> 选填
     */
    private Map<String,Integer> function_config_status;

    public Map<String, Integer> getFunction_config_status() {
        return function_config_status;
    }

    public QueryFunctionConfigStatusVo setFunction_config_status(Map<String, Integer> function_config_status) {
        this.function_config_status = function_config_status;
        return this;
    }
}
