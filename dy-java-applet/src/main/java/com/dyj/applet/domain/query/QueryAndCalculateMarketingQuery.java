package com.dyj.applet.domain.query;

import com.dyj.applet.domain.MarketingGoodsMarketingInfo;
import com.dyj.applet.domain.MarketingOrderMarketingInfo;
import com.dyj.common.domain.query.UserInfoQuery;

import java.util.List;

public class QueryAndCalculateMarketingQuery extends UserInfoQuery {

    /**
     * 商品维度用户选用的营销信息
     */
    private List<MarketingGoodsMarketingInfo> goods_marketing_info;

    /**
     * 订单维度用户选用的营销信息 选填
     */
    private MarketingOrderMarketingInfo order_marketing_info;


    /**
     * 透传参数 选填
     */
    private String callback_data;

    /**
     * 是否需要默认算价（首次获取营销信息时为true）选填
     */
    private Boolean need_default_marketing;

    public List<MarketingGoodsMarketingInfo> getGoods_marketing_info() {
        return goods_marketing_info;
    }

    public QueryAndCalculateMarketingQuery setGoods_marketing_info(List<MarketingGoodsMarketingInfo> goods_marketing_info) {
        this.goods_marketing_info = goods_marketing_info;
        return this;
    }

    public MarketingOrderMarketingInfo getOrder_marketing_info() {
        return order_marketing_info;
    }

    public QueryAndCalculateMarketingQuery setOrder_marketing_info(MarketingOrderMarketingInfo order_marketing_info) {
        this.order_marketing_info = order_marketing_info;
        return this;
    }

    public String getCallback_data() {
        return callback_data;
    }

    public QueryAndCalculateMarketingQuery setCallback_data(String callback_data) {
        this.callback_data = callback_data;
        return this;
    }

    public Boolean getNeed_default_marketing() {
        return need_default_marketing;
    }

    public QueryAndCalculateMarketingQuery setNeed_default_marketing(Boolean need_default_marketing) {
        this.need_default_marketing = need_default_marketing;
        return this;
    }

    public static QueryAndCalculateMarketingQueryBuilder builder() {
        return new QueryAndCalculateMarketingQueryBuilder();
    }

    public static final class QueryAndCalculateMarketingQueryBuilder {
        private List<MarketingGoodsMarketingInfo> goods_marketing_info;
        private MarketingOrderMarketingInfo order_marketing_info;
        private String callback_data;
        private Boolean need_default_marketing;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private QueryAndCalculateMarketingQueryBuilder() {
        }

        public QueryAndCalculateMarketingQueryBuilder goodsMarketingInfo(List<MarketingGoodsMarketingInfo> goodsMarketingInfo) {
            this.goods_marketing_info = goodsMarketingInfo;
            return this;
        }

        public QueryAndCalculateMarketingQueryBuilder orderMarketingInfo(MarketingOrderMarketingInfo orderMarketingInfo) {
            this.order_marketing_info = orderMarketingInfo;
            return this;
        }

        public QueryAndCalculateMarketingQueryBuilder callbackData(String callbackData) {
            this.callback_data = callbackData;
            return this;
        }

        public QueryAndCalculateMarketingQueryBuilder needDefaultMarketing(Boolean needDefaultMarketing) {
            this.need_default_marketing = needDefaultMarketing;
            return this;
        }

        public QueryAndCalculateMarketingQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public QueryAndCalculateMarketingQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryAndCalculateMarketingQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryAndCalculateMarketingQuery build() {
            QueryAndCalculateMarketingQuery queryAndCalculateMarketingQuery = new QueryAndCalculateMarketingQuery();
            queryAndCalculateMarketingQuery.setGoods_marketing_info(goods_marketing_info);
            queryAndCalculateMarketingQuery.setOrder_marketing_info(order_marketing_info);
            queryAndCalculateMarketingQuery.setCallback_data(callback_data);
            queryAndCalculateMarketingQuery.setNeed_default_marketing(need_default_marketing);
            queryAndCalculateMarketingQuery.setOpen_id(open_id);
            queryAndCalculateMarketingQuery.setTenantId(tenantId);
            queryAndCalculateMarketingQuery.setClientKey(clientKey);
            return queryAndCalculateMarketingQuery;
        }
    }
}
