package com.dyj.applet.domain.query;

import com.dyj.applet.domain.ApplyAliasModifyTag;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class ApplyAliasSetSearchTagQuery extends BaseQuery {

    /**
     * 新增的搜索标签
     */
    private List<String> add_tag_list;

    /**
     * 删除的搜索标签
     */
    private List<String> delete_tag_list;

    /**
     * 修改的搜索标签
     */
    private List<ApplyAliasModifyTag> modify_tag_list;


    public static ApplyAliasSetSearchTagQueryBuilder builder() {
        return new ApplyAliasSetSearchTagQueryBuilder();
    }

    public static final class ApplyAliasSetSearchTagQueryBuilder {
        /**
         * 新增的搜索标签
         */
        private List<String> addTagList;

        /**
         * 删除的搜索标签
         */
        private List<String> deleteTagList;

        /**
         * 修改的搜索标签
         */
        private List<ApplyAliasModifyTag> modifyTagList;

        public ApplyAliasSetSearchTagQueryBuilder addTagList(List<String> addTagList) {
            this.addTagList = addTagList;
            return this;
        }

        public ApplyAliasSetSearchTagQueryBuilder deleteTagList(List<String> deleteTagList) {
            this.deleteTagList = deleteTagList;
            return this;
        }

        public ApplyAliasSetSearchTagQueryBuilder modifyTagList(List<ApplyAliasModifyTag> modifyTagList) {
            this.modifyTagList = modifyTagList;
            return this;
        }

        public ApplyAliasSetSearchTagQuery build() {
            ApplyAliasSetSearchTagQuery applyAliasSetSearchTagQuery = new ApplyAliasSetSearchTagQuery();
            applyAliasSetSearchTagQuery.setAdd_tag_list(addTagList);
            applyAliasSetSearchTagQuery.setDelete_tag_list(deleteTagList);
            applyAliasSetSearchTagQuery.setModify_tag_list(modifyTagList);
            return applyAliasSetSearchTagQuery;
        }
    }

    public List<String> getAdd_tag_list() {
        return add_tag_list;
    }

    public void setAdd_tag_list(List<String> add_tag_list) {
        this.add_tag_list = add_tag_list;
    }

    public List<String> getDelete_tag_list() {
        return delete_tag_list;
    }

    public void setDelete_tag_list(List<String> delete_tag_list) {
        this.delete_tag_list = delete_tag_list;
    }

    public List<ApplyAliasModifyTag> getModify_tag_list() {
        return modify_tag_list;
    }

    public void setModify_tag_list(List<ApplyAliasModifyTag> modify_tag_list) {
        this.modify_tag_list = modify_tag_list;
    }
}
