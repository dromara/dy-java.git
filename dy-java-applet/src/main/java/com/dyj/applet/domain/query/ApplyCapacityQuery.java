package com.dyj.applet.domain.query;

import com.dyj.applet.domain.CapacityApplyInfo;
import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-07-12 11:47
 **/
public class ApplyCapacityQuery extends BaseQuery {

    private String capacityKey;

    private String apply_reason;

    private CapacityApplyInfo apply_info;

    public static ApplyCapacityQueryBuilder builder()
    {
        return new ApplyCapacityQueryBuilder();
    }

    public static final class ApplyCapacityQueryBuilder {

        /**
         * 固定值“ma.component.page_layout_custom”
         */
        private String capacityKey;

        /**
         * 申请能力的原因
         */
        private String applyReason;

        private CapacityApplyInfo applyInfo;

        private Integer tenantId;

        private String clientKey;

        public ApplyCapacityQueryBuilder capacityKey(String capacityKey) {
            this.capacityKey = capacityKey;
            return this;
        }

        public ApplyCapacityQueryBuilder applyReason(String applyReason) {
            this.applyReason = applyReason;
            return this;
        }

        public ApplyCapacityQueryBuilder applyInfo(CapacityApplyInfo applyInfo) {
            this.applyInfo = applyInfo;
            return this;
        }

        public ApplyCapacityQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public ApplyCapacityQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public ApplyCapacityQuery build() {
            ApplyCapacityQuery applyCapacityQuery = new ApplyCapacityQuery();
            applyCapacityQuery.setCapacityKey(capacityKey);
            applyCapacityQuery.setApply_reason(applyReason);
            applyCapacityQuery.setApply_info(applyInfo);
            applyCapacityQuery.setTenantId(tenantId);
            applyCapacityQuery.setClientKey(clientKey);
            return applyCapacityQuery;
        }
    }

    public String getCapacityKey() {
        return capacityKey;
    }

    public void setCapacityKey(String capacityKey) {
        this.capacityKey = capacityKey;
    }

    public String getApply_reason() {
        return apply_reason;
    }

    public void setApply_reason(String apply_reason) {
        this.apply_reason = apply_reason;
    }

    public CapacityApplyInfo getApply_info() {
        return apply_info;
    }

    public void setApply_info(CapacityApplyInfo apply_info) {
        this.apply_info = apply_info;
    }
}
