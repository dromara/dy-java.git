package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisComponentSourceData;

import java.util.List;

public class AnalysisComponentWithSourceVo {

    private List<AnalysisComponentSourceData> ScenesDataList;

    public List<AnalysisComponentSourceData> getScenesDataList() {
        return ScenesDataList;
    }

    public void setScenesDataList(List<AnalysisComponentSourceData> scenesDataList) {
        ScenesDataList = scenesDataList;
    }
}
