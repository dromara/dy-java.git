package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QueryRefundResult;

import java.util.List;

/**
 * 查询退款返回值
 */
public class QueryRefundResultVo {

    /**
     * 退款信息列表
     */
    private List<QueryRefundResult> refund_list;

    public List<QueryRefundResult> getRefund_list() {
        return refund_list;
    }

    public QueryRefundResultVo setRefund_list(List<QueryRefundResult> refund_list) {
        this.refund_list = refund_list;
        return this;
    }
}
