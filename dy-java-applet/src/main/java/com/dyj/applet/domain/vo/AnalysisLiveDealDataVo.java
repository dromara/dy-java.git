package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisLiveDealData;

import java.util.List;

public class AnalysisLiveDealDataVo {

    /**
     * 直播交易数据列表详情
     */
    private List<AnalysisLiveDealData> deal_data_list;

    /**
     * 直播交易数据总览
     */
    private AnalysisLiveDealData deal_overview_data;

    public List<AnalysisLiveDealData> getDeal_data_list() {
        return deal_data_list;
    }

    public void setDeal_data_list(List<AnalysisLiveDealData> deal_data_list) {
        this.deal_data_list = deal_data_list;
    }

    public AnalysisLiveDealData getDeal_overview_data() {
        return deal_overview_data;
    }

    public void setDeal_overview_data(AnalysisLiveDealData deal_overview_data) {
        this.deal_overview_data = deal_overview_data;
    }
}
