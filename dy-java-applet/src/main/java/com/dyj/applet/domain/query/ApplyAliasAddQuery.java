package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class ApplyAliasAddQuery extends BaseQuery {

    /**
     * 小程序别名
     */
    private String alias_word;


    public String getAlias_word() {
        return alias_word;
    }

    public void setAlias_word(String alias_word) {
        this.alias_word = alias_word;
    }

    public static ApplyAliasQueryBuilder builder() {
        return new ApplyAliasQueryBuilder();
    }

    public static final class ApplyAliasQueryBuilder {
        /** 小程序别名 **/
        private String aliasWord;

        private Integer tenantId;

        private String clientKey;

        public ApplyAliasQueryBuilder aliasWord(String aliasWord) {
            this.aliasWord = aliasWord;
            return this;
        }

        public ApplyAliasQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public ApplyAliasQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public ApplyAliasAddQuery build() {
            ApplyAliasAddQuery applyAliasQuery = new ApplyAliasAddQuery();
            applyAliasQuery.setAlias_word(aliasWord);
            applyAliasQuery.setTenantId(tenantId);
            applyAliasQuery.setClientKey(clientKey);
            return applyAliasQuery;
        }
    }
}
