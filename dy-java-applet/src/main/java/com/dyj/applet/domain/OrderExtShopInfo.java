package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 14:27
 **/
public class OrderExtShopInfo {

    /**
     * 外部商户ID
     */
    private String ext_shop_id;
    /**
     * 抖音商户ID
     */
    private String shop_id;

    public static OrderExtShopInfoBuilder builder() {
        return new OrderExtShopInfoBuilder();
    }

    public static class OrderExtShopInfoBuilder {
        private String extShopId;
        private String shopId;

        public OrderExtShopInfoBuilder extShopId(String extShopId) {
            this.extShopId = extShopId;
            return this;
        }

        public OrderExtShopInfoBuilder shopId(String shopId) {
            this.shopId = shopId;
            return this;
        }

        public OrderExtShopInfo build() {
            OrderExtShopInfo orderExtShopInfo = new OrderExtShopInfo();
            orderExtShopInfo.setExt_shop_id(extShopId);
            orderExtShopInfo.setShop_id(shopId);
            return orderExtShopInfo;
        }
    }

    public String getExt_shop_id() {
        return ext_shop_id;
    }

    public void setExt_shop_id(String ext_shop_id) {
        this.ext_shop_id = ext_shop_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }
}
