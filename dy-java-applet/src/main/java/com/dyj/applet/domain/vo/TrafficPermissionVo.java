package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-06-17 15:16
 **/
public class TrafficPermissionVo {

    /**
     * 是否可开通
     * 0：不可开通
     * 1：可开通
     */
    private Integer can_open;

    /**
     * 开通状态
     * 0：未开通
     * 1：已开通
     */
    private Integer status;


    public Integer getCan_open() {
        return can_open;
    }

    public void setCan_open(Integer can_open) {
        this.can_open = can_open;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
