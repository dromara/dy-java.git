package com.dyj.applet.domain;

import java.util.List;

/**
 * 标签组信息
 */
public class TagQueryTagDetail {


    /**
     * <p>标签组id</p>
     */
    private String tag_group_id;
    /**
     * <p>标签组对应规则信息列表</p>
     */
    private List<TagQueryTagDetailRule> rule_list;
    /**
     * <p>标签组对应标签信息列表</p>
     */
    private List<TagQueryTagDetailTag> tag_list;

    public String getTag_group_id() {
        return tag_group_id;
    }

    public TagQueryTagDetail setTag_group_id(String tag_group_id) {
        this.tag_group_id = tag_group_id;
        return this;
    }

    public List<TagQueryTagDetailRule> getRule_list() {
        return rule_list;
    }

    public TagQueryTagDetail setRule_list(List<TagQueryTagDetailRule> rule_list) {
        this.rule_list = rule_list;
        return this;
    }

    public List<TagQueryTagDetailTag> getTag_list() {
        return tag_list;
    }

    public TagQueryTagDetail setTag_list(List<TagQueryTagDetailTag> tag_list) {
        this.tag_list = tag_list;
        return this;
    }
}
