package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-04-28 14:58
 **/
public class SupplierSyncVo extends BaseVo {

    /**
     * 抖音平台商户ID
     */
    private String supplier_id;

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }
}
