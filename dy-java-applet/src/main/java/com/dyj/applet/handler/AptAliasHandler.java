package com.dyj.applet.handler;

import com.dyj.applet.domain.query.ApplyAliasAddQuery;
import com.dyj.applet.domain.query.ApplyAliasSetSearchTagQuery;
import com.dyj.applet.domain.query.ApplyAliasUpdateQuery;
import com.dyj.applet.domain.vo.ApplyAliasListVo;
import com.dyj.applet.domain.vo.ApplyAliasSetSearchTagVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

/**
 * 小程序别名处理类
 */
public class AptAliasHandler extends AbstractAppletHandler {
    public AptAliasHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 添加小程序别名
     *
     * @param query 入参
     */
    public DySimpleResult<String> createAlias(ApplyAliasAddQuery query) {
        baseQuery(query);
        return getAliasClient().createAlias(query);
    }

    /**
     * 查询小程序别名
     */
    public DySimpleResult<ApplyAliasListVo> queryAlias() {
        return getAliasClient().queryAlias(baseQuery());
    }

    /**
     * 修改小程序别名
     *
     * @param query 入参
     */
    public DySimpleResult<String> updateAlias(ApplyAliasUpdateQuery query) {
        baseQuery(query);
        return getAliasClient().updateAlias(query);
    }

    /**
     * 删除小程序别名
     *
     * @param alias:别名
     */
    public DySimpleResult<String> deleteAlias(String alias) {
        return getAliasClient().deleteAlias(baseQuery(), alias);
    }

    /**
     * 设置小程序搜索标签
     */
    public DySimpleResult<String> setSearchTag(ApplyAliasSetSearchTagQuery query) {
        baseQuery(query);
        return getAliasClient().setSearchTag(query);
    }

    /**
     * 查询小程序搜索标签列表
     */
    public DySimpleResult<ApplyAliasSetSearchTagVo> getSearchTagList() {
        return getAliasClient().getSearchTagList(baseQuery());
    }
}
