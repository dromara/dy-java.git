package com.dyj.applet.handler;

import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DyResult;

/**
 * @author danmo
 * @date 2024-05-24 14:31
 **/
public class AptUserDataHandler extends AbstractAppletHandler {
    public AptUserDataHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 获取用户视频情况
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserItemVo>
     */
    public DyResult<AptUserItemVo> getUserItem(String openId, Long dataType) {
        return getUserDataClient().getUserItem(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户视频情况(经营授权)
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserItemVo>
     */
    public DyResult<AptUserItemVo> getUserBizItem(String openId, Long dataType) {
        return getUserDataClient().getUserBizItem(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户粉丝情况
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserFansVo>
     */
    public DyResult<AptUserFansVo> getUserFans(String openId, Long dataType) {
        return getUserDataClient().getUserFans(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户粉丝情况(经营授权)
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserFansVo>
     */
    public DyResult<AptUserFansVo> getUserBizFans(String openId, Long dataType) {
        return getUserDataClient().getUserBizFans(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户点赞情况
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserLikeVo>
     */
    public DyResult<AptUserLikeVo> getUserLike(String openId, Long dataType) {
        return getUserDataClient().getUserLike(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户点赞情况(经营授权)
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserLikeVo>
     */
    public DyResult<AptUserLikeVo> getUserBizLike(String openId, Long dataType) {
        return getUserDataClient().getUserBizLike(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户评论情况
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserCommentVo>
     */

    public DyResult<AptUserCommentVo> getUserComment(String openId, Long dataType) {
        return getUserDataClient().getUserComment(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户评论情况(经营授权)
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserCommentVo>
     */
    public DyResult<AptUserCommentVo> getUserBizComment(String openId, Long dataType) {
        return getUserDataClient().getUserBizComment(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户分享情况
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserShareVo>
     */
    public DyResult<AptUserShareVo> getUserShare(String openId, Long dataType) {
        return getUserDataClient().getUserShare(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户分享情况(经营授权)
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserShareVo>
     */
    public DyResult<AptUserShareVo> getUserBizShare(String openId, Long dataType) {
        return getUserDataClient().getUserBizShare(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户主页访问数
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserProfileVo>
     */
    public DyResult<AptUserProfileVo> getUserProfile(String openId, Long dataType) {
        return getUserDataClient().getUserProfile(userInfoQuery(openId), dataType, openId);
    }

    /**
     * 获取用户主页访问数(经营授权)
     *
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserProfileVo>
     */
    public DyResult<AptUserProfileVo> getUserBizProfile(String openId, Long dataType) {
        return getUserDataClient().getUserBizProfile(userInfoQuery(openId), dataType, openId);
    }

}
