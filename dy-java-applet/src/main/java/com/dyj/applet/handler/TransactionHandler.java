package com.dyj.applet.handler;

import com.dyj.applet.domain.*;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DataAndExtraVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.domain.vo.BaseVo;

import java.io.InputStream;
import java.util.List;

/**
 * 交易系统 通用交易系统
 */
public class TransactionHandler extends AbstractAppletHandler{
    public TransactionHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 进件图片上传
     * @param imageType 图片格式，支持格式：jpg、jpeg、png
     * @param imageContent 图片二进制字节流，最大为2M
     * @return
     */
    public DySimpleResult<ImageUploadImageIdVo> merchantImageUpload(String imageType, InputStream imageContent){
        BaseTransactionMerchantQuery baseQuery = new BaseTransactionMerchantQuery();
        baseQuery.setClientKey(agentConfiguration.getClientKey());
        baseQuery.setTenantId(agentConfiguration.getTenantId());
        return getTransactionClient().merchantImageUpload(baseQuery, imageType, imageContent);
    }

    /**
     * 发起进件
     * @param body 发起进件请求值
     * @return
     */
    public DySimpleResult<CreateSubMerchantDataVo> createSubMerchantV3(CreateSubMerchantMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().createSubMerchantV3(body);
    }

    /**
     * 进件查询
     * @param body 进件查询请求值
     * @return
     */
    public DySimpleResult<QueryMerchantStatus> queryMerchantStatusV3(QueryMerchantStatusMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().queryMerchantStatusV3(body);
    }

    /**
     * 开发者获取小程序收款商户/合作方进件页面
     * @param body 开发者获取小程序收款商户/合作方进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openAppAddSubMerchant(GetMerchantUrlMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().openAppAddSubMerchant(body);
    }

    /**
     * 服务商获取小程序收款商户进件页面
     * @param body 服务商获取小程序收款商户进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openAddAppMerchant(GetMerchantUrlMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().openAddAppMerchant(body);
    }

    /**
     * 服务商获取服务商进件页面
     * @param body 服务商获取服务商进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openSaasAddMerchant(OpenSaasAddMerchantMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().openSaasAddMerchant(body);
    }

    /**
     * 服务商获取合作方进件页面
     * @param body 服务商获取合作方进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openSaasAddSubMerchant(OpenSaasAddSubMerchantMerchantQuery body) {
        baseQuery(body);
        return getTransactionClient().openSaasAddSubMerchant(body);
    }

    /**
     * 查询标签组信息
     * @param body 查询标签组信息请求值
     * @return
     */
    public DySimpleResult<TagQueryVo> tagQuery(TagQueryQuery body){
        baseQuery(body);
        return getTransactionClient().tagQuery(body);
    }

    /**
     * 查询CPS信息
     * @param body 查询CPS信息请求值
     * @return
     */
    public DySimpleResult<QueryTransactionCps> queryCps(QueryTransactionCpsQuery body){
        baseQuery(body);
        return getTransactionClient().queryCps(body);
    }

    /**
     * 查询订单信息
     * @param body 查询订单信息请求值
     * @return
     */
    public DySimpleResult<QueryTransactionOrder> queryOrder(QueryTransactionOrderQuery body) {
        baseQuery(body);
        return getTransactionClient().queryOrder(body);
    }

    /**
     * 发起退款
     * @param body 发起退款请求值
     * @return
     */
    public DySimpleResult<CreateRefundVo> createRefund(CreateRefundQuery body) {
        baseQuery(body);
        return getTransactionClient().createRefund(body);
    }

    /**
     * 查询退款
     * @param body 查询退款请求值
     * @return
     */
    public DySimpleResult<QueryRefundResultVo> queryRefund(QueryRefundQuery body) {
        baseQuery(body);
        return getTransactionClient().queryRefund(body);
    }

    /**
     * 同步退款审核结果
     * @param body 同步退款审核结果请求值
     * @return
     */
    public DySimpleResult<?> merchantAuditCallback(MerchantAuditCallbackQuery body) {
        baseQuery(body);
        return getTransactionClient().merchantAuditCallback(body);
    }

    /**
     * 推送履约状态
     * @param body 推送履约状态请求值
     * @return
     */
    public DySimpleResult<?> fulfillPushStatus(FulfillPushStatusQuery body){
        baseQuery(body);
        return getTransactionClient().fulfillPushStatus(body);
    }

    /**
     * 发起分账
     * @param body 发起分账请求值
     * @return
     */
    public DySimpleResult<CreateSettleVo> createSettle(CreateSettleQuery body) {
        baseQuery(body);
        return getTransactionClient().createSettle(body);
    }

    /**
     * 查询分账
     * @param body 查询分账请求值
     * @return
     */
    public DySimpleResult<List<QuerySettleResult>> querySettle(QuerySettleQuery body) {
        baseQuery(body);
        return getTransactionClient().querySettle(body);
    }

    /**
     * 商户余额查询
     * @param body 商户余额查询请求值
     * @return
     */
    public DySimpleResult<QueryChannelBalanceAccountVo> queryChannelBalanceAccount(QueryChannelBalanceAccountQuery body){
        baseQuery(body);
        return getTransactionClient().queryChannelBalanceAccount(body);
    }

    /**
     * 商户提现
     * @param body 商户提现请求值
     * @return
     */
    public DySimpleResult<MerchantWithdrawVo> merchantWithdraw(MerchantWithdrawQuery body){
        baseQuery(body);
        return getTransactionClient().merchantWithdraw(body);
    }

    /**
     * 商户提现结果查询
     * @param body 商户提现结果查询请求值
     * @return
     */
    public DySimpleResult<QueryWithdrawOrderVo> queryWithdrawOrder(QueryWithdrawOrderQuery body) {
        baseQuery(body);
        return getTransactionClient().queryWithdrawOrder(body);
    }

    /**
     * 开发者获取小程序收款商户/合作方提现页面
     * @param body
     * @return
     */
    public DySimpleResult<MerchantUrl> saasAppAddSubMerchant(SaasAppAddSubMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().saasAppAddSubMerchant(body);
    }

    /**
     * 服务商获取小程序收款商户提现页面
     * @param body
     * @return
     */
    public DySimpleResult<MerchantUrl> saasGetAppMerchant(OpenAddAppMerchantMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().saasGetAppMerchant(body);
    }

    /**
     * 服务商获取服务商提现页面
     * @param body
     * @return
     */
    public DySimpleResult<MerchantUrl> saasAddMerchant(SaasAddMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().saasAddMerchant(body);
    }

    /**
     * 服务商获取合作方提现页面
     * @param body
     * @return
     */
    public DySimpleResult<MerchantUrl> saasAddSubMerchant(OpenSaasAddSubMerchantMerchantQuery body) {
        baseQuery(body);
        return getTransactionClient().saasAddSubMerchant(body);
    }

    /**
     * 获取资金账单
     * @param body 获取资金账单请求值
     * @return
     */
    public DySimpleResult<GetFundBillVo> getFundBill(GetFundBillQuery body) {
        baseQuery(body);
        return getTransactionClient().getFundBill(body);
    }

    /**
     * 获取交易账单
     * @param body 获取交易账单请求值
     * @return
     */
    public DySimpleResult<GetBillVo> getBill(GetBillQuery body) {
        baseQuery(body);
        return getTransactionClient().getBill(body);
    }

    /**
     * 查询订单基本信息。
     * @param body 查询订单基本信息。请求值
     * @return
     */
    public DataAndExtraVo<QueryIndustryOrderVo> queryIndustryOrder(QueryIndustryOrderQuery body){
        baseQuery(body);
        return getTransactionClient().queryIndustryOrder(body);
    }

    /**
     * 查询券状态信息
     * @param body 查询券状态信息请求值
     * @return
     */
    public DataAndExtraVo<QueryIndustryItemOrderInfoVo> queryIndustryItemOrderInfo(QueryIndustryItemOrderInfoQuery body){
        baseQuery(body);
        return getTransactionClient().queryIndustryItemOrderInfo(body);
    }

    /**
     * 生活服务交易系统->查询 CPS 信息
     * @param body 查询 CPS 信息请求值
     * @return
     */
    public DataAndExtraVo<QueryIndustryCpsVo> queryIndustryOrderCps(QueryIndustryOrderCpsQuery body){
        baseQuery(body);
        return getTransactionClient().queryIndustryOrderCps(body);
    }


    /**
     * 生活服务交易系统->预下单->开发者发起下单
     * @param body
     * @return
     */
    public DySimpleResult<PreCreateIndustryOrderVo> preCreateIndustryOrder(PreCreateIndustryOrderQuery body){
        baseQuery(body);
        return getTransactionClient().preCreateIndustryOrder(body);
    }

    /**
     * 生活服务交易系统->营销算价->查询营销算价
     * @param body
     */
    public DySimpleResult<QueryAndCalculateMarketingResult> queryAndCalculateMarketing(QueryAndCalculateMarketingQuery body){
        baseQuery(body);
        return getTransactionClient().queryAndCalculateMarketing(body);
    }

    /**
     * 生活服务交易系统->核销->抖音码->验券准备
     * @param body 验券准备请求值
     * @return
     */
    public DataAndExtraVo<DeliveryPrepareVo> deliveryPrepare(DeliveryPrepareQuery body) {
        baseQuery(body);
        return getTransactionClient().deliveryPrepare(body);
    }

    /**
     * 生活服务交易系统->核销->抖音码->验券
     * @param body 验券请求值
     * @return
     */
    public DataAndExtraVo<DeliveryVerifyVo> deliveryVerify(DeliveryVerifyQuery body){
        baseQuery(body);
        return getTransactionClient().deliveryVerify(body);
    }

    /**
     * 生活服务交易系统->核销->抖音码->撤销核销
     * 生活服务交易系统->核销->三方码->撤销核销
     * @param body 撤销核销请求值
     * @return
     */
    public DataAndExtraVo<BaseVo> verifyCancel(VerifyCancelQuery body){
        baseQuery(body);
        return getTransactionClient().verifyCancel(body);
    }

    /**
     * 生活服务交易系统->核销->三方码->推送核销状态
     * @param body 推送核销状态请求值
     * @return
     */
    public DataAndExtraVo<PushDeliveryVo> pushDelivery(PushDeliveryQuery body){
        baseQuery(body);
        return getTransactionClient().pushDelivery(body);
    }

    /**
     * 生活服务交易系统->分账->查询分账
     * @param body 查询分账请求值
     * @return
     */
    public DataAndExtraVo<List<IndustryQuerySettleVo>> industryQuerySettle(IndustryQuerySettleQuery body){
        baseQuery(body);
        return getTransactionClient().industryQuerySettle(body);
    }

    /**
     * 生活服务交易系统->退货退款->开发者发起退款
     * @param body
     * @return
     */
    public DataAndExtraVo<DeveloperCreateRefundVo> developerCreateRefund(DeveloperCreateRefundQuery body){
        baseQuery(body);
        return getTransactionClient().developerCreateRefund(body);
    }

    /**
     * 生活服务交易系统->退货退款->同步退款审核结果
     * @param body 同步退款审核结果请求值
     * @return
     */
    public DataAndExtraVo<BaseVo> refundMerchantAuditCallback(RefundMerchantAuditCallbackQuery body){
        baseQuery(body);
        return getTransactionClient().refundMerchantAuditCallback(body);
    }

    /**
     * 生活服务交易系统->退货退款->查询退款
     * @param body
     * @return
     */
    public DataAndExtraVo<IndustryQueryRefundVo> industryQueryRefund(QueryRefundQuery body){
        baseQuery(body);
        return getTransactionClient().industryQueryRefund(body);
    }

    /**
     * 生活服务交易系统->预约->创建预约单
     * @param body
     * @return
     */
    public DataAndExtraVo<CreateBookVo> createBook(CreateBookQuery body){
        baseQuery(body);
        return getTransactionClient().createBook(body);
    }

    /**
     * 生活服务交易系统->预约->预约接单结果回调
     * @param body
     */
    public DataAndExtraVo<Void> bookResultCallback(BookResultCallbackQuery body){
        baseQuery(body);
        return getTransactionClient().bookResultCallback(body);
    }


    /**
     * 生活服务交易系统->预约->商家取消预约
     * @param body
     */
    public DataAndExtraVo<Void> merchantCancelBook(MerchantCancelBookQuery body){
        baseQuery(body);
        return getTransactionClient().merchantCancelBook(body);
    }


    /**
     * 生活服务交易系统->预约->用户取消预约
     * @param body
     */
    public DataAndExtraVo<Void> userCancelBook(UserCancelBookQuery body){
        baseQuery(body);
        return getTransactionClient().userCancelBook(body);
    }

    /**
     * 生活服务交易系统->预约->查询预约单信息
     */
    public DataAndExtraVo<List<QueryBookVo>> queryBook(QueryBookQuery body){
        baseQuery(body);
        return getTransactionClient().queryBook(body);
    }

    /**
     * 生活服务交易系统->预下单->关闭订单
     * @return
     */
    public DataAndExtraVo<Void> closeOrder(CloseOrderQuery body) {
        baseQuery(body);
        return getTransactionClient().closeOrder(body);
    }

    /**
     * 生活服务交易系统->核销->核销工具->查询用户券列表
     * @param body
     */
    public DataAndExtraVo<QueryUserCertificatesVo> queryUserCertificates(QueryUserCertificatesQuery body){
        baseQuery(body);
        return getTransactionClient().queryUserCertificates(body);
    }


    /**
     * 生活服务交易系统->核销->核销工具->查询订单可用门店
     * @param body
     */
    public DataAndExtraVo<QueryCertificatesOrderInfo> orderCanUse(OrderCanUseQuery body) {
        baseQuery(body);
        return getTransactionClient().orderCanUse(body);
    }

    /**
     * 生活服务交易系统->核销->核销工具->设置商家展示信息
     * @param body
     */
    public DySimpleResult<Void> updateMerchantConf(UpdateMerchantConfQuery body) {
        baseQuery(body);
        return getTransactionClient().updateMerchantConf(body);
    }

    /**
     * 生活服务交易系统->核销->核销工具->查询商家配置文案
     * @param body
     */
    public DySimpleResult<TradeToolkitQueryTextVo> tradeToolkitQueryText(TradeToolkitQueryTextQuery body) {
        baseQuery(body);
        return getTransactionClient().tradeToolkitQueryText(body);
    }

    /**
     * 生活服务交易系统->核销->核销工具->设置订单详情页按钮白名单接口
     * @param body
     */
    public DySimpleResult<Void> buttonWhiteSetting(ButtonWhiteSettingQuery body){
        baseQuery(body);
        return getTransactionClient().buttonWhiteSetting(body);
    }

    /**
     * 生活服务交易系统->核销->核销工具->设置小程序跳转path
     * @param body
     */
    public DySimpleResult<Void> updateMerchantPath(UpdateMerchantPathQuery body) {
        baseQuery(body);
        return getTransactionClient().updateMerchantPath(body);
    }

    /**
     * 生活服务交易系统->分账->发起分账
     * @param body 发起分账请求值
     * @return
     */
    public DataAndExtraVo<CreateSettleV2Vo> createSettleV2(CreateSettleV2Query body) {
        baseQuery(body);
        return getTransactionClient().createSettleV2(body);
    }
}
