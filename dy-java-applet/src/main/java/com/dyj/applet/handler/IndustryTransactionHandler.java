package com.dyj.applet.handler;

import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dyj.applet.domain.QueryAndCalculateMarketingResult;
import com.dyj.applet.domain.QueryCertificatesOrderInfo;
import com.dyj.applet.domain.QueryIndustryItemOrderInfo;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DataAndExtraVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.vo.BaseVo;
import com.dyj.common.interceptor.ClientTokenInterceptor;

import java.util.List;

/**
 * 交易系统 生活服务交易系统
 */
public class IndustryTransactionHandler extends AbstractAppletHandler{
    public IndustryTransactionHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }




    /**
     * 行业交易系统->预下单->查询 CPS 信息
     * @param body 查询 CPS 信息请求值
     * @return
     */
    public DySimpleResult<QueryIndustryCpsVo> queryTradeCps(QueryIndustryOrderCpsQuery body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().queryTradeCps(body);
    }

    /**
     * 行业交易系统->预下单->查询订单信息
     * @param body 查询订单信息请求值
     * @return
     */
    public DySimpleResult<QueryTradeOrderVo> queryTradeOrder(QueryTradeOrderQuery body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().queryTradeOrder(body);
    }


    /**
     * 行业交易系统->预下单->开发者发起下单
     * @param body 开发者发起下单
     * @return
     */
    public DySimpleResult<PreCreateIndustryOrderVo> createTradeOrder(CreateTradeOrderQuery body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().createTradeOrder(body);
    }

    /**
     * 行业交易系统->核销->抖音码->验券准备
     * @param body 验券准备请求值
     * @return
     */
    public DySimpleResult<TradeDeliveryPrepareVo> tradeDeliveryPrepare(DeliveryPrepareQuery body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().tradeDeliveryPrepare(body);
    }

    /**
     * 行业交易系统->核销->抖音码->验券
     * @param body 验券准备请求值
     * @return
     */
    public DySimpleResult<TradeDeliveryVerifyVo> tradeDeliveryVerify(DeliveryVerifyQuery body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().tradeDeliveryVerify(body);
    }

    /**
     * 行业交易系统->核销->抖音码->查询劵状态信息
     * @param body 查询劵状态信息请求值
     * @return
     */
    public DySimpleResult<List<QueryIndustryItemOrderInfo>> queryItemOrderInfo(QueryIndustryItemOrderInfoQuery body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().queryItemOrderInfo(body);
    }

    /**
     * 行业交易系统->核销->三方码->推送核销状态
     * @param body 推送核销状态
     * @return
     */
    public DySimpleResult<TradePushDeliveryVo> tradePushDelivery(TradePushDeliveryQuery body){
        baseQuery(body);
        return getIndustryOpenTransactionClient().tradePushDelivery(body);
    }

    /**
     * 行业交易系统->分账->发起分账
     * @param body 发起分账
     * @return
     */
    public DySimpleResult<CreateSettleV2Vo> tradeCreateSettle(CreateSettleV2Query body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().tradeCreateSettle(body);
    }

    /**
     * 行业交易系统->分账->查询分账
     * @param body 查询分账
     * @return
     */
    public DySimpleResult<TradeQuerySettleVo> tradeQuerySettle(TradeQuerySettleQuery body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().tradeQuerySettle(body);
    }

    /**
     * 行业交易系统->退货退款->开发者发起退款
     * @param body 开发者发起退款
     * @return
     */
    public DySimpleResult<CreateRefundVo> tradeCreateRefund(TradeCreateRefundQuery body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().tradeCreateRefund(body);
    }

    /**
     * 行业交易系统->退货退款->同步退款审核结果
     * @param body 同步退款审核结果请求值
     * @return
     */
    public DySimpleResult<Void> merchantAuditCallbackV2(MerchantAuditCallbackQuery body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().merchantAuditCallbackV2(body);
    }

    /**
     * 行业交易系统->退货退款->查询退款
     * @param body 查询退款
     * @return
     */
    public DySimpleResult<TradeQueryRefundVo> tradeQueryRefund(QueryRefundQuery body) {
        baseQuery(body);
        return getIndustryOpenTransactionClient().tradeQueryRefund(body);
    }
}
