package com.dyj.applet.handler;

import com.dyj.applet.domain.query.PlayletBusinessUploadQuery;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

/**
 * @author danmo
 * @date 2024-05-21 10:40
 **/
public class PlayletBusinessHandler extends AbstractAppletHandler {
    public PlayletBusinessHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 上传短剧权益事件
     *
     * @param query 上传短剧权益事件入参
     * @return DySimpleResult
     */
    public DySimpleResult<String> upload(PlayletBusinessUploadQuery query) {
        baseQuery(query);
        return getPlayletBusinessClient().upload(query);
    }
}
