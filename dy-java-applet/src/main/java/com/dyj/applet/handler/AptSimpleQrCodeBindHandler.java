package com.dyj.applet.handler;

import com.dyj.applet.domain.vo.SimpleQrBindListVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

/**
 * @author danmo
 * @date 2024-07-04 10:44
 **/
public class AptSimpleQrCodeBindHandler extends AbstractAppletHandler {

    public AptSimpleQrCodeBindHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }


    /**
     * 查询普通二维码绑定列表
     *
     * @param pageNum  分页编号，从 1 开始
     * @param pageSize 分页大小，小于等于 50
     * @return DySimpleResult<SimpleQrBindListVo>
     */
    public DySimpleResult<SimpleQrBindListVo> querySimpleQrBindList(Long pageNum, Long pageSize) {
        return getSimpleQrCodeBindClient().querySimpleQrBindList(baseQuery(), pageNum, pageSize);
    }

    /**
     * 查询普通二维码绑定列表(服务商代调用场景)
     *
     * @param pageNum  分页编号，从 1 开始
     * @param pageSize 分页大小，小于等于 50
     * @return DySimpleResult<SimpleQrBindListVo>
     */
    public DySimpleResult<SimpleQrBindListVo> queryTpSimpleQrBindList(Long pageNum, Long pageSize) {
        return getSimpleQrCodeBindClient().queryTpSimpleQrBindList(baseQuery(), pageNum, pageSize);
    }

    /**
     * 新增绑定二维码
     *
     * @param exclusiveQrUrlPrefix 是否独占该链接作为前缀0：不独占1：独占
     * @param loadPath             跳转的小程序路径
     * @param qrUrl                绑定的链接地址
     * @param stage                测试范围，latest-测试 online-线上
     * @return DySimpleResult
     */
    public DySimpleResult<String> addSimpleQrBind(Integer exclusiveQrUrlPrefix, String loadPath, String qrUrl, String stage) {
        return getSimpleQrCodeBindClient().addSimpleQrBind(baseQuery(), exclusiveQrUrlPrefix, loadPath, qrUrl, stage);
    }

    /**
     * 新增绑定二维码(服务商代调用场景)
     *
     * @param exclusiveQrUrlPrefix 是否独占该链接作为前缀0：不独占1：独占
     * @param loadPath             跳转的小程序路径
     * @param qrUrl                绑定的链接地址
     * @param stage                测试范围，latest-测试 online-线上
     * @return DySimpleResult
     */
    public DySimpleResult<String> addTpSimpleQrBind(Integer exclusiveQrUrlPrefix, String loadPath, String qrUrl, String stage) {
        return getSimpleQrCodeBindClient().addTpSimpleQrBind(baseQuery(), exclusiveQrUrlPrefix, loadPath, qrUrl, stage);
    }

    /**
     * 更新绑定二维码链接
     *
     * @param beforeQrUrl          更新前的链接地址
     * @param exclusiveQrUrlPrefix 是否独占该链接作为前缀0：不独占1：独占
     * @param loadPath             跳转的小程序路径
     * @param qrUrl                绑定的链接地址
     * @param stage                测试范围，latest-测试 online-线上
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> updateSimpleQrBind(String beforeQrUrl,
                                                     Integer exclusiveQrUrlPrefix,
                                                     String loadPath,
                                                     String qrUrl,
                                                     String stage) {
        return getSimpleQrCodeBindClient().updateSimpleQrBind(baseQuery(), beforeQrUrl, exclusiveQrUrlPrefix, loadPath, qrUrl, stage);
    }

    /**
     * 更新绑定二维码链接(服务商代调用场景)
     *
     * @param beforeQrUrl          更新前的链接地址
     * @param exclusiveQrUrlPrefix 是否独占该链接作为前缀0：不独占1：独占
     * @param loadPath             跳转的小程序路径
     * @param qrUrl                绑定的链接地址
     * @param stage                测试范围，latest-测试 online-线上
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> updateTpSimpleQrBind(String beforeQrUrl,
                                                       Integer exclusiveQrUrlPrefix,
                                                       String loadPath,
                                                       String qrUrl,
                                                       String stage) {
        return getSimpleQrCodeBindClient().updateTpSimpleQrBind(baseQuery(), beforeQrUrl, exclusiveQrUrlPrefix, loadPath, qrUrl, stage);
    }

    /**
     * 更新绑定二维码状态
     *
     * @param qrUrl  需要更新状态的链接地址
     * @param status 需要更新的状态1：发布2：下线
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> updateSimpleQrBindStatus(String qrUrl, Integer status) {
        return getSimpleQrCodeBindClient().updateSimpleQrBindStatus(baseQuery(), qrUrl, status);
    }


    /**
     * 更新绑定二维码状态(服务商代调用场景)
     *
     * @param qrUrl  需要更新状态的链接地址
     * @param status 需要更新的状态1：发布2：下线
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> updateTpSimpleQrBindStatus(String qrUrl, Integer status) {
        return getSimpleQrCodeBindClient().updateTpSimpleQrBindStatus(baseQuery(), qrUrl, status);
    }

    /**
     * 删除绑定二维码链接
     *
     * @param qrUrl 需要删除的链接地址
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> deleteSimpleQrBind(String qrUrl) {
        return getSimpleQrCodeBindClient().deleteSimpleQrBind(baseQuery(), qrUrl);
    }

    /**
     * 删除绑定二维码链接(服务商代调用场景)
     *
     * @param qrUrl 需要删除的链接地址
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> deleteTpSimpleQrBind(String qrUrl) {
        return getSimpleQrCodeBindClient().deleteTpSimpleQrBind(baseQuery(), qrUrl);
    }
}
