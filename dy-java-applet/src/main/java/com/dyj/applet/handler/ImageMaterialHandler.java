package com.dyj.applet.handler;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.query.AddFunctionConfigQuery;
import com.dyj.applet.domain.query.UploadImageMaterialQuery;
import com.dyj.applet.domain.vo.QueryFunctionConfigStatusVo;
import com.dyj.applet.domain.vo.UploadImageMaterialVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;
import com.dyj.common.interceptor.TransactionMerchantTokenInterceptor;

import java.util.List;

/**
 * 素材库
 */
public class ImageMaterialHandler extends AbstractAppletHandler{
    public ImageMaterialHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 运营->素材库->素材库图片上传
     * @param body
     * @return
     */
    public DySimpleResult<UploadImageMaterialVo> uploadImageMaterial(UploadImageMaterialQuery body){
        baseQuery(body);
        return getImageMaterialClient().uploadImageMaterial(body);
    }

    /**
     * 运营->素材库->素材库设置功能配置
     * @param body 素材库设置功能配置请求值
     * @return
     */
    public DySimpleResult<Void> addFunctionConfig(AddFunctionConfigQuery body) {
        baseQuery(body);
        return getImageMaterialClient().addFunctionConfig(body);
    }

    /**
     * 运营->素材库->素材库查询功能配置审核状态
     * @param functionId 功能id
     * @return
     */
    public DySimpleResult<QueryFunctionConfigStatusVo> queryFunctionConfigStatus(TransactionMerchantTokenTypeEnum type, List<String> functionId){
        BaseTransactionMerchantQuery query = new BaseTransactionMerchantQuery();
        query.setTransactionMerchantTokenType(type);
        baseQuery(query);
        return getImageMaterialClient().queryFunctionConfigStatus(query, functionId);
    }
}
