package com.dyj.applet.handler;

import com.dyj.applet.domain.UpdateCommonPlanStatus;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DyProductResult;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-28 16:27
 **/
public class LifeServiceHandler extends AbstractAppletHandler {
    public LifeServiceHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 商铺同步
     *
     * @param query 入参
     * @return DyResult<SupplierSyncVo>
     */
    public DyResult<SupplierSyncVo> supplierSync(SupplierSyncQuery query) {
        baseQuery(query);
        return getLifeServicesClient().supplierSync(query);
    }

    /**
     * 查询店铺
     *
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierVo>
     */
    public DyResult<SupplierVo> querySupplier(String supplierExtId) {
        return getLifeServicesClient().querySupplier(baseQuery(), supplierExtId);
    }

    /**
     * 获取抖音POI ID
     *
     * @param amapId 高德POI ID
     * @return DyResult<PoiIdVo>
     */
    public DyResult<PoiIdVo> queryPoiId(String amapId) {
        return getLifeServicesClient().queryPoiId(baseQuery(), amapId);
    }


    /**
     * 店铺匹配任务结果查询
     *
     * @param supplierTaskIds 店铺任务ID
     * @return DyResult<SupplierTaskResultVo>
     */
    public DyResult<SupplierTaskResultVo> querySupplierTaskResult(String supplierTaskIds) {
        return getLifeServicesClient().querySupplierTaskResult(baseQuery(), supplierTaskIds);
    }

    /**
     * 店铺匹配任务状态查询
     *
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierTaskStatusVo>
     */
    public DyResult<SupplierTaskStatusVo> querySupplierMatchStatus(String supplierExtId) {
        return getLifeServicesClient().querySupplierMatchStatus(baseQuery(), supplierExtId);
    }

    /**
     * 提交门店匹配任务
     *
     * @param query 入参
     * @return DyResult<SupplierSubmitTaskVo>
     */
    public DyResult<SupplierSubmitTaskVo> submitSupplierMatchTask(SupplierSubmitTaskQuery query) {
        baseQuery(query);
        return getLifeServicesClient().submitSupplierMatchTask(query);
    }


    /**
     * 查询全部店铺信息接口(天级别请求5次)
     *
     * @return DyResult<SupplierTaskVo>
     */
    public DyResult<SupplierTaskVo> queryAllSupplier() {
        return getLifeServicesClient().queryAllSupplier(baseQuery());
    }

    /**
     * 查询店铺全部信息任务返回内容
     *
     * @param taskId 任务ID
     * @return DyResult<SupplierTaskVo>
     */
    public DyResult<SupplierTaskVo> querySupplierCallback(String taskId) {
        return getLifeServicesClient().querySupplierCallback(baseQuery(), taskId);
    }

    /**
     * （老版本）SKU同步
     *
     * @param query 入参
     * @return DyResult<BaseVo>
     */
    public DyResult<BaseVo> skuSync(SkuSyncQuery query) {
        baseQuery(query);
        return getLifeServicesClient().skuSync(query);
    }

    /**
     * （老版本）sku拉取(该接口由接入方实现)
     *
     * @param spuExtId  接入方SPU ID 列表
     * @param startDate 拉取价格时间区间[start_date, end_date)
     * @param endDate   拉取价格时间区间[start_date, end_date)
     * @return DyResult<SkuHotelPullVo>
     */
    public DyResult<SkuHotelPullVo> skuHotelPull(List<String> spuExtId, String startDate, String endDate) {
        return getLifeServicesClient().skuHotelPull(baseQuery(), spuExtId, startDate, endDate);
    }

    /**
     * （老版本）多门店SPU同步
     *
     * @param query 入参
     * @return DyResult<SpuSyncVo>
     */
    public DyResult<SpuSyncVo> spuSync(SpuSyncQuery query) {
        baseQuery(query);
        return getLifeServicesClient().spuSync(query);
    }

    /**
     * （老版本）多门店SPU状态同步
     *
     * @param spuExtIdList 接入方商品ID列表
     * @param status       SPU状态， 1 - 在线; 2 - 下线
     * @return DyResult<SpuStatusVo>
     */
    public DyResult<SpuStatusVo> spuStatusSync(List<String> spuExtIdList, Integer status) {
        return getLifeServicesClient().spuStatusSync(baseQuery(), spuExtIdList, status);
    }

    /**
     * （老版本）多门店SPU库存同步
     *
     * @param spuExtId 接入方商品ID
     * @param stock    库存
     * @return DyResult<SpuStockVo>
     */
    public DyResult<SpuStockVo> spuStockSync(String spuExtId, Long stock) {
        return getLifeServicesClient().spuStockSync(SpuStockQuery.builder()
                .spuExtId(spuExtId)
                .stock(stock)
                .tenantId(agentConfiguration.getTenantId())
                .clientKey(agentConfiguration.getClientKey())
                .build());
    }

    /**
     * （老版本）多门店SPU信息查询
     *
     * @param spuExtId                   第三方SPU ID
     * @param needSpuDraft               是否需要商品草稿数据(带有商品的审核状态，只展示最近30天的数据，并且最多最近提交的20次内)
     * @param spuDraftCount              需要商品草稿数据的数目(0-20)，超过这个范围默认返回20个
     * @param supplierIdsForFilterReason 供应商id列表，需要商品在某供应商下的过滤状态
     * @return DyResult<SpuVo>
     */
    public DyResult<SpuVo> spuQuery(String spuExtId, Boolean needSpuDraft, Integer spuDraftCount, List<String> supplierIdsForFilterReason) {
        return getLifeServicesClient().spuQuery(baseQuery(), spuExtId, needSpuDraft, spuDraftCount, supplierIdsForFilterReason);
    }

    /**
     * 创建/修改团购商品
     *
     * @param query 入参
     * @return DyProductResult<SaveGoodsProductVo>
     */
    public DyProductResult<SaveGoodsProductVo> saveGoodsProduct(SaveGoodsProductQuery query) {
        baseQuery(query);
        return getLifeServicesClient().saveGoodsProduct(query);
    }

    /**
     * 免审修改商品
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    public DyProductResult<String> freeAuditGoodsProduct(FreeAuditGoodsProductQuery query) {
        baseQuery(query);
        return getLifeServicesClient().freeAuditGoodsProduct(query);
    }

    /**
     * 上下架商品
     *
     * @param productId 商品ID
     * @param outId     商品外部ID
     * @param opType    操作类型  1-上线 2-下线
     * @return DyProductResult<String>
     */
    public DyProductResult<String> operateGoodsProduct(String productId, String outId, Integer opType) {
        return getLifeServicesClient().operateGoodsProduct(OperateGoodsProductQuery.build()
                .productId(productId)
                .outId(outId)
                .opType(opType)
                .tenantId(agentConfiguration.getTenantId())
                .clientKey(agentConfiguration.getClientKey())
                .build());
    }

    /**
     * 同步库存
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    public DyProductResult<String> syncGoodsStock(SyncGoodsStockQuery query) {
        baseQuery(query);
        return getLifeServicesClient().syncGoodsStock(query);
    }

    /**
     * 查询商品模板
     *
     * @param categoryId  行业类目；详细见； 商品类目表
     * @param productType 商品类型 1 : 团购套餐 3 : 预售券 4 : 日历房 5 : 门票 7 : 旅行跟拍 8 : 一日游 11 : 代金券
     * @return DyProductResult<GoodsTemplateVo>
     */
    public DyProductResult<GoodsTemplateVo> getGoodsTemplate(String categoryId, Integer productType) {
        return getLifeServicesClient().getGoodsTemplate(baseQuery(), categoryId, productType);
    }

    /**
     * 查询商品草稿数据
     *
     * @param productIds 商品ID列表（逗号分隔）
     * @param outIds     外部商品ID列表（逗号分隔）
     * @return DyProductResult<GoodsProductDraftVo>
     */
    public DyProductResult<GoodsProductDraftVo> getGoodsProductDraft(String productIds, String outIds) {
        return getLifeServicesClient().getGoodsProductDraft(baseQuery(), productIds, outIds);
    }

    /**
     * 查询商品线上数据
     *
     * @param productIds 商品ID列表（逗号分隔）
     * @param outIds     外部商品ID列表（逗号分隔）
     * @return DyProductResult<GoodsProductOnlineVo>
     */
    public DyProductResult<GoodsProductOnlineVo> getGoodsProductOnline(String productIds, String outIds) {
        return getLifeServicesClient().getGoodsProductOnline(baseQuery(), productIds, outIds);
    }

    /**
     * 查询商品线上数据列表
     *
     * @param cursor 第一页不传，之后用前一次返回的next_cursor传入进行翻页
     * @param count  分页数量，不传默认为5
     * @param status 过滤在线状态 1-在线 2-下线 3-封禁
     * @return DyProductResult
     */
    public DyProductResult<GoodsProductOnlineVo> queryGoodsProductOnlineList(String cursor, Integer count, Integer status) {
        return getLifeServicesClient().queryGoodsProductOnlineList(baseQuery(), cursor, count, status);
    }

    /**
     * 查询商品草稿数据列表
     *
     * @param cursor 第一页不传，之后用前一次返回的next_cursor传入进行翻页
     * @param count  分页数量，不传默认为5
     * @param status 过滤草稿状态，10-审核中 12-审核失败 1-审核通过
     * @return DyProductResult<GoodsProductDraftVo>
     */
    public DyProductResult<GoodsProductDraftVo> queryGoodsProductDraftList(String cursor, Integer count, Integer status) {
        return getLifeServicesClient().queryGoodsProductDraftList(baseQuery(), cursor, count, status);
    }

    /**
     * 创建/更新多SKU商品的SKU列表
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    public DyProductResult<String> batchSaveGoodsSku(BatchSaveGoodsSkuQuery query) {
        baseQuery(query);
        return getLifeServicesClient().batchSaveGoodsSku(query);
    }

    /**
     * 查询商品品类
     *
     * @param categoryId 行业类目ID，返回当前id下的直系子类目信息；传0或者不传，均返回所有一级行业类目
     * @param accountId  服务商的入驻商户ID/代运营的商户ID，不传时默认为服务商身份
     * @return GoodsCategoryVo
     */
    public GoodsCategoryVo getGoodsCategory(String categoryId, String accountId) {
        return getLifeServicesClient().getGoodsCategory(baseQuery(), categoryId, accountId);
    }

    /**
     * 订单同步
     *
     * @param query 入参
     * @return DyResult<SyncOrderVo>
     */
    public DyResult<SyncOrderVo> syncOrder(SyncOrderQuery query) {
        return getLifeServicesClient().syncOrder(query);
    }

    /**
     * 获取POI基础数据
     *
     * @param poiId     抖音poi_id
     * @param beginDate 最近30天，开始日期(yyyy-MM-dd)
     * @param endDate   最近30天，结束日期(yyyy-MM-dd)
     * @return DyResult<PoiBaseDataVo>
     */
    public DyResult<PoiBaseDataVo> queryPoiBaseData(String poiId, String beginDate, String endDate) {
        return getLifeServicesClient().queryPoiBaseData(baseQuery(), poiId, beginDate, endDate);
    }

    /**
     * POI用户数据
     *
     * @param poiId    抖音poi_id
     * @param dateType 近7/15/30天
     * @return DyResult<PoiUserDataVo>
     */
    public DyResult<PoiUserDataVo> queryPoiUserData(String poiId, Integer dateType) {
        return getLifeServicesClient().queryPoiUserData(baseQuery(), poiId, dateType);
    }

    /**
     * POI服务基础数据
     *
     * @param poiId       抖音poi_id
     * @param serviceType 服务类型，40:民宿
     * @param beginDate   最近30天，开始日期(yyyy-MM-dd)
     * @param endDate     最近30天，结束日期(yyyy-MM-dd)
     * @return DyResult<PoiServiceBaseDataVo>
     */
    public DyResult<PoiServiceBaseDataVo> queryPoiServiceBaseData(String poiId, Integer serviceType, String beginDate, String endDate) {
        return getLifeServicesClient().queryPoiServiceBaseData(baseQuery(), poiId, serviceType, beginDate, endDate);
    }

    /**
     * POI服务成交用户数据
     *
     * @param poiId       抖音poi_id
     * @param serviceType 近7/15/30天
     * @param dateType    服务类型，40:民宿
     * @return DyResult<PoiServiceTransactionUserDataVo>
     */
    public DyResult<PoiServiceTransactionUserDataVo> queryPoiServiceTransactionUserData(String poiId, Integer serviceType, Integer dateType) {
        return getLifeServicesClient().queryPoiServiceTransactionUserData(baseQuery(), poiId, serviceType, dateType);
    }

    /**
     * POI热度榜
     *
     * @param billboardType 10：近30日餐饮类POI的热度TOP500；20：近30日景点类POI的热度TOP500；30：近30日住宿类POI的热度TOP500
     * @return DyResult<PoiBillboardVo>
     */
    public DyResult<PoiBillboardVo> queryPoiBillboard(String billboardType) {
        return getLifeServicesClient().queryPoiBillboard(baseQuery(), billboardType);
    }

    /**
     * POI认领列表
     *
     * @param openId 通过/oauth/access_token/获取，用户唯一标志
     * @param cursor 分页游标, 第一页请求cursor是0, response中会返回下一页请求用到的cursor, 同时response还会返回has_more来表明是否有更多的数据
     * @param count  每页数量
     * @return DyResult
     */
    public DyResult<PoiClaimVo> queryPoiClaimList(String openId, String cursor, Integer count) {
        return getLifeServicesClient().queryPoiClaimList(baseQuery(), openId, cursor, count);
    }

    /**
     * 通过高德POI ID获取抖音POI ID
     *
     * @param amapId 高德POI ID
     * @return DyResult<AmapPoiIdVo>
     */
    public DyResult<AmapPoiIdVo> queryAmapPoiId(String amapId) {
        return getLifeServicesClient().queryAmapPoiId(baseQuery(), amapId);

    }

    /**
     * 优惠券同步
     *
     * @param query 入参
     * @return DyResult<SyncV2CouponVo>
     */
    public DyResult<SyncV2CouponVo> syncV2Coupon(SyncV2CouponQuery query) {
        baseQuery(query);
        return getLifeServicesClient().syncV2Coupon(query);
    }

    /**
     * 优惠券更新
     *
     * @param query 入参
     * @return DyResult<SyncV2CouponVo>
     */
    public DyResult<SyncV2CouponVo> syncV2CouponAvailable(SyncV2CouponAvailableQuery query) {
        baseQuery(query);
        return getLifeServicesClient().syncV2CouponAvailable(query);
    }


    /**
     * 通用佣金计划查询带货数据
     *
     * @param planIdList 通用佣金计划ID列表
     * @return DySimpleResult<CommonPlanSellDetailVo>
     */
    public DySimpleResult<CommonPlanSellDetailVo> queryCommonPlanSellDetail(List<Long> planIdList) {
        return getLifeServicesClient().queryCommonPlanSellDetail(baseQuery(), planIdList);
    }

    /**
     * 通用佣金计划查询带货达人列表
     *
     * @param pageNum  分页参数：页码，从1开始计数
     * @param pageSize 分页参数：数量，1<=page_size<=100
     * @param planId   通用佣金计划ID
     * @return DySimpleResult<CommonPlanTalentVo>
     */
    public DySimpleResult<CommonPlanTalentVo> queryCommonPlanTalentList(Integer pageNum, Integer pageSize, Long planId) {
        return getLifeServicesClient().queryCommonPlanTalentList(baseQuery(), pageNum, pageSize, planId);
    }

    /**
     * 通用佣金计划查询达人带货数据
     *
     * @param douyinIdList 待查询的达人抖音号列表
     * @param planId       通用佣金计划ID
     * @return DySimpleResult<CommonPlanTalentSellDetailVo>
     */
    public DySimpleResult<CommonPlanTalentSellDetailVo> queryCommonPlanTalentSellDetail(List<String> douyinIdList, Long planId) {
        return getLifeServicesClient().queryCommonPlanTalentSellDetail(baseQuery(), douyinIdList, planId);
    }

    /**
     * 通用佣金计划查询达人带货详情
     *
     * @param contentType 带货场景，1-仅短视频 2-仅直播间 3-短视频和直播间
     * @param douyinId    达人抖音号
     * @param pageNum     分页参数：页码，从1开始计数
     * @param pageSize    分页参数：数量，1<=page_size<=100
     * @param planId      通用佣金计划ID
     * @return DySimpleResult<CommonPlanTalentMedialVo>
     */
    public DySimpleResult<CommonPlanTalentMedialVo> queryCommonPlanTalentMediaList(Integer contentType, String douyinId, Integer pageNum, Integer pageSize, Long planId) {
        return getLifeServicesClient().queryCommonPlanTalentMediaList(baseQuery(), contentType, douyinId, pageNum, pageSize, planId);
    }

    /**
     * 查询通用佣金计划
     *
     * @param pageNo    分页参数：页码，从1开始计数
     * @param pageSize  分页参数：数量，1<=page_size<=100
     * @param spuId     上传小程序商品时返回的抖音内部商品ID
     * @param spuIdType 商品id类型 Product=1  MSpu=2
     * @return DySimpleResult<CommonPlanListVo>
     */
    public DySimpleResult<CommonPlanListVo> queryCommonPlanList(Integer pageNo, Integer pageSize, Long spuId, Integer spuIdType) {
        return getLifeServicesClient().queryCommonPlanList(baseQuery(), pageNo, pageSize, spuId, spuIdType);
    }

    /**
     * 发布/修改通用佣金计划
     *
     * @param commissionRate 商品的抽佣率，万分数。 小程序商品可选值范围：100-2900具有代运营商服合作关系的商品可选值范围：100-8000，须小于或等于商家确认的佣金比例
     * @param contentType    商品支持的达人带货场景，可选以下的值：1：仅短视频2：仅直播间3：短视频和直播间
     * @param spuId          上传小程序商品时返回的抖音内部商品ID
     * @param planId         计划ID，创建计划时，plan_id不传或传0。修改现有计划时，传入待修改计划的ID
     * @return DySimpleResult<SaveCommonPlanVo>
     */
    public DySimpleResult<SaveCommonPlanVo> saveCommonPlan(Long commissionRate, Integer contentType, Long spuId, Long planId) {
        return getLifeServicesClient().saveCommonPlan(baseQuery(), commissionRate, contentType, spuId, planId);
    }

    /**
     * 修改通用佣金计划状态
     *
     * @param statusList 待更新的计划与状态列表
     * @return DySimpleResult<UpdateCommonPlanStatusVo>
     */
    public DySimpleResult<UpdateCommonPlanStatusVo> updateCommonPlanStatus(List<UpdateCommonPlanStatus> statusList) {
        return getLifeServicesClient().updateCommonPlanStatus(baseQuery(), statusList);
    }

    /**
     * 发布/修改直播间定向佣金计划
     *
     * @param query 入参
     * @return DySimpleResult<SaveCommonPlanVo>
     */
    public DySimpleResult<SaveCommonPlanVo> saveLivePlan(SaveLivePlanQuery query) {
        baseQuery(query);
        return getLifeServicesClient().saveLivePlan(query);
    }

    /**
     * 发布/修改短视频定向佣金计划
     *
     * @param query 入参
     * @return DySimpleResult<SaveCommonPlanVo>
     */
    public DySimpleResult<SaveCommonPlanVo> saveShortVideoPlan(SaveShortVideoPlanQuery query) {
        baseQuery(query);
        return getLifeServicesClient().saveShortVideoPlan(query);
    }

    /**
     * 取消定向佣金计划指定的达人
     *
     * @param planId   定向佣金计划ID
     * @param douyinId 达人抖音号
     * @return DySimpleResult
     */
    public DySimpleResult<String> deletePlanTalent(Long planId, String douyinId) {
        return getLifeServicesClient().deletePlanTalent(baseQuery(), planId, douyinId);
    }

    /**
     * 查询达人的定向佣金计划带货数据
     *
     * @param douyinIdList 待查询的达人抖音号列表
     * @param planId       定向佣金计划ID
     * @return DySimpleResult<OrientedPlanTalentSellDetailVo>
     */
    public DySimpleResult<OrientedPlanTalentSellDetailVo> queryOrientedPlanTalentSellDetail(List<String> douyinIdList, Long planId) {
        return getLifeServicesClient().queryOrientedPlanTalentSellDetail(baseQuery(), douyinIdList, planId);
    }

    /**
     * 通过商品 ID 查询定向佣金计划
     *
     * @param spuIdList 小程序商品 ID 列表，长度限制不超过 50 个
     * @return DySimpleResult<OrientedPlanTalentVo>
     */
    public DySimpleResult<OrientedPlanTalentVo> queryOrientedPlanList(List<Long> spuIdList) {
        return getLifeServicesClient().queryOrientedPlanList(baseQuery(), spuIdList);
    }

    /**
     * 查询定向佣金计划带货汇总数据
     *
     * @param planIdList 待查询的定向佣金计划ID列表
     * @return DySimpleResult<OrientedPlanSellDetailVo>
     */
    public DySimpleResult<OrientedPlanSellDetailVo> queryOrientedPlanSellDetail(List<Long> planIdList) {
        return getLifeServicesClient().queryOrientedPlanSellDetail(baseQuery(), planIdList);
    }
}
