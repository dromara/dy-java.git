![输入图片说明](readme-pic/0-logo.png)

<div align="center">

[![star](https://gitee.com/dromara/dy-java/badge/star.svg?theme=gvp)](https://gitee.com/dromara/dy-java/stargazers)
[![fork](https://gitee.com/dromara/dy-java/badge/fork.svg?theme=gvp)](https://gitee.com/dromara/dy-java/members)
[![LinkWeChat](https://img.shields.io/badge/DyJava-V1.0.0-brightgreen)](https://gitee.com/dromara/dy-java/tree/v1.0.0.0/)
[![license](http://img.shields.io/badge/license-Apache%202.0-orange)](https://gitee.com/dromara/dy-java/blob/master/LICENSE)
![star](https://gitcode.com/dromara/dy-java/star/badge.svg)

</div>

## 一、关于 DyJava

### 1.1、简介

DyJava 是一款功能强大的抖音 Java 开发工具包（SDK），支持抖音各个应用 OpenAPI 快速调用，包括但不限于移动/网站应用、抖音开放平台、抖店和抖音小程序等。

DyJava 致力于简化开发流程，提高开发效率，让开发者能够更专注于创新和业务逻辑的实现。

### 1.2、设计使命

> 抖音开发，可以更简单。

DyJava 的初心就像 Slogan 一样很简单纯粹：

* 助力开发者高效快捷接入抖音开放平台，提高开发效率，推动平台发展。

但假如说 DyJava 比抖音开放平台 OpenAPI 更好用一些，那一定是因为 DyJava 站在了抖音开放生态的肩膀上。

所以在后续 DyJava 开源社区的发展中，我们还将践行更多的使命：

* 提供抖音开放能力的应用实例，助力企业和商家了解抖音生态及其商业价值。
* 携手抖音开放平台，为企业和商家提供优质技术服务，助力其实现商业目标。

### 1.3、愿景

DyJava 精心打磨每一个细节，提供更高效流畅的抖音开发体验，共筑抖音生态的无限可能。

## 二、DyJava 的能力

### 2.1、最新特性

截至 2025-01，DyJava 已完成对抖音移动\网站应用全部 OpenAPI 以及抖音小程序应用大部分 OpenAPI 的支持。

以抖音开放平台最新版文档（2025-01）为基准，具体支持如下：

*  **移动\网站应用（全部支持）** 

    - [x]  **个人资料；** 
    - [x]  **关系能力：** 用户数据、粉丝判断、粉丝画像数据；
    - [x]  **内容能力：** 抖音、投稿任务、视频数据、场景跳转；
    - [x]  **互动评论：** 视频评论管理、查询关键词视频列表、关键词视频评论管理；
    - [x]  **私信群聊：** 私信管理、群聊管理、经营工具、意向用户管理；

*  **小程序应用（90%支持）** 

    - [x]  **基础能力：** 联合授权、视频能力、线索组件、接口调用凭证、登录、Web化接入、隐私协议、流量主、小程序码与链接、用户信息、抖音号绑定、任务能力、分享、客服、小程序券；
    - [x]  **触达与营销：** 搜索能力、私信与群聊；
    - [x]  **支付：** 交易工具、交易系统、抖店绑定；
    - [x]  **运营：** 解决方案、数据分析、服务类目、直播间能力、抖音开放能力、能力申请、页面结构自定义、普通二维码绑定；

具体以 DyJava 开源仓库能力为准。

### 2.2、开发计划

DyJava 开源组计划于 2025-02-15 前完成对抖音小程序应用全部 OpenAPI 的支持，具体包括如下：

* **小程序应用待支持能力** 

    - [ ]  **基础能力：** 小程序推广计划、内容安全；
    - [ ]  **触达与营销：** 订阅消息、分发；
    - [ ]  **支付：** 评价；
    - [ ]  **运营：** 素材库；
    - [ ]  **其他：** 小程序视频打通能力、上传资源；

另外因抖音开放平台即将下线小程序 OpenAPI 中 **担保支付** 能力，DyJava 后续也不再支持。

## 三、开发指南

### 3.1、Maven 引用

*  **移动\网站应用** 

```xml
 <dependency>
     <groupId>com.dyj</groupId>
     <artifactId>dy-java-web</artifactId>
     <version>${dy-java.version}</version>
 </dependency>
```

*  **小程序应用** 

```xml
 <dependency>
     <groupId>com.dyj</groupId>
     <artifactId>dy-java-applet</artifactId>
     <version>${dy-java.version}</version>
 </dependency>
```

### 3.2、文件配置

``` yml
dyjava:
  beanId:  
  domain: https://open.douyin.com #自定义抖音域名，默认线上域名
  ttDomain: https://developer.toutiao.com #自定义头条域名，默认线上域名
  agents:  #可配置多个应用
    - tenantId: 1   #租户名称
      clientKey:  #应用Key
      clientSecret:   #应用秘钥
      #应用公钥版本
      application-public-key-version: 1
      #应用私钥文件地址(resources目录下的相对路径)
      application-private-key-path: private.key
      #应用公钥文件地址(resources目录下的相对路径)
      application-public-key-path: public.key
      #平台私钥文件地址(resources目录下的相对路径)
      platform-private-key-path: private.key
      #平台公钥文件地址(resources目录下的相对路径)
      platform-public-key-path: public.key
      #应用信息加载类，默认读取配置文件 实现IAgentConfigService接口
      agentSourceClass: com.dyj.common.service.impl.PropertiesAgentConfigServiceImpl
      #令牌信息加载类，默认读取缓存 实现IAgentTokenService接口
      tokenSourceClass: com.dyj.common.service.impl.CacheAgentTokenServiceImpl
      forest:
        backend: okhttp3             # 后端HTTP框架httpclient、okhttp3（默认为 okhttp3）
        max-connections: 1000        # 连接池最大连接数（默认为 500）
        max-route-connections: 500   # 每个路由的最大连接数（默认为 500）
        max-request-queue-size: 500  # 最大请求等待队列大小
        max-async-thread-size: 300   # 最大异步线程数
        max-async-queue-size: 16     # 最大异步线程池队列大小
        timeout: 3000                # 请求超时时间，单位为毫秒（默认为 3000）
        connect-timeout: 3000        # 连接超时时间，单位为毫秒（默认为 timeout）
        read-timeout: 3000           # 数据读取超时时间，单位为毫秒（默认为 timeout）
        max-retry-count: 3           # 请求失败后重试次数（默认为 0 次不重试）
        ssl-protocol: TLS            # 单向验证的HTTPS的默认TLS协议（默认为 TLS）
        log-enabled: true            # 打开或关闭日志（默认为 true）
        log-request: true            # 打开/关闭请求日志（默认为 true）
        log-response-status: true    # 打开/关闭响应状态日志（默认为 true）
        log-response-content: false  # 打开/关闭响应内容日志（默认为 false）
        async-mode: platform         # 异步模式（默认为 platform）
```

### 3.3、使用示例

*  **移动\网站应用** 

```java
//单租户获取AccessToken
AccessTokenVo accessToken = DyWebClient.getInstance().accessToken(code).getData();
//多租户单应用获取AccessToken
AccessTokenVo accessToken = DyWebClient.getInstance()
        .tenantId(1)
        .accessToken(code).getData();
//多租户多应用获取AccessToken
AccessTokenVo accessToken = DyWebClient.getInstance()
        .tenantId(1)
        .clientKey("123")
        .accessToken(code).getData();
```

*  **小程序应用** 

```java
//单租户获取AccessToken
DyAppletClient.getInstance().accessToken(code).getData();
//多租户单应用获取AccessToken
DyAppletClient.getInstance()
        .tenantId(1)
        .accessToken(code).getData();
//多租户多应用获取AccessToken
DyAppletClient.getInstance()
        .tenantId(1)
        .clientKey("123")
        .accessToken(code).getData();
```

### 3.4、必要说明

* `IAgentConfigService`接口的作用

用来获取`agent`应用信息，默认读取配置文件，从配置文件中读取`agent`应用信息：

````java
public class PropertiesAgentConfigServiceImpl implements IAgentConfigService {
    //通过配置文件获取agent应用信息
}
````

如果需要自定义获取`agent`应用信息，只需要实现`IAgentConfigService`接口即可，然后在配置文件中配置`agentSourceClass`即可。

* IAgentTokenService 接口的作用

用来获取`agent`应用`token`，默认读取本地缓存，从缓存中读取`agent`应用`token`：

````java
public class CacheAgentTokenServiceImpl implements IAgentTokenService {
}
````

如果需要自定义获取`agent`应用`token`，只需要实现`IAgentTokenService`接口即可，然后在配置文件中配置`tokenSourceClass`即可(推荐使用)。

## 四、应用案例

### 4.1、官方案例

为了更好地帮助个人或企业开发者用户熟悉抖音开放生态及学习 DyJava 的能力，DyJava 开源社区官方将推出一个抖音开放生态应用案例：**矩链互动(开源版)** 。

矩链互动(开源版)基于 DyJava 抖音开发能力，帮助个人或企业开发者用户快速搭建一套 **社交媒体运营平台框架** ，提升研发效率，降低学习成本。

*  **矩链互动(开源版)特性** 

    - [x]  **数据洞察：** Dashboard、账号洞察、作品洞察、粉丝洞察、意向用户洞察； 
    - [x]  **内容生产：** 抖音作品管理；
    - [x]  **私域营销：** 意向用户管理、粉丝群管理、粉丝/粉丝群标签管理；
    - [x]  **系统管理：** 抖音授权管理、组织协同管理

具体以开源计划为准。

*  **官网&体验环境** 

 _（封测中，即将开放...）_ 


*  **开源计划** 

 _（规划中，即将发布...）_ 


*  **特性预览** 

![输入图片说明](readme-pic/maxmatri-1.png)
![输入图片说明](readme-pic/maxmatri-2.png)
![输入图片说明](readme-pic/maxmatri-3.png)
![输入图片说明](readme-pic/maxmatri-4.png)


### 4.2、社区案例

完整案例登记列表，请【[访问这里](https://gitee.com/sxwdmjy/dy-java/issues/I9G8DI)】查看。

欢迎登记更多的案例，让更多的开发者看到 DyJava。

## 五、参与贡献

我们非常欢迎社区小伙伴通过但不限于以下方式，积极参与到 DyJava 开源社区的建设中来：

* Fork 本仓库
* 新建分支
* 提交代码
* 新建 Pull Request
* 提交 Issue

## 六、联系我们

如果您发现了任何问题或者有任何建议，欢迎加入我们的官方技术交流群（群号：307395193），也可以直接扫码联系我们：

![输入图片说明](readme-pic/5-contactus.png)

## 七、特别鸣谢

非常感谢以下开源项目对 DyJava 的帮助与支持：

* [forest：声明式与编程式双修的 Java HTTP 客户端框架](https://forest.dtflyx.com/) 

## 八、开源协议

DyJava 采用 [Apache 2.0](https://gitee.com/dromara/dy-java/blob/master/LICENSE) 开源协议，个人或企业用户需要确保在分发、修改或使用 DyJava 时，保留我们的原始版权声明、许可条款和任何归属性声明。

我们希望 DyJava 开源社区不仅能促进抖音开发生态的发展，同时也能推动国内开源生态及优质开源软件的发展与合作。