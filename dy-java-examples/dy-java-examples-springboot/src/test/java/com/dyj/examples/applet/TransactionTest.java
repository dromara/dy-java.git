package com.dyj.examples.applet;

import com.alibaba.fastjson.JSON;
import com.dyj.applet.DyAppletClient;
import com.dyj.applet.domain.GetFundBillQuery;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.QueryWithdrawOrderQuery;
import com.dyj.applet.domain.vo.UploadImageMaterialVo;
import com.dyj.applet.handler.ImageMaterialHandler;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.DeveloperNotifyQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;
import com.dyj.common.utils.DecryptUtils;
import com.dyj.examples.DyJavaExamplesApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;

/**
 * 交易系统测试
 */
@EnableAutoConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DyJavaExamplesApplication.class)
public class TransactionTest {


    /**
     * 进件图片上传
     */
    @Test
    public void merchantImageUpload(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.merchantImageUpload("jpg",null)
                )
        );
    }

    /**
     * 发起进件
     */
    @Test
    public void createSubMerchantV3(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createSubMerchantV3(CreateSubMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 进件查询
     */
    @Test
    public void queryMerchantStatusV3(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryMerchantStatusV3(QueryMerchantStatusMerchantQuery.builder()
                                .merchantId("1")
                                .appId("1")
                                .build())
                )
        );
    }

    /**
     * 开发者获取小程序收款商户/合作方进件页面
     */
    @Test
    public void openAppAddSubMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openAppAddSubMerchant(GetMerchantUrlMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取小程序收款商户进件页面
     */
    @Test
    public void openAddAppMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openAddAppMerchant(GetMerchantUrlMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取服务商进件页面
     */
    @Test
    public void openSaasAddMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openSaasAddMerchant(OpenSaasAddMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取合作方进件页面
     */
    @Test
    public void openSaasAddSubMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openSaasAddSubMerchant(OpenSaasAddSubMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 查询标签组信息
     */
    @Test
    public void tagQuery(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.tagQuery(TagQueryQuery.builder().build())
                )
        );
    }

    /**
     * 查询CPS信息
     */
    @Test
    public void queryCps(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryCps(QueryTransactionCpsQuery.builder().build())
                )
        );
    }

    /**
     * 查询订单信息
     */
    @Test
    public void queryOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryOrder(QueryTransactionOrderQuery.builder().build())
                )
        );
    }

    /**
     * 发起退款
     */
    @Test
    public void createRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createRefund(CreateRefundQuery.builder().build())
                )
        );
    }

    /**
     * 查询退款
     */
    @Test
    public void queryRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryRefund(QueryRefundQuery.builder().build())
                )
        );
    }

    /**
     * 同步退款审核结果
     */
    @Test
    public void merchantAuditCallback(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.merchantAuditCallback(MerchantAuditCallbackQuery.builder().build())
                )
        );
    }

    /**
     * 推送履约状态
     */
    @Test
    public void fulfillPushStatus(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.fulfillPushStatus(FulfillPushStatusQuery.builder().build())
                )
        );
    }

    /**
     * 发起分账
     */
    @Test
    public void createSettle(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createSettle(CreateSettleQuery.builder().build())
                )
        );
    }

    /**
     * 查询分账
     */
    @Test
    public void querySettle(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.querySettle(QuerySettleQuery.builder().build())
                )
        );
    }

    /**
     * 商户余额查询
     */
    @Test
    public void queryChannelBalanceAccount(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryChannelBalanceAccount(QueryChannelBalanceAccountQuery.builder().build())
                )
        );
    }

    /**
     * 商户提现
     */
    @Test
    public void merchantWithdraw(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.merchantWithdraw(MerchantWithdrawQuery.builder().build())
                )
        );
    }

    /**
     * 商户提现结果查询
     */
    @Test
    public void queryWithdrawOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryWithdrawOrder(QueryWithdrawOrderQuery.builder().build())
                )
        );
    }

    /**
     * 开发者获取小程序收款商户/合作方提现页面
     */
    @Test
    public void saasAppAddSubMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.saasAppAddSubMerchant(SaasAppAddSubMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取小程序收款商户提现页面
     */
    @Test
    public void saasGetAppMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.saasGetAppMerchant(OpenAddAppMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取服务商提现页面
     */
    @Test
    public void saasAddMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.saasAddMerchant(SaasAddMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取合作方提现页面
     */
    @Test
    public void saasAddSubMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.saasAddSubMerchant(OpenSaasAddSubMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 获取资金账单
     */
    @Test
    public void getFundBill(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.getFundBill(GetFundBillQuery.builder().build())
                )
        );
    }

    /**
     * 获取交易账单
     */
    @Test
    public void getBill(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.getBill(GetBillQuery.builder().build())
                )
        );
    }

    /**
     * 查询订单基本信息
     */
    @Test
    public void queryIndustryOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryIndustryOrder(QueryIndustryOrderQuery.builder().build())
                )
        );
    }

    /**
     * 查询券状态信息
     */
    @Test
    public void queryIndustryItemOrderInfo(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryIndustryItemOrderInfo(QueryIndustryItemOrderInfoQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->查询 CPS 信息
     */
    @Test
    public void queryIndustryOrderCps(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryIndustryOrderCps(QueryIndustryOrderCpsQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->预下单->开发者发起下单
     */
    @Test
    public void preCreateIndustryOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.preCreateIndustryOrder(PreCreateIndustryOrderQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->营销算价->查询营销算价
     */
    @Test
    public void queryAndCalculateMarketing(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryAndCalculateMarketing(QueryAndCalculateMarketingQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->核销->抖音码->验券准备
     */
    @Test
    public void deliveryPrepare(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.deliveryPrepare(DeliveryPrepareQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->核销->抖音码->验券
     */
    @Test
    public void deliveryVerify(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.deliveryVerify(DeliveryVerifyQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->核销->抖音码->撤销核销
     * 生活服务交易系统->核销->三方码->撤销核销
     */
    @Test
    public void verifyCancel(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.verifyCancel(VerifyCancelQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->核销->三方码->推送核销状态
     */
    @Test
    public void pushDelivery(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.pushDelivery(PushDeliveryQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->分账->查询分账
     */
    @Test
    public void industryQuerySettle(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.industryQuerySettle(IndustryQuerySettleQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->退货退款->开发者发起退款
     */
    @Test
    public void developerCreateRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.developerCreateRefund(DeveloperCreateRefundQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->退货退款->同步退款审核结果
     */
    @Test
    public void refundMerchantAuditCallback(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.refundMerchantAuditCallback(RefundMerchantAuditCallbackQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->退货退款->查询退款
     */
    @Test
    public void industryQueryRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.industryQueryRefund(QueryRefundQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->预约->创建预约单
     */
    @Test
    public void createBook(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createBook(CreateBookQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->预约->预约接单结果回调
     */
    @Test
    public void bookResultCallback(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.bookResultCallback(BookResultCallbackQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->预约->商家取消预约
     */
    @Test
    public void merchantCancelBook(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.merchantCancelBook(MerchantCancelBookQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->预约->用户取消预约
     */
    @Test
    public void userCancelBook(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.userCancelBook(UserCancelBookQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->预约->查询预约单信息
     */
    @Test
    public void queryBook(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryBook(QueryBookQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->预下单->关闭订单
     */
    @Test
    public void closeOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.closeOrder(CloseOrderQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->核销->核销工具->查询用户券列表
     */
    @Test
    public void queryUserCertificates(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryUserCertificates(QueryUserCertificatesQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->核销->核销工具->查询订单可用门店
     */
    @Test
    public void orderCanUse(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.orderCanUse(OrderCanUseQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->核销->核销工具->设置商家展示信息
     */
    @Test
    public void updateMerchantConf(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.updateMerchantConf(UpdateMerchantConfQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->核销->核销工具->查询商家配置文案
     */
    @Test
    public void tradeToolkitQueryText(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.tradeToolkitQueryText(TradeToolkitQueryTextQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->核销->核销工具->设置订单详情页按钮白名单接口
     */
    @Test
    public void buttonWhiteSetting(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.buttonWhiteSetting(ButtonWhiteSettingQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->核销->核销工具->设置小程序跳转path
     */
    @Test
    public void updateMerchantPath(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.updateMerchantPath(UpdateMerchantPathQuery.builder().build())
                )
        );
    }

    /**
     * 生活服务交易系统->分账->发起分账
     */
    @Test
    public void createSettleV2(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createSettleV2(CreateSettleV2Query.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->预下单->查询 CPS 信息
     */
    @Test
    public void queryTradeCps(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryTradeCps(QueryIndustryOrderCpsQuery.builder()
                                .outOrderNo("64738920198376278").build())
                )
        );
    }

    /**
     * 行业交易系统->预下单->查询订单信息
     */
    @Test
    public void queryTradeOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryTradeOrder(QueryTradeOrderQuery.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->预下单->开发者发起下单
     */
    @Test
    public void createTradeOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createTradeOrder(CreateTradeOrderQuery.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->核销->抖音码->验券准备
     */
    @Test
    public void tradeDeliveryPrepare(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.tradeDeliveryPrepare(DeliveryPrepareQuery.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->核销->抖音码->验券
     */
    @Test
    public void tradeDeliveryVerify(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.tradeDeliveryVerify(DeliveryVerifyQuery.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->核销->抖音码->查询劵状态信息
     */
    @Test
    public void queryItemOrderInfo(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryItemOrderInfo(QueryIndustryItemOrderInfoQuery.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->核销->三方码->推送核销状态
     */
    @Test
    public void tradePushDelivery(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.tradePushDelivery(TradePushDeliveryQuery.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->分账->发起分账
     */
    @Test
    public void tradeCreateSettle(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.tradeCreateSettle(CreateSettleV2Query.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->分账->查询分账
     */
    @Test
    public void tradeQuerySettle(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.tradeQuerySettle(TradeQuerySettleQuery.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->退货退款->开发者发起退款
     */
    @Test
    public void tradeCreateRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.tradeCreateRefund(TradeCreateRefundQuery.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->退货退款->同步退款审核结果
     */
    @Test
    public void merchantAuditCallbackV2(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.merchantAuditCallbackV2(MerchantAuditCallbackQuery.builder().build())
                )
        );
    }

    /**
     * 行业交易系统->退货退款->查询退款
     */
    @Test
    public void tradeQueryRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.tradeQueryRefund(QueryRefundQuery.builder().build())
                )
        );
    }

    /**
     * 运营->素材库->素材库图片上传
     */
    @Test
    public void uploadImageMaterial(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.uploadImageMaterial(UploadImageMaterialQuery.builder().build())
                )
        );
    }

    /**
     * 运营->素材库->素材库设置功能配置
     */
    @Test
    public void addFunctionConfig(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.addFunctionConfig(AddFunctionConfigQuery.builder().build())
                )
        );
    }

    /**
     * 运营->素材库->素材库查询功能配置审核状态
     */
    @Test
    public void queryFunctionConfigStatus(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryFunctionConfigStatus(TransactionMerchantTokenTypeEnum.CLIENT_TOKEN, Arrays.asList("23123","12312"))
                )
        );
    }

    /**
     * 基础能力->内容安全->图片检测V3
     */
    @Test
    public void imageCensor(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.imageCensor(ImageCensorQuery.builder().build())
                )
        );
    }

    /**
     * 基础能力->内容安全->内容安全检测
     */
    @Test
    public void textAntiDirt(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.textAntiDirt(TextAntiDirtQuery.builder().build())
                )
        );
    }

    /**
     * 触达与营销->订阅消息->给用户发送订阅消息
     */
    @Test
    public void developerNotify(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.developerNotify(DeveloperNotifyQuery.builder().build())
                )
        );
    }

    /**
     * 触达与营销->订阅消息->查询订阅消息模版库
     */
    @Test
    public void querySubscriptionTplList(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.querySubscriptionTplList("",1,"",1,1,1)
                )
        );
    }

    /**
     * 触达与营销->订阅消息->新建订阅消息模板
     */
    @Test
    public void createSubscriptionTpl(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createSubscriptionTpl(CreateSubscriptionTplQuery.builder().build())
                )
        );
    }


    /**
     * 触达与营销->订阅消息->查询小程序新建的订阅消息模板列表
     */
    @Test
    public void queryCreatedSubscriptionTplList(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryCreatedSubscriptionTplList(1,1,1)
                )
        );
    }

    @Test
    public void signTest(){
        String privateKeyPath = "private.key";
        String publicKeyPath = "public.key";
        PrivateKey privateKey = DecryptUtils.loadRsaPrivateKey(privateKeyPath);
        PublicKey publicKey = DecryptUtils.loadRsaPublicKey(publicKeyPath);
        String waitSign = "POST\n/abc\n1680835692\ngjjRNfQlzoDIJtVDOfUe\n{\"eventTime\":1677653869000,\"status\":102}\n";
        String signStr = "RFQ65hHlo4xyZ6EC31LZC0SzsyN0nd2Fb2wAiISvY1mkiC6G8gn2QZwLGq7qgjenRGl/Z8OrTtkBHWb9GOazkJFkHrPeRqogqnwZ+kSOxGvtou8FPN669E1wwb+BShN4pIUgPFzaukR9/rCRBsbLoq9RPVA2sbf3iKoHGa81zhXjQSuFbF1CyiWkL5qqniNTM/BSfwfLZfPW8nBanRl3U+mQaymbj0DCF0ZdWhFz1FnZPAfEpx8YEwFNZWtxzz4p3WJ1swnUocJC4LXoDazo6DhEPDuoZXOXrB1SqzL1wRqA4p8uj3Z8Seki/PMGWiGpGWPMv3tJyvWmzMOuVJtEjg==";

        String sign2 = DecryptUtils.sha256withRSASignature(waitSign, privateKey);
        System.out.println(sign2);
        System.out.println(sign2.equals(signStr));
        System.out.println(DecryptUtils.sha256withRSAVerify(waitSign, sign2, publicKey));
    }
}
