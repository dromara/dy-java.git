package com.dyj.examples.applet;

import com.alibaba.fastjson.JSON;
import com.dyj.applet.DyAppletClient;
import com.dyj.applet.domain.GetFundBillQuery;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.QuerySignOrderVo;
import com.dyj.applet.domain.vo.QueryWithdrawOrderQuery;
import com.dyj.examples.DyJavaExamplesApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 交易系统测试
 */
@EnableAutoConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DyJavaExamplesApplication.class)
public class TradeToolTest {


    /**
     * 交易工具->信用免押->信用授权->信用准入查询
     */
    @Test
    public void queryAdmissibleAuth(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryAdmissibleAuth(QueryAdmissibleAuthQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->信用免押->信用授权->信用下单
     */
    @Test
    public void createAuthOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createAuthOrder(CreateAuthOrderQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->信用免押->信用授权->信用订单撤销
     */
    @Test
    public void cancelAuthOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.cancelAuthOrder(CancelAuthOrderQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->信用免押->信用授权->信用订单完结
     */
    @Test
    public void finishAuthOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.finishAuthOrder(FinishAuthOrderQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->信用免押->信用授权->信用订单查询
     */
    @Test
    public void queryAuthOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryAuthOrder(QueryAuthOrderQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->信用免押->扣款->发起扣款
     */
    @Test
    public void createPayOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createPayOrder(CreatePayOrderQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->信用免押->扣款->关闭扣款
     */
    @Test
    public void closePayOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.closePayOrder(ClosePayOrderQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->信用免押->扣款->扣款单查询
     */
    @Test
    public void queryPayOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryPayOrder(QueryPayOrderQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->信用免押->退款->发起退款
     */
    @Test
    public void createPayOrderRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createPayOrderRefund(CreatePayOrderRefundQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->信用免押->退款->退款查询
     */
    @Test
    public void queryPayRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryPayRefund(QueryPayRefundQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->周期代扣->签约授权->发起解约
     */
    @Test
    public void terminateSign(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.terminateSign(TerminateSignQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->周期代扣->签约授权->签约订单查询
     */
    @Test
    public void querySignOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.querySignOrder(QuerySignOrderQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->周期代扣->代扣->发起代扣
     */
    @Test
    public void createSignPayOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createSignPayOrder(CreateSignPayOrderQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->周期代扣->代扣->代扣结果查询
     */
    @Test
    public void querySignPayOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.querySignPayOrder(QuerySignPayOrderQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->周期代扣->退款->发起退款
     */
    @Test
    public void createSignRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createSignRefund(CreateSignRefundQuery.builder().build())
                )
        );
    }

    /**
     * 交易工具->周期代扣->退款->退款查询
     */
    @Test
    public void querySignRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.querySignRefund(QuerySignRefundQuery.builder().build())
                )
        );
    }
}
